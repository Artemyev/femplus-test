#ifndef FEMPLUS_TEST_COARSE_FINITE_ELEMENT_HPP
#define FEMPLUS_TEST_COARSE_FINITE_ELEMENT_HPP

#include "config.hpp"
#include "gtest/gtest.h"
#include "femplus/coarse_finite_element.hpp"
#include "femplus/coarse_rectangle.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"

using namespace femplus;


// =============================================================================
// These tests are not fully "tests", since they don't compare anything. They
// exist just to check how visual representation of multiscale basis functions
// work, and to see those basis functions in ParaView
// =============================================================================
TEST(CoarseFE, compute_basis_scalar)
{
  const double x0 = 0;
  const double x1 = 25;
  const double y0 = 0;
  const double y1 = 25;
  const int    nx = 25;
  const int    ny = 25;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 1; // scalar FE

  CoarseFiniteElement coarse_fe(&coarse_rect,
                                coarse_mesh_order,
                                fine_mesh_order,
                                n_fine_dofs_per_fine_node);

  int n_boundary_bf = 5;
  int n_interior_bf = 5;
  std::shared_ptr<Function> coef_mass(new ConstantFunction(1.));
  std::shared_ptr<Function> coef_stif(new ConstantFunction(1.));

  double t0 = get_wall_time();
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  const bool show_time = false;
  coarse_fe.compute_basis(coef_mass,
                          coef_stif,
                          n_boundary_bf,
                          n_interior_bf,
                          JADAMILU,
                          JADAMILU,
                          boundary_regularization,
                          interior_regularization,
                          boundary_take_first_eigen,
                          interior_take_first_eigen,
                          boundary_edge_mass_matrix,
                          show_time);

  double t1 = get_wall_time();
  std::cout << "time = " << t1 - t0 << std::endl;

  coarse_fe.write_boundary_bf(TESTOUT_DIR + "coarse_fe_bound_bf_scalar.vtu");
  coarse_fe.write_interior_bf(TESTOUT_DIR + "coarse_fe_inter_bf_scalar.vtu");
}

// =============================================================================
TEST(CoarseFE, compute_basis_vector2)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 1000.;
      const double vp = 3000.;
      const double vs = 2000.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 10;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 10;
  const int    ny = 10;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 2; // vector FE

  CoarseFiniteElement coarse_fe(&coarse_rect,
                                coarse_mesh_order,
                                fine_mesh_order,
                                n_fine_dofs_per_fine_node);

  int n_boundary_bf = 10;
  int n_interior_bf = 10;
  std::shared_ptr<Function> coef_rho(new ConstantFunction(1000.));
  std::shared_ptr<Function> coef_C(new CoefC());

  double t0 = get_wall_time();
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  const bool show_time = false;
  coarse_fe.compute_basis(coef_rho,
                          coef_C,
                          n_boundary_bf,
                          n_interior_bf,
                          LAPACK,
                          LAPACK,
                          boundary_regularization,
                          interior_regularization,
                          boundary_take_first_eigen,
                          interior_take_first_eigen,
                          boundary_edge_mass_matrix,
                          show_time);

  double t1 = get_wall_time();
  std::cout << "time = " << t1 - t0 << std::endl;

  coarse_fe.write_boundary_bf(TESTOUT_DIR + "coarse_fe_bound_bf_vector2.vtu");
  coarse_fe.write_interior_bf(TESTOUT_DIR + "coarse_fe_inter_bf_vector2.vtu");
}


#endif // FEMPLUS_TEST_COARSE_FINITE_ELEMENT_HPP
