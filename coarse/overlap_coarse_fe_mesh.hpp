#ifndef FEMPLUS_TEST_OVERLAP_COARSE_FE_MESH_HPP
#define FEMPLUS_TEST_OVERLAP_COARSE_FE_MESH_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/overlap_coarse_fe_mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/gms_dof_handler.hpp"

using namespace femplus;

//==============================================================================
TEST(OverlapCoarseFEMesh, compute_damping_matrix_rect_scalar)
{
  // 0. set the parameters
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 1;
  const int _n_overlap_elements = 4;
  const double rho    = 2.5e+3;
  const double kappa  = 1e+9;
  const int _n_boundary_bf = 1;
  const int _n_interior_bf = 1;
  Eigensolver eigensolver_boundary = JADAMILU;
  Eigensolver eigensolver_interior = JADAMILU;
  const double gamma = 0.5;
  const double f = 10.;
  const double hx = (_x_end-_x_beg)/(_n_coarse_x*_n_fine_x);
  const double abc_layer_width = 10.*hx;

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh with overlapping
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new OverlapCoarseFEMesh(coarse_mesh,
                                FE_ORDER_COARSE,
                                FE_ORDER_FINE,
                                n_fine_dofs_per_fine_node,
                                _n_overlap_elements));
  t1 = get_wall_time();
  std::cout << "build finite element mesh with overlapping, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices, damping
  //    matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new ConstantFunction(kappa));
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  const bool show_time = false;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         boundary_edge_mass_matrix,
                         show_time);

  std::vector<Vector> weights;
  fe_mesh->compute_damping_matrices(*coef_mass.get(),
                                    *coef_stif.get(),
                                    abc_layer_width, f,
                                    &weights);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, damping matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs and write the weights of the damping matrix
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  const std::string fname = "over_coarse_fe_mesh_weights_scalar.vtu";
  std::vector<std::string> names(fe_mesh->n_elements(), "weights");
  dof_handler.write_solution_vtu_in_cells(fname, weights, names);
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs and write weights, time = " << t1-t0 << std::endl;
  t0 = t1;
}

//==============================================================================
TEST(OverlapCoarseFEMesh, compute_damping_matrix_rect_vector2)
{
  // 0. set the parameters
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const int _n_overlap_elements = 4;
  const double rho = 2500.;
  const double vp  = 1000.;
  const double vs  = 1000.;
  const int _n_boundary_bf = 1;
  const int _n_interior_bf = 1;
  Eigensolver eigensolver_boundary = JADAMILU;
  Eigensolver eigensolver_interior = JADAMILU;
  const double gamma = 0.5;
  const double f = 10.;
  const double hx = (_x_end-_x_beg)/(_n_coarse_x*_n_fine_x);
  const double abc_layer_width = 10.*hx;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh with overlapping
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new OverlapCoarseFEMesh(coarse_mesh,
                                FE_ORDER_COARSE,
                                FE_ORDER_FINE,
                                n_fine_dofs_per_fine_node,
                                _n_overlap_elements));
  t1 = get_wall_time();
  std::cout << "build finite element mesh with overlapping, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices, damping
  //    matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  const bool show_time = false;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         boundary_edge_mass_matrix,
                         show_time);

  std::vector<Vector> weights;
  fe_mesh->compute_damping_matrices(*coef_mass.get(),
                                    *coef_stif.get(),
                                    abc_layer_width, f,
                                    &weights);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, damping matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs and write the weights of the damping matrix
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  const std::string fname = "over_coarse_fe_mesh_weights_vector.vtu";
  std::vector<std::string> names(fe_mesh->n_elements(), "weights");
  dof_handler.write_solution_vtu_in_cells(fname, weights, names);
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs and write weights, time = " << t1-t0 << std::endl;
  t0 = t1;
}

#endif // FEMPLUS_TEST_OVERLAP_COARSE_FE_MESH_HPP
