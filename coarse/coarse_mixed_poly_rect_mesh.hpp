#ifndef TEST_FEMPLUS_COARSE_MIXED_POLY_RECT_MESH_HPP
#define TEST_FEMPLUS_COARSE_MIXED_POLY_RECT_MESH_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/function.hpp"
#include "femplus/coarse_mesh.hpp"
#include "femplus/coarse_mixed_poly_rect_mesh.hpp"
#include "femplus/coarse_element.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/mesh_element.hpp"

#include "analytic_functions.hpp"

#include <algorithm>


using namespace femplus;

#if 0 // commented for speedup, UNCOMMENT
// =======================================================
TEST(CoarseMixedPolyRectMesh, build1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 2;
  const int Ny = 2;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect1.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build1_2)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 2;
  const int Ny = 2;
  const int nx = 2;
  const int ny = 2;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));
  top_surface.push_back(Point(0.5, y0)); // coincides with the mesh vertex

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect1_2.msh");
}

//// =======================================================
//TEST(CoarseMixedPolyRectMesh, build1_3)
//{
//  std::vector<Point> top_surface;
//  top_surface.push_back(Point(0, 0));
//  top_surface.push_back(Point(1, 0));

//  const int N = 100;
//  for (int i = 0; i < N - 1; ++i)
//  {
//    // one point coincides with the mesh vertex,
//    // and there are many points which are close
//    // to the vertex
//    top_surface.push_back(Point((i+1)*1./N, 0));
//  }

//  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(top_surface, 1, 2, 2, 2, 2));
//  mesh->build();

//  //EXPECT_EQ(mesh->n_vertices(), 9);
//  //EXPECT_EQ(mesh->n_elements(), 4);
//  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  mesh->write_msh("coarse_mixed_poly_rect1_3.msh");
//}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build1_4)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 2;
  const int Ny = 2;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect1_4.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build1_5)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 2;
  const int Ny = 2;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny,
                                                 FineMeshType::Triangular));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect1_5.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build2)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 3;
  const int Ny = 3;
  const int nx = 2;
  const int ny = 2;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect2.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build2_2)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 3;
  const int Ny = 3;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect2_2.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build3)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 10;
  const int Ny = 10;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect3.msh");
}
#endif

#if 0 // UNCOMMENT THIS AND TEST !!!
// =======================================================
TEST(CoarseMixedPolyRectMesh, build4)
{
  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, 0));
  top_surface.push_back(Point(1, 0.1));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect4.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build5)
{
  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, 0));
  top_surface.push_back(Point(1, 0));
  top_surface.push_back(Point(0.25, 0.1));
  top_surface.push_back(Point(0.75, 0.1));
  top_surface.push_back(Point(0.5, 0));

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect5.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build6)
{
  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, 0));
  top_surface.push_back(Point(1, 0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = (i + 1) * 1. / N;
    top_surface.push_back(Point(x, 0.1 * sin(x)));
  }

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect6.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build7)
{
  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, 0));
  top_surface.push_back(Point(1, 0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = (i + 1) * 1. / N;
    top_surface.push_back(Point(x, 0.1 * sin(10*x)));
  }

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect7.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build8)
{
  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, 0));
  top_surface.push_back(Point(1, 0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = (i + 1) * 1. / N;
    top_surface.push_back(Point(x, 0.02 * sin(10*math::PI*x)));
  }

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect8.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build8_1)
{
  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, 0));
  top_surface.push_back(Point(1, 0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = (i + 1) * 1. / N;
    top_surface.push_back(Point(x, 0.02 * sin(10*math::PI*x)));
  }

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10,
                                                 FineMeshType::Rectangular,
                                                 FineMeshType::QuadrilateralSimple));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect8_1.msh");
}

// =======================================================
//TEST(CoarseMixedPolyRectMesh, build8_2)
//{
//  std::vector<Point> top_surface;
//  top_surface.push_back(Point(0, 0));
//  top_surface.push_back(Point(1, 0));

//  const int N = 100;
//  for (int i = 0; i < N - 1; ++i)
//  {
//    const double x = (i + 1) * 1. / N;
//    top_surface.push_back(Point(x, 0.02 * sin(10*math::PI*x)));
//  }

//  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 10, 10));
//  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10,
//                                                 FineMeshType::Triangular,
//                                                 FineMeshType::Triangular));
//  mesh->build_coarse_mesh();
//  mesh->build_fine_mesh();

//  //EXPECT_EQ(mesh->n_vertices(), 9);
//  //EXPECT_EQ(mesh->n_elements(), 4);
//  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

////  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
////  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

//  mesh->write_msh("coarse_mixed_poly_rect8_2.msh");
//  std::vector<std::vector<int> > dummy_vec1, dummy_vec2;
//  MeshPtr full_tria_mesh = mesh->full_fine_triangular_mesh(dummy_vec1, dummy_vec2);
//  std::vector<std::set<int> > connections;
//  full_tria_mesh->vertices_direct_connections(connections);
//  full_tria_mesh->numerate_edges(connections);
//  TriangularMesh *tr_mesh = dynamic_cast<TriangularMesh*>(full_tria_mesh.get());
//  EXPECT_TRUE(tr_mesh != NULL);
//  tr_mesh->clear_boundary_elements();
//  tr_mesh->boundary_elements_initialization();
//  tr_mesh->write("mixed_poly_rect8_2_full_tria.msh");
//}

// =======================================================
// this test is commented because QuadrilateralMesh class doesn't have read_msh
// function implemented
//TEST(CoarseMixedPolyRectMesh, build8_3)
//{
//  std::vector<Point> top_surface;
//  top_surface.push_back(Point(0, 0));
//  top_surface.push_back(Point(1, 0));

//  const int N = 100;
//  for (int i = 0; i < N - 1; ++i)
//  {
//    const double x = (i + 1) * 1. / N;
//    top_surface.push_back(Point(x, 0.02 * sin(10*math::PI*x)));
//  }

//  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, 1, 2, 2));
//  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 5, 5,
//                                                 FineMeshType::Rectangular,
//                                                 FineMeshType::QuadrilateralGmsh));
//  mesh->build_coarse_mesh();
//  mesh->build_fine_mesh();

//  mesh->write_msh("coarse_mixed_poly_rect8_3.msh");
//}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build9)
{
  std::vector<Point> top_surface;
  const double x0 = -5;
  const double x1 = 5;
  const double y0 = 0;
  const double depth = 10;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = x0 + (i + 1) * (x1 - x0) / N;
    top_surface.push_back(Point(x, gauss(0, 0.4, x)));
  }

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, depth, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect9.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build9_1)
{
  std::vector<Point> top_surface;
  const double x0 = -5;
  const double x1 = 5;
  const double y0 = 0;
  const double depth = 10;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = x0 + (i + 1) * (x1 - x0) / N;
    top_surface.push_back(Point(x, gauss(0, 0.4, x)));
  }

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, depth, 10, 10));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 10, 10,
                                                 FineMeshType::Rectangular,
                                                 FineMeshType::QuadrilateralSimple));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_msh(TESTOUT_DIR + "coarse_mixed_poly_rect9_1.msh");
}

// =======================================================
TEST(CoarseMixedPolyRectMesh, build9_2)
{
  std::vector<Point> top_surface;
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const double mu = 0.5 * (x0 + x1);
  const double sigma = 0.025;

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = x0 + (i + 1) * (x1 - x0) / N;
    top_surface.push_back(Point(x, 0.0025*gauss(mu, sigma, x)));
  }

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, depth, 5, 5));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, 5, 5,
                                                 FineMeshType::Triangular,
                                                 FineMeshType::Triangular));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_msh("coarse_mixed_poly_rect9_2.msh");
  MeshPtr full_tria_mesh = mesh->full_fine_triangular_mesh();
  std::vector<std::set<int> > connections;
  full_tria_mesh->vertices_direct_connections(connections);
  full_tria_mesh->numerate_edges(connections);
  full_tria_mesh->clear_boundary_elements();
  full_tria_mesh->init_boundary_elements();
  full_tria_mesh->write(TESTOUT_DIR + "mixed_poly_rect9_2_full_tria.msh");
}
#endif

// =============================================================================
#if 0 // uncomment and test !!!
TEST(CoarseMixedPolyRectMesh, build_salt_dome)
{
  const std::string fname = TESTFILES_DIR + "/salt_dome_for_topo_points.msh";

  TriangularMesh tmesh(fname); // read the mesh

  std::vector<int> top_points;
  std::vector<Point> top_surface;
  for (int i = 0; i < tmesh.n_boundary_elements(); ++i) {
    const MeshElement *line = tmesh.boundary_element(i);
    if (line->material_id() == Mesh::TOP_LINE) {
      for (int v = 0; v < line->n_vertices(); ++v) {
        const int vert = line->vertex_number(v);
        if (std::find(top_points.begin(), top_points.end(), vert) == top_points.end()) {
          top_points.push_back(vert);
          top_surface.push_back(tmesh.vertex(vert));
        }
      }
    }
  }

  std::sort(top_surface.begin(), top_surface.end(), Point::compare_by_x);

  const double zero_surface = 1000;
  const double depth = 1000;
  const int Nx = 10;
  const int Ny = 11;
  const int nx = 15;
  const int ny = 15;
  const double percent_accept = 1.;

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, zero_surface, depth,
                                                      Nx, Ny, percent_accept));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny,
                                                 FineMeshType::Rectangular,
                                                 FineMeshType::Triangular));
  mesh->build_coarse_mesh();
  mesh->build_fine_mesh();

  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int Nnx = 150;//Nx*nx;
  const int Nny = 150;//Ny*ny;
  const std::string rect_file = TESTFILES_DIR + "/salt_dome_flat_F_R.msh";
  MeshPtr f_r_mesh(new RectangularMesh(x0, x1, y0, y1, Nnx, Nny));
  f_r_mesh->read(rect_file);

  const int top_layer_id = 1;
  CoarseMixedPolyRectMesh *cmesh = dynamic_cast<CoarseMixedPolyRectMesh*>(mesh.get());
  cmesh->set_material_rect(f_r_mesh.get());
  cmesh->set_material_poly(top_layer_id);

  mpr_mesh->write_geo(TESTOUT_DIR + "salt_dome_topo_C1.geo");
  mesh->write_msh(TESTOUT_DIR + "salt_dome_topo_C1.msh");
}
#endif

// =============================================================================
#if 1 // uncomment and test !!!
TEST(CoarseMixedPolyRectMesh, build_karst)
{
  const std::string fname = TESTFILES_DIR + "/karst.msh";

  TriangularMesh tmesh(fname); // read the mesh

  // extract the points which are on the top surface of the domain
  std::cout << "top surface points extraction..." << std::flush;
  std::vector<int> top_points;
  std::vector<Point> top_surface;
  for (int i = 0; i < tmesh.n_boundary_elements(); ++i) {
    const MeshElement *line = tmesh.boundary_element(i);
    if (line->material_id() == Mesh::TOP_LINE) {
      for (int v = 0; v < line->n_vertices(); ++v) {
        const int vert = line->vertex_number(v);
        if (std::find(top_points.begin(), top_points.end(), vert) == top_points.end()) {
          top_points.push_back(vert);
          top_surface.push_back(tmesh.vertex(vert));
        }
      }
    }
  }
  std::sort(top_surface.begin(), top_surface.end(), Point::compare_by_x);
  std::cout << "done" << std::endl;

  const double zero_surface = -10;
  const double depth = 490;
  const int Nx = 10;
  const int Ny = 11;
  const double h = 3.;
  const int nx = 1000 / Nx / h;//15;
  const int ny = 490 / (Ny-1) / h;//15;
  const double percent_accept = 1e-16;

  MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, zero_surface, depth,
                                                      Nx, Ny, percent_accept));
  CoarseMeshPtr mesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny,
                                                 FineMeshType::Rectangular,
                                                 FineMeshType::Triangular));
  std::cout << "coarse mesh building..." << std::flush;
  mesh->build_coarse_mesh(); std::cout << "done" << std::endl;
  std::cout << "fine mesh building..." << std::flush;
  mesh->build_fine_mesh(); std::cout << "done" << std::endl;

  const bool show_info = false;
  const int top_layer_id = 1;
  CoarseMixedPolyRectMesh *cmesh = dynamic_cast<CoarseMixedPolyRectMesh*>(mesh.get());
  std::cout << "set material IDs in rect part..." << std::flush;
  cmesh->set_material_rect(tmesh, show_info); std::cout << "done" << std::endl;
  std::cout << "set material IDs in poly part..." << std::flush;
  cmesh->set_material_tria(top_layer_id); std::cout << "done" << std::endl;

  std::cout << "writing .geo file..." << std::flush;
  mpr_mesh->write_geo(TESTOUT_DIR + "karst_C1.geo"); std::cout << "done" << std::endl;
//  std::cout << "writing .msh file..." << std::flush;
//  mesh->write_msh(TESTOUT_DIR + "karst_C1.msh"); std::cout << "done" << std::endl;
}
#endif

#endif // TEST_FEMPLUS_COARSE_MIXED_POLY_RECT_MESH_HPP
