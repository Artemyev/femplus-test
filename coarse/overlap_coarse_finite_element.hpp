#ifndef FEMPLUS_TEST_OVERLAP_COARSE_FINITE_ELEMENT_HPP
#define FEMPLUS_TEST_OVERLAP_COARSE_FINITE_ELEMENT_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/overlap_coarse_finite_element.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/rectangle.hpp"
#include "femplus/coarse_rectangle.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/dof_handler.hpp"

using namespace femplus;

// =============================================================================
TEST(OverlapCoarseFE, ctor0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int n_elems_overlap = 1;
  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 1;
  OverlapCoarseFiniteElement over_coarse_fe(&coarse_rect,
                                            coarse_mesh_order,
                                            fine_mesh_order,
                                            n_fine_dofs_per_fine_node,
                                            n_elems_overlap);

}

// =============================================================================
TEST(OverlapCoarseFE, ctor1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 10;
  const int    ny = 10;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int n_elems_overlap = 10;
  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 1;
  OverlapCoarseFiniteElement over_coarse_fe(&coarse_rect,
                                            coarse_mesh_order,
                                            fine_mesh_order,
                                            n_fine_dofs_per_fine_node,
                                            n_elems_overlap);

}

// =============================================================================
TEST(OverlapCoarseFE, ctor2)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 10;
  const int    ny = 10;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int n_elems_overlap = 10;
  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 2;
  OverlapCoarseFiniteElement over_coarse_fe(&coarse_rect,
                                            coarse_mesh_order,
                                            fine_mesh_order,
                                            n_fine_dofs_per_fine_node,
                                            n_elems_overlap);

}

// =============================================================================
TEST(OverlapCoarseFE, build_mass_matrix)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 3;
  const int    ny = 3;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int n_elems_overlap = 2;
  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 2;
  OverlapCoarseFiniteElement over_coarse_fe(&coarse_rect,
                                            coarse_mesh_order,
                                            fine_mesh_order,
                                            n_fine_dofs_per_fine_node,
                                            n_elems_overlap);
}

// =============================================================================
TEST(OverlapCoarseFE, compute_basis_vector2)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 1000.;
      const double vp = 3000.;
      const double vs = 2000.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 2;
  const double x1 = 8;
  const double y0 = 2;
  const double y1 = 8;
  const int    nx = 6;
  const int    ny = 6;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const int n_elems_overlap = 2;

  OverlapCoarseFiniteElement over_coarse_fe(&coarse_rect,
                                            coarse_mesh_order,
                                            fine_mesh_order,
                                            n_fine_dofs_per_fine_node,
                                            n_elems_overlap);

  int n_boundary_bf = 10;
  int n_interior_bf = 10;
  std::shared_ptr<Function> coef_rho(new ConstantFunction(1000.));
  std::shared_ptr<Function> coef_C(new CoefC());

  double t0 = get_wall_time();
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  const bool show_time = true;
  over_coarse_fe.compute_basis(coef_rho,
                               coef_C,
                               n_boundary_bf,
                               n_interior_bf,
                               LAPACK,
                               LAPACK,
                               boundary_regularization,
                               interior_regularization,
                               boundary_take_first_eigen,
                               interior_take_first_eigen,
                               boundary_edge_mass_matrix,
                               show_time);


  double t1 = get_wall_time();
  std::cout << "time = " << t1 - t0 << std::endl;

  over_coarse_fe.write_boundary_bf("over_coarse_fe_bound_bf.vtu");
  over_coarse_fe.write_interior_bf("over_coarse_fe_inter_bf.vtu");

  over_coarse_fe.write_orig_boundary_bf("over_coarse_fe_orig_bound_bf.vtu");
  over_coarse_fe.write_orig_interior_bf("over_coarse_fe_orig_inter_bf.vtu");
}

//==============================================================================
TEST(OverlapCoarseFE, build_damping_matrix_rect_scalar)
{
  const double x0 = 0;
  const double x1 = 100;
  const double y0 = 0;
  const double y1 = 100;
  const int    nx = 100;
  const int    ny = 100;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 1;
  const int n_elems_overlap = 2;

  OverlapCoarseFiniteElement over_coarse_fe(&coarse_rect,
                                            coarse_mesh_order,
                                            fine_mesh_order,
                                            n_fine_dofs_per_fine_node,
                                            n_elems_overlap);

  const double h = (x1 - x0) / nx;
  ConstantFunction coef_mass(1.);
  ConstantFunction coef_stif(1.);
  const double abc_layer_width = 20*h;
  const double source_frequency = 20;
  std::vector<double> weights;
  over_coarse_fe.compute_damping_matrix(coef_mass, coef_stif,
                                        abc_layer_width, source_frequency,
                                        x0-10, x1+10, y0-10, y1+10,
                                        &weights);

  const DoFHandler &dh = over_coarse_fe.dof_handler_fine();

  std::vector<Vector> weights_to_write(1);
  weights_to_write[0] = weights;
  std::vector<std::string> names(1);
  names[0] = "weights";
  const std::string fname = "over_coarse_fe_damping_weights3.vtu";
  dh.write_solution_vtu_in_cells(fname, weights_to_write, names);
}

//==============================================================================
TEST(OverlapCoarseFE, build_damping_matrix_rect_vector2)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 1000.;
      const double vp = 3000.;
      const double vs = 2000.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 100;
  const double y0 = 0;
  const double y1 = 100;
  const int    nx = 100;
  const int    ny = 100;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  const int coarse_mesh_order = 1;
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const int n_elems_overlap = 2;

  OverlapCoarseFiniteElement over_coarse_fe(&coarse_rect,
                                            coarse_mesh_order,
                                            fine_mesh_order,
                                            n_fine_dofs_per_fine_node,
                                            n_elems_overlap);

  const double h = (x1 - x0) / nx;
  ConstantFunction coef_mass(1.);
  CoefC coef_stif;
  const double abc_layer_width = 20*h;
  const double source_frequency = 20;
  std::vector<double> weights;
  over_coarse_fe.compute_damping_matrix(coef_mass, coef_stif,
                                        abc_layer_width, source_frequency,
                                        x0, x1, y0, y1,
                                        &weights);

  const DoFHandler &dh = over_coarse_fe.dof_handler_fine();

  std::vector<Vector> weights_to_write(1);
  weights_to_write[0] = weights;
  std::vector<std::string> names(1);
  names[0] = "weights";
  const std::string fname = "over_coarse_fe_damping_weights_vector.vtu";
  dh.write_solution_vtu_in_cells(fname, weights_to_write, names);
}


#endif // FEMPLUS_TEST_OVERLAP_COARSE_FINITE_ELEMENT_HPP
