#include "compare_functions.hpp"
#include "femplus/point.hpp"
#include "femplus/dof_handler.hpp"
#include "femplus/finite_element.hpp"
#include <vector>
#include <gtest/gtest.h>

using namespace femplus;



void compare_points(const Mesh &mesh,
                    const std::vector<Point> &points)
{
  // check the number of vertices
  EXPECT_EQ(mesh.n_vertices(), (int)points.size());

  // check the coordinates
  for (int i = 0; i < mesh.n_vertices(); ++i)
    for (int j = 0; j < Point::n_coord; ++j)
      EXPECT_DOUBLE_EQ(mesh.vertex(i).coord(j), points[i].coord(j));
}



void compare_points(const std::vector<Point> &points_0,
                    const std::vector<Point> &points_1)
{
  // check the size of vectors
  EXPECT_EQ(points_0.size(), points_1.size());

  // check the coordinates
  for (unsigned i = 0; i < points_0.size(); ++i)
    for (int j = 0; j < Point::n_coord; ++j)
      EXPECT_DOUBLE_EQ(points_0[i].coord(j), points_1[i].coord(j));
}



void compare_dofs_indices(
    const femplus::DoFHandler &dof_handler,
    const std::vector<std::shared_ptr<femplus::FiniteElement> > &fe_elements)
{
  const FiniteElementMesh *fe_mesh = dof_handler.finite_element_mesh();
  EXPECT_EQ(fe_mesh->n_elements(),
            (int)fe_elements.size());

  for (int i = 0; i < fe_mesh->n_elements(); ++i)
  {
    const FiniteElement *fe_0 = fe_mesh->element(i);
    const FiniteElement *fe_1 = fe_elements[i].get();
    EXPECT_EQ(fe_0->n_dofs(), fe_1->n_dofs());
    for (int d = 0; d < fe_0->n_dofs(); ++d)
      EXPECT_EQ(fe_0->dof_number(d), fe_1->dof_number(d));
  }
}



void compare_connections(const std::vector<std::set<int> > &con_0,
                         const std::vector<std::set<int> > &con_1)
{
  EXPECT_EQ(con_0.size(), con_1.size());
  for (unsigned i = 0; i < con_0.size(); ++i)
  {
    EXPECT_EQ(con_0[i].size(), con_1[i].size());
    EXPECT_TRUE(std::equal(con_0[i].begin(),
                           con_0[i].end(),
                           con_1[i].begin()));
  }
}



void compare_elements(const Mesh &mesh,
                      const std::vector<MeshElementPtr> &elements,
                      const std::vector<Point> &points,
                      int vert_start_from)
{
  // check the number of elements
  EXPECT_EQ(mesh.n_elements(), (int)elements.size());

  // check elements themselves
  for (int i = 0; i < mesh.n_elements(); ++i)
  {
    const MeshElement *mesh_element = mesh.element(i);
    for (int j = 0; j < mesh_element->n_vertices(); ++j)
    {
      // check vertices numbers.
      // we add "vert_start_from" to the vertex number of the mesh element since
      // the mesh elements are created by hand - and sometimes the vertices
      // start from 0, sometimes - from 1 (especially in older tests).
      // but in the program the vertices are always from 0
      EXPECT_EQ(mesh_element->vertex_number(j),
                elements[i]->vertex_number(j) - vert_start_from);

      // check vertices coordinates (once again)
      for (int k = 0; k < Point::n_coord; ++k)
      {
        const int idx0 = mesh_element->vertex_number(j);
        const int idx1 = elements[i]->vertex_number(j) - vert_start_from;
        EXPECT_DOUBLE_EQ(mesh.vertex(idx0).coord(k),
                         points[idx1].coord(k));
      }
    }
  }
}


void compare_edges(const Mesh &mesh,
                   const std::vector<MeshElement*> edges)
{
  EXPECT_EQ((int)edges.size(), mesh.n_edges());
  for (int e = 0; e < mesh.n_edges(); ++e)
  {
    for (int v = 0; v < mesh.edge(e)->n_vertices(); ++v)
      EXPECT_EQ(edges[e]->vertex_number(v), mesh.edge(e)->vertex_number(v));
  }
}



void compare_edges(const DGFiniteElementMesh &mesh,
                   const std::vector<MeshElement*> edges)
{
  EXPECT_EQ((int)edges.size(), mesh.n_edges());
  for (int e = 0; e < mesh.n_edges(); ++e)
  {
    for (int v = 0; v < mesh.edge(e)->n_vertices(); ++v)
      EXPECT_EQ(edges[e]->vertex_number(v), mesh.edge(e)->vertex_number(v));
  }
}



void compare_csr_pattern(const CSRPattern &csr_pattern,
                         int n_points,
                         int *row,
                         int *col)
{
  EXPECT_EQ(csr_pattern.size(), n_points);

  for (int i = 0; i < n_points + 1; ++i)
    EXPECT_EQ(csr_pattern.row(i), row[i]);

  for (int i = 0; i < csr_pattern.row(n_points); ++i)
    EXPECT_EQ(csr_pattern.col(i), col[i]);
}
