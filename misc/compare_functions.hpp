#ifndef TEST_FEMPLUS_COMPARE_FUNCTIONS_HPP
#define TEST_FEMPLUS_COMPARE_FUNCTIONS_HPP

#include "femplus/mesh.hpp"
#include "femplus/mesh_element.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/dof_handler.hpp"

void compare_points(const femplus::Mesh &mesh,
                    const std::vector<femplus::Point> &points);

void compare_points(const std::vector<femplus::Point> &points_0,
                    const std::vector<femplus::Point> &points_1);

void compare_dofs_indices(
    const femplus::DoFHandler &dof_handler,
    const std::vector<std::shared_ptr<femplus::FiniteElement> > &fe_elements);

void compare_connections(const std::vector<std::set<int> > &con_0,
                         const std::vector<std::set<int> > &con_1);

void compare_elements(const femplus::Mesh &mesh,
                      const std::vector<femplus::MeshElementPtr> &elements,
                      const std::vector<femplus::Point> &points,
                      int vertices_start_from = 1);

void compare_edges(const femplus::Mesh &mesh,
                   const std::vector<femplus::MeshElement*> edges);

void compare_edges(const femplus::DGFiniteElementMesh &mesh,
                   const std::vector<femplus::MeshElement*> edges);

void compare_csr_pattern(const femplus::CSRPattern &csr_pattern,
                         int n_points,
                         int *row,
                         int *col);

#endif // TEST_FEMPLUS_COMPARE_FUNCTIONS_HPP
