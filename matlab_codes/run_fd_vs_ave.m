function ret = run_fd_vs_ave(coeffile_lay, coeffile_ave)

% run a comparison of two solutions obtained with FDM
% for a fine scale distributed coefficient, defined in
% 'coeffile_lay' (means that this is a layered distribution),
% and for a fine scale averaged coefficient defined in
% 'coeffile_ave'. both solutions are on a fine scale


% n = 250;
% 
% a = load(coeffile);
% n_values = size(a, 1) * size(a, 2);
% a = reshape(a, sqrt(n_values), n_values / sqrt(n_values));
% a = a';
% a = (a.*a)*(1e-6); % we need velocity^2 and scaled
% a = a(1:end-1, 1:end-1);
% [X,Y] = meshgrid(1/size(a,2)/2 : 1/size(a,2) : 1,...
%                  1/size(a,1)/2 : 1/size(a,1) : 1);
% [X1,Y1] = meshgrid(1/n/2 : 1/n : 1,...
%                    1/n/2 : 1/n : 1);
% a = interp2(X, Y, a, X1, Y1, 'spline');

param = [];
param.N = 25;
param.n = 10;
param.T = 0.4;
param.K = 4000;
param.GAMMA = 1;
param.COEFFILE = coeffile_lay;
param.SOURCE_FREQUENCY = 20;
param.SUPPORT_COEF = 1; % 1 for 250 fine cells, 2 for 500 fine cells
param.SOURCE_CENTER_X = 0.5;
param.SOURCE_CENTER_Y = 0.1;

aa = ones(param.N * param.n);
aa = load_coef_file(coeffile_lay, aa);
a = aa*(1e-6);

% finite difference solution
param.n_fd = param.N * param.n;
Ufd = test_fd_wave(param, a); % (N*n x 1) vector
%Ufd_coarse = zeros(param.N * param.N, 1);
%Ufd_coarse(:, 1) = Ufd(1 : param.n : (param.N*param.n+1)^2, 1);
%norm_fd = norm(Ufd_coarse);

param.COEFFILE = coeffile_ave;
aa = ones(param.N * param.n);
aa = load_coef_file(coeffile_ave, aa);
a = aa*(1e-6);

% averaged coef
Uave = test_fd_wave(param, a); % (N x 1) vector
diff = Uave - Ufd;
rel_err = norm(diff) / norm(Ufd);
fprintf('\n---------\n rel_err = %2.4f \n------------\n\n', rel_err * 100);


[X, Y] = meshgrid(0 : 1/(param.N * param.n) : 1, 0 : 1/(param.N * param.n) : 1);
[~, coefstem, ~] = fileparts(coeffile_lay);
fname = strcat(pwd, '/res/fd_ave_', coefstem, '_diff.vts');
Udiff = reshape(diff, (param.N * param.n + 1), (param.N * param.n + 1));
print_vts(fname, param.N, param.n, X*(1e+3), Y*(1e+3), abs(Udiff), a);



ret = 0;
