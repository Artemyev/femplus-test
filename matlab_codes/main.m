

% this is NOT used for getting results for SEG2014 abstract


%% initialize
clear

N = 2;
n = 16;

Nx = N;
Ny = N;
siz1 = Nx*n;
siz2 = Ny*n;

siz = N*n;

H = 1/N;
h= H/n;

E_per = 0.05;
gamma = 2;

dt = H/40/4/10;

T = .2;

K = T/dt;

F0 = 20;

eig_num = 30;

hH = 1/10;


Af = @(t) (1 - 2*pi^2*F0^2*(t-2/F0)^2)*exp(-pi^2*F0^2*(t-2/F0)^2);

% F = @(x,y)1./(hH)^2*exp(-((x-.5).^2+(y-.5).^2)/(hH)^2);
F = @(x,y) 1-sign(-(x-y-.5)).*sign(-(x-y));
% F = @(x,y)1 + 0*x;




fid1 = fopen('velocity.sq');
aa=fread(fid1,[751 2301],'single');
a=aa/(1e6);

a = a(1:end-1,1:1:end-1);

% a(:) = 1; 

[X,Y] = meshgrid(1/size(a,2)/2:1/size(a,2):1,1/size(a,1)/2:1/size(a,1):1);
[X1,Y1] = meshgrid(1/n/Nx/2:1/n/Nx:1,1/n/Ny/2:1/n/Ny:1);
a = interp2(X,Y,a,X1,Y1,'spline');

rho = 1 +0*X1*Y1;




%% main
tic


mass = conv2(ones(n,n),[1, 1; 1, 1]/4);
mass_line = conv(ones(n,1),[1, 1]/2);

element_num = eig_num +4*n*9;



[Global_A,WW,MM,num_boundary,num_basis,l]= local_problem(N,N,h,a,rho,eig_num,gamma,E_per);

W = sparse( size(Global_A,1) , num_basis(N,N) +(num_boundary(N,N)+eig_num) );
for ii = 1:N
    for jj = 1:N
        
        W( ((ii-1)*N + (jj-1))*(n+1)^2 + (1:(n+1)^2) , num_basis(ii,jj) + (1:(num_boundary(ii,jj)+eig_num)) ) = reshape(WW(:,:,1:(num_boundary(ii,jj)+eig_num),ii,jj),(n+1)^2,(num_boundary(ii,jj)+eig_num));
        
    end
end

A = W'*Global_A*W;
A = (A'+A)/2;

% % load tamp
% % A = AAA;
% % MM = MMM;

f0 = zeros( N^2*(n+1)^2 ,1);

[X,Y] = meshgrid(0:1/(N*n):1);
F1 = F(X,Y)';



vxa0 = -H/2; vya0 = -H/2; vxb0 = H/2; vyb0 = H/2; vxa2 = 0 ; vya2 = 0; 

for i = 1:N
    for j = 1:N
%       
            f0( ((j-1)*N + (i-1))*(n+1)^2 + (1:(n+1)^2) ) = reshape(mass.*F1(1+(j-1)*n:j*n+1,1+(i-1)*n:i*n+1)',(n+1)^2,1)*h^2;

    end
end

f = W'*f0;

  u1 = 0*f;
  u0 = u1;
  U = u0;
  


 u1 = 0*f;
  u0 = u1;
  U = u0;

    AA = MM\A;
    ff = MM\f;
    toc
    tic

  for k = 1:K

U = 2*u1 - u0 - dt^2*(AA*u1-Af((k-1)*dt)*ff);
u2 = u1;
u1 = U;
u0 = u2;

sol = zeros(n+1,n+1,N,N);
sol1 = zeros(siz+1,siz+1);

  end
  toc


   

sol = zeros(n+1,n+1,N,N);
sol1 = zeros(siz+1,siz+1);

for i = 1:N
    for j = 1:N
        for k = 1 : (num_boundary(i,j)+eig_num)
            sol(:,:,i,j) = sol(:,:,i,j) + U( num_basis(i,j) + k)*WW(:,:,k,i,j);
        end
        sol1( (j-1)*n+1:j*n+1 , (i-1)*n+1:i*n+1 ) = sol1( (j-1)*n+1:j*n+1 , (i-1)*n+1:i*n+1 ) + mass.*sol(:,:,i,j);

    end
end
    

