% to launch several program cases with different coefficient files

%clear; 
run_fd_wave('n_50_r_1_a_1_f_10_p_1_T_04_dt_00001', 1); % homogeneous medium with coefficient a = 1 (i.e. rho = 1, kappa = 1)

%clear; run_fd_wave('/u/artemyev/projects/fem2d_acoustic/coef/co_3_ave_08_250.dat');
%clear; run_fd_wave('/u/artemyev/projects/fem2d_acoustic/coef/co_3_bin_24_250.dat');
%clear; run_fd_wave('/u/artemyev/projects/fem2d_acoustic/coef/co_slop_bin_08_250.dat');
%clear; run_fd_wave('/u/artemyev/projects/fem2d_acoustic/coef/co_slop_ave_08_250.dat');
%clear; run_fd_wave('/u/artemyev/projects/fem2d_acoustic/coef/co_slop_bin_24_250.dat');
