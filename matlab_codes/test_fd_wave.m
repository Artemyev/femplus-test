function sol_vector = test_fd_wave(param, coef_a)

% solution of a wave equation with finite difference method
% 'param' contains all necessary parameters,
% 'coef_a' is optional, contains the distribution of a coefficient 'a'

if nargin < 2
    coef_a = 0;
end

n = param.n_fd; % the number of fine cells
h = 1. / n;
T = param.T; % total time
K = T / param.dt; % number of time steps

coeffile = param.COEFFILE;

F0   = param.SOURCE_FREQUENCY;
hH   = param.SUPPORT_COEF * h;
xcen = param.SOURCE_CENTER_X;
ycen = param.SOURCE_CENTER_Y;
F_t = @(t) (1 - 2*pi^2*F0^2*(t-2/F0)^2)*exp(-pi^2*F0^2*(t-2/F0)^2);
F_xy = @(x,y)1./(hH)^2*exp(-((x-xcen).^2+(y-ycen).^2)/(hH)^2);

rho = ones(n);
if coef_a == 0
    aa = ones(n);
    if strcmp(coeffile, '')
        a = aa;
    else
        aa = load_coef_file(coeffile, aa);
        a = aa*(1e-6);
    end
else
    if size(coef_a, 1) == 1 && size(coef_a, 2) == 1
        a = ones(n);
        a = a * coef_a;
    else
        assert(size(coef_a, 1) == n && size(coef_a, 2) == n, 'asd');
        a = coef_a;
    end
end

%create a directory for the results
[~,coefstem,~] = fileparts(coeffile);
% RES_DIR = strcat(pwd, '/res/fd_n', num2str(n), '_T', num2str(T), '_K', num2str(K),...
%                  '_f', num2str(F0), '_P', num2str(param.SUPPORT_COEF),...
%                  '_xc', num2str(xcen), '_yc', num2str(ycen), '_coef', coefstem);
FNAME = strcat(pwd, '/res/fd_', coefstem, '.vts');

% check if necessary directory exists
% ex_code = exist(RES_DIR, 'dir');
% if (ex_code == 0) % directory doesn't exists
%     s = mkdir(RES_DIR); % create the directory
%     assert(s, strcat('Directory ', RES_DIR, ' cannot be created'));
%     ex_code = exist(RES_DIR, 'dir'); % check again
% end
% assert(ex_code == 7, strcat('Directory ', RES_DIR, ' was not created properly'));

fprintf('setups are done\n');

%% solver
[sol_vector, sol] = fd_wave(n, T, K, a, rho, F_xy, F_t);

assert(size(sol, 1) == size(sol, 2) && size(sol, 1) == n+1, 'size of solution is strange');

[X,Y] = meshgrid(0:h:1);
%fname = strcat(RES_DIR, '/fd_solution.vts');
%fprintf('writing in %s\n', fname);

%print_vts(FNAME, n, 1, X*(1e+3), Y*(1e+3), sol*(1e-6), a*(1e+6), 'FDM solution');
print_vts(FNAME, n, 1, X, Y, sol, a, 'FDM solution');

fname_dat = strcat(pwd, '/res/fd_', coefstem, '.dat');
print_dat(fname_dat, sol);


end % function
