function ret = run_fd_wave(coeffile, coefvalue)

% run the FDM solution of wave equation
% for the particular coefficient distribution
% defined in 'coeffile' file

param = [];
param.n_fd = 50;
param.T    = 0.4;
param.dt   = 1e-4;
param.COEFFILE = coeffile;  % for distributed (in heterogeneous medium) values
%param.COEFVALUE = coefvalue;% for homogeneous medium
param.SOURCE_FREQUENCY = 10;
param.SUPPORT_COEF     = 1; % 1 for 250 fine cells, 2 for 500 fine cells
param.SOURCE_CENTER_X  = 0.5;
param.SOURCE_CENTER_Y  = 0.5;

test_fd_wave(param, coefvalue);

ret = 0;
end
