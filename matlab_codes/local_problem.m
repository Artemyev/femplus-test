function [Global_A,UU,MM,num_boundary,num_basis,l]= local_problem(Nx,Ny,h,a,rho,eig_num,gamma,boundary_num)

% this is a very important part of a GMsFEM implementation.
% it is written by Tat Leung


N = Nx;
max_eig = 1e+10;
max_eig_2 = 1e+10;
num_boundary = zeros(Nx,Ny);
num_basis = zeros(Nx,Ny);
rhoa = zeros(size(rho,1)+2,size(rho,2)+2);
rhoa(2:end-1,2:end-1) = rho;
rhoa(2:end-1,1) = rho(:,1);
rhoa(2:end-1,end) = rho(:,end);
rhoa(1,2:end-1) = rho(1,:);
rhoa(end,2:end-1) = rho(end,:);
rho=rhoa;
rhoa = conv2(rho,[1,1;1,1]/4,'valid');


ax = conv2(a,[1,1]/2);
ax(:,1) = a(:,1);
ax(:,end) = a(:,end);
ay = conv2(a,[1;1]/2);
ay(1,:) = a(1,:);
ay(end,:) = a(end,:);

ax(2:end+1,:) = ax;
ay(:,2:end+1) = ay;
ay(:,end+1) = ay(:,end);
ax(end+1,:) = ax(end,:);

H = 1/Ny;
n = H/h;
mass_line = conv(ones(n,1),[1, 1]/2);
mass = conv2(ones(n,n),[1, 1; 1, 1]/4);

element_num = eig_num+n*4;

UU = zeros( (n+1) , (n+1) , element_num , Nx, Ny );
U = zeros( (n+1)^2 , element_num );

MM = sparse(1,1);

j0 = (1:n)';
u0 = (1:(n+3)^2)';

vv = [1:n+1,n*(n+1)+1:(n+1)^2,n+2:n+1:(n-1)*(n+1)+1,2*(n+1):n+1:(n)*(n+1)];
u00 = 1:(n+1)^2;
u00([1:n+1,n*(n+1)+1:(n+1)^2,n+2:n+1:(n-1)*(n+1)+1,2*(n+1):n+1:(n)*(n+1)]) = [];
boundary_x = [1:n+3 , (n+3)*(n+2)+1:(n+3)^2 , (n+3)+1:n+3:(n+1)*(n+3)+1 , 2*(n+3):n+3:(n+2)*(n+3)];
in_u = u0;
in_u(boundary_x) = [];

in_boundary_x = [n+5:2*n+5, ((n+3)*(n+1)+2:(n+3)*(n+1)+n+2) , (2*n+8:n+3:(n+3)*(n)+2) , (3*n+8:n+3:(n+3)*(n)+n+2)];
    

A1 = [               in_u ;            in_u ;                 in_u ;                 in_u ;           in_u];
A2 = [               in_u ;          in_u+1 ;              in_u-1  ;             in_u+n+3 ;       in_u-n-3];


boundary_x00 = (1:(n+1)) + (n+1);
boundary_x0 = 1:(n+1);
boundary_x11 = ((n*(n+1)+1):(n+1)^2) - (n+1);
boundary_x1 = (n*(n+1)+1):(n+1)^2;
boundary_x22 = ((1):(n+1):(n*(n+1)+1)) + 1;
boundary_x2 = (1):(n+1):(n*(n+1)+1);
boundary_x33 = ((n+1):(n+1):(n+1)*(n+1)) - 1;
boundary_x3 = (n+1):(n+1):(n+1)*(n+1);


Global_A = sparse(N^2*(n+1)^2,N^2*(n+1)^2);
l = zeros(element_num,Nx,Ny);
f = zeros((n+3)^2,size(in_boundary_x,2));

f( in_boundary_x , : ) = speye( size(in_boundary_x,2) );
f( boundary_x , : )   = [];

[X_idx,Y_idx] = meshgrid(1:Nx,1:Ny);

mm = reshape(1:(n+1)^2,n+1,n+1);
mm0 = mm(1:n,1:n);
mm1 = mm(2:n+1,1:n);
mm2 = mm(1:n,2:n+1);
mm3 = mm(2:n+1,2:n+1);
Mass_M = sparse([1:n^2 ,1:n^2, 1:n^2, 1:n^2],[mm0(1:n^2),mm1(1:n^2),mm2(1:n^2),mm3(1:n^2)],ones(4*n^2,1)/4);

DD1 = [1:n^2,1:n^2,1:n^2,1:n^2];
DD2 = [mm0(1:n^2),mm1(1:n^2),mm2(1:n^2),mm3(1:n^2)];

DD3 = [-ones(1,n^2),ones(1,n^2),-ones(1,n^2),ones(1,n^2)];
DD4 = [-ones(1,n^2),-ones(1,n^2),ones(1,n^2),ones(1,n^2)];
Dx = sparse(DD1,DD2,DD3/2);
Dy = sparse(DD1,DD2,DD4/2);
num_basis_current =0;
for idx = 1:Nx*Ny
    jj = X_idx(idx);
    ii = Y_idx(idx);


        
        A3 = [ reshape( ((ax( (jj-1)*n + (1:n+1) , (ii-1)*n + (1:n+1) ) + ax( (jj-1)*n + (2:n+2) , (ii-1)*n + (1:n+1) )) + ay( (jj-1)*n + (1:n+1) , (ii-1)*n + (1:n+1) ) + ay( (jj-1)*n + (1:n+1) , (ii-1)*n + (2:n+2) )) , (n+1)^2, 1 );...
               reshape(  -ax( (jj-1)*n + (2:n+2) , (ii-1)*n + (1:n+1) ) , (n+1)^2 , 1 ) ;...
               reshape(  -ax( (jj-1)*n + (1:n+1) , (ii-1)*n + (1:n+1) ) , (n+1)^2 , 1 ) ;...
               reshape(  -ay( (jj-1)*n + (1:n+1) , (ii-1)*n + (2:n+2) ) , (n+1)^2 , 1 ) ;...
               reshape(  -ay( (jj-1)*n + (1:n+1) , (ii-1)*n + (1:n+1) ) , (n+1)^2 , 1 ) ]/h^2;
           
           
           
           A = sparse(A1, A2, A3, (n+3)^2,  (n+3)^2 );
           AA0 = A;
           AA1 = A;
           
           B2 = sparse(1:(n-1)^2,1:(n-1)^2,reshape(rhoa((jj-1)*n+(2:n),(ii-1)*n+(2:n))',(n-1)^2,1));
           B3 = sparse(1:(n)^2,1:(n)^2,reshape(a((jj-1)*n+(1:n),(ii-1)*n+(1:n)),(n)^2,1));
           
           AA0([boundary_x,in_boundary_x],:) = [];
           AA0(:,[boundary_x,in_boundary_x]) = [];
           
           AA1( (n+3+2:2*n+5) + ((n+3+2:2*n+5)-1)*size(AA1,1) ) = AA1( (n+3+2:2*n+5) + ((n+3+2:2*n+5)-1)*size(AA1,1) )  -  ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 1)'/h^2;
           AA1( ((n+1)*(n+3)+2:(n+2)*(n+3)-1) + (((n+1)*(n+3)+2:(n+2)*(n+3)-1)-1)*size(AA1,1) ) = AA1( ((n+1)*(n+3)+2:(n+2)*(n+3)-1) + (((n+1)*(n+3)+2:(n+2)*(n+3)-1)-1)*size(AA1,1) ) - ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+2)'/h^2;
           AA1( (n+3+2:n+3:(n+1)*(n+3)+2) + ((n+3+2:n+3:(n+1)*(n+3)+2)-1)*size(AA1,1) ) = AA1( (n+3+2:n+3:(n+1)*(n+3)+2) + ((n+3+2:n+3:(n+1)*(n+3)+2)-1)*size(AA1,1) ) - ax( (jj-1)*n + (  1) , (ii-1)*n + (1:n+1))/h^2;
           AA1( (2*n+5:n+3:(n+2)*(n+3)-1) + ((2*n+5:n+3:(n+2)*(n+3)-1)-1)*size(AA1,1) ) = AA1( (2*n+5:n+3:(n+2)*(n+3)-1) + ((2*n+5:n+3:(n+2)*(n+3)-1)-1)*size(AA1,1) ) -  ax( (jj-1)*n + (n+2) , (ii-1)*n + (1:n+1))/h^2;

           AA1([boundary_x],:) = [];
           AA1(:,[boundary_x]) = [];
           
           AA1(vv,vv) = AA1(vv,vv)/2;
           
           AA1( (2:n) + ((2:n)-1)*size(AA1,1) )                     = AA1( (2:n) + ((2:n)-1)*size(AA1,1) )  +  ay( (jj-1)*n + (2:n) , (ii-1)*n + 2)'/h^2/2;
           AA1( (n*(n+1)+(2:n)) + ((n*(n+1)+(2:n))-1)*size(AA1,1) ) = AA1( (n*(n+1)+(2:n)) + ((n*(n+1)+(2:n))-1)*size(AA1,1) ) + ay( (jj-1)*n + (2:n) , (ii-1)*n + n+1)'/h^2/2;
           AA1( (n+2:(n+1):(n+1)*n) + ((n+2:(n+1):(n+1)*n)-1)*size(AA1,1) )         = AA1( (n+2:(n+1):(n+1)*n) + ((n+2:(n+1):(n+1)*n)-1)*size(AA1,1) ) + ax( (jj-1)*n + (  2) , (ii-1)*n + (2:n))/h^2/2;
           AA1( (2*(n+1):(n+1):(n+1)*n) + ((2*(n+1):(n+1):(n+1)*n)-1)*size(AA1,1) ) = AA1( (2*(n+1):(n+1):(n+1)*n) + ((2*(n+1):(n+1):(n+1)*n)-1)*size(AA1,1) ) +  ax( (jj-1)*n + (n+1) , (ii-1)*n + (2:n))/h^2/2;
           
           
           A(in_boundary_x,:) = 0;
           A(in_boundary_x,in_boundary_x) = speye(size(in_boundary_x,2));
           A(boundary_x,:) = [];
           A(:,boundary_x) = [];
           
           Uu = A\f;
           
           AAA = ((Uu'*AA1*Uu*h*h)' + Uu'*AA1*Uu*h*h)/2;
           [Uuc,ll] = eig(AAA);
           
           ll = diag(ll);
           
           total_E = sum(ll);
           
           [ll,Xl]=sort(ll);
           %if jj ==1
           %    if ii ==1
           %        plot(ll);
           %    end
           %end
           lll=ll;
           
           Uuc=Uuc(:,Xl);
           
%            ll = find(sum(toeplitz(ll,[ll(1),zeros(1,length(ll)-1)]))> E_per*total_E);
           ll = 4*n-boundary_num;
           num_boundary(idx) = size(Uu,2)-max(ll);

           num_basis(idx) = num_basis_current;
           num_basis_current = num_basis(idx)+num_boundary(idx)+eig_num;
           max_eig = min([max_eig,lll(num_boundary(idx)+1)]);
           Uu0 = Uu*Uuc(:,1:end-max(ll));

           if eig_num>0
           [Ud,ld] = eigs(AA0,B2,eig_num+1,'sm');
           U(u00,1:eig_num) = Ud(:,2:eig_num+1);
           l(1:eig_num+1,ii,jj) = diag(ld);
           max_eig_2 = min([max_eig_2,l(2)]);
           else
               l(1,ii,jj) = 0;
           end
           
           U(:,(eig_num+1):(eig_num+num_boundary(ii,jj))) = Uu0;
           UU(:,:,:,ii,jj) = permute(reshape(full(U(1:(n+1)^2,:)),n+1,n+1,element_num),[1,2,3]);
           
           for k = 1:num_boundary(idx)+eig_num
               for kk = 1:num_boundary(idx)+eig_num
                   
                   MM(num_basis(idx) + k,num_basis(idx) + kk) = sum(sum( mass.*conv2(rho(1+n*(jj-1):n*jj+2,1+n*(ii-1):2+n*ii),[1,1;1,1]/4,'valid').*UU(:,:,kk,ii,jj).*UU(:,:,k,ii,jj) ))*H^2/n^2;
               end
           end

           
           
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + (1:(n+1)^2) , ((ii-1)*N + (jj-1))*(n+1)^2 + (1:(n+1)^2) ) = AA1*h^2;
%            Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + (1:(n+1)^2) , ((ii-1)*N + (jj-1))*(n+1)^2 + (1:(n+1)^2) ) = Dx'*B3*Dx+Dy'*B3*Dy;
           
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)' ))/2;
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'))/2;
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)) ))/2;
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) ))/2;
           
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)' ))/2;
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'))/2;
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)) ))/2;
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) ))/2;
           
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)' + gamma*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)'+ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 1)')/2 ));
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)' + gamma*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'+ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+2)')/2));
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)) + gamma*(ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1))+ax( (jj-1)*n + (  1) , (ii-1)*n + (1:n+1)))/2 ));
           Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) + gamma*(ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1))+ax( (jj-1)*n + (n+2) , (ii-1)*n + (1:n+1)))/2 ));
           if ii ~=1
               Global_A( ((ii-2)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) = Global_A( ((ii-2)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) - sparse(1:n+1,1:n+1,mass_line'.*(-(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 1)'+ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)')/2 + gamma*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)'+ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 1)')/2));
               Global_A( ((ii-2)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 ) = Global_A( ((ii-2)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 ) - sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)'))/2;
               Global_A( ((ii-2)*N + (jj-1))*(n+1)^2 + boundary_x11 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) = Global_A( ((ii-2)*N + (jj-1))*(n+1)^2 + boundary_x11 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) - sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 1)'))/2;
               
           else
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)'));
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)' ))/2;
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x00 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x0 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + 2)' ))/2;
           end
           if ii ~= N
               Global_A( ((ii  )*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) = Global_A( ((ii  )*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) - sparse(1:n+1,1:n+1,mass_line'.*(-(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+2)'+ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)')/2 + gamma*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'+ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+2)')/2));
               Global_A( ((ii  )*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 ) = Global_A( ((ii  )*N + (jj-1))*(n+1)^2 + boundary_x0 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 ) - sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'))/2;
               Global_A( ((ii  )*N + (jj-1))*(n+1)^2 + boundary_x00 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) = Global_A( ((ii  )*N + (jj-1))*(n+1)^2 + boundary_x00 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) - sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+2)'))/2;
           else
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'));
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'))/2;
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x1 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x11 ) + sparse(1:n+1,1:n+1,mass_line'.*(ay( (jj-1)*n + (1:n+1) , (ii-1)*n + n+1)'))/2;
               
           end
           if jj ~= 1
           Global_A( ((ii-1)*N + (jj-2))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) = Global_A( ((ii-1)*N + (jj-2))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) - sparse(1:n+1,1:n+1,mass_line'.*(-(ax( (jj-1)*n + (  1) , (ii-1)*n + (1:n+1))+ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)))/2 + gamma*(ax( (jj-1)*n + (  1) , (ii-1)*n + (1:n+1)) + ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)))/2));
           Global_A( ((ii-1)*N + (jj-2))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 ) = Global_A( ((ii-1)*N + (jj-2))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 ) - sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)) ))/2;
           Global_A( ((ii-1)*N + (jj-2))*(n+1)^2 + boundary_x33 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) = Global_A( ((ii-1)*N + (jj-2))*(n+1)^2 + boundary_x33 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) - sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (  1) , (ii-1)*n + (1:n+1)) ))/2;
           else
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)) ));
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)) ))/2;
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x22 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (  2) , (ii-1)*n + (1:n+1)) ))/2;
           end
           if jj ~= N
           Global_A( ((ii-1)*N + (jj  ))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) = Global_A( ((ii-1)*N + (jj  ))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) - sparse(1:n+1,1:n+1,mass_line'.*(-(ax( (jj-1)*n + (n+2) , (ii-1)*n + (1:n+1))+ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)))/2 + gamma*(ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) + ax( (jj-1)*n + (n+2) , (ii-1)*n + (1:n+1)))/2));
           Global_A( ((ii-1)*N + (jj  ))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 ) = Global_A( ((ii-1)*N + (jj  ))*(n+1)^2 + boundary_x2 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 ) - sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) ))/2;
           Global_A( ((ii-1)*N + (jj  ))*(n+1)^2 + boundary_x22 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) = Global_A( ((ii-1)*N + (jj  ))*(n+1)^2 + boundary_x22 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) - sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (n+2) , (ii-1)*n + (1:n+1)) ))/2;
           else
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) + sparse(1:n+1,1:n+1,mass_line'.*(-ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) ));
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) ))/2;
               Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 ) = Global_A( ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x3 , ((ii-1)*N + (jj-1))*(n+1)^2 + boundary_x33 ) + sparse(1:n+1,1:n+1,mass_line'.*(ax( (jj-1)*n + (n+1) , (ii-1)*n + (1:n+1)) ))/2;
           end
           
           
end


