function ret = print_dat(fname, U)

fid = fopen(fname, 'w');

assert(fid ~= -1, strcat('File ', fname, ' cannot be opened'));

ave_cells = conv2(U, [1, 1; 1, 1]/4);

N = size(ave_cells, 1);
assert(N == size(U, 1)+1, 'err');

fprintf(fid, '%d\n', (N-2)*(N-2));
for i = 2 : N-1
    for j = 2 : N-1
        fprintf(fid, '%2.14e\n', ave_cells(i, j));
    end
end

fclose (fid);
ret = 0;
end % function
