% this is a script to solve
% the wave equation on a coarser grid
% with an averaged coefficient distributed
% according to a von Karman model.
% it looks that it doesn't involve the
% solution with GMsFEM, so I don't
% remember why it's called this way
% (but maybe this comparison ave_vs_gms was meant
% from the beginning)


n = 250;
coeffile = '../vonkarman.dat';

% ======= read and prepare coefficient ===================
a = load(coeffile);
n_values = size(a, 1) * size(a, 2);
a = reshape(a, sqrt(n_values), n_values / sqrt(n_values));
a = a';
a = (a.*a)*(1e-6); % we need velocity^2 and scaled
a = a(1:end-1, 1:end-1);
[X,Y] = meshgrid(1/size(a,2)/2 : 1/size(a,2) : 1,...
                 1/size(a,1)/2 : 1/size(a,1) : 1);
[X1,Y1] = meshgrid(1/n/2 : 1/n : 1,...
                   1/n/2 : 1/n : 1);
a = interp2(X, Y, a, X1, Y1, 'spline');


% ========= averaging ===================================
param = [];
param.N = 125;
param.n = 2;
param.T = 0.4;
param.K = 4000;
param.COEFFILE = coeffile;
param.SOURCE_FREQUENCY = 20;
param.SUPPORT_COEF = 1; % 1 for 250 fine cells, 2 for 500 fine cells
param.SOURCE_CENTER_X = 0.5;
param.SOURCE_CENTER_Y = 0.1;

test_fd_ave(param, a, 0);

% % =====
% param.N = 25;
% param.n = 10;
% test_fd_ave(param, a, 0);
% 
% % =====
% param.N = 50;
% param.n = 5;
% test_fd_ave(param, a, 0);
% 
% % =====
% param.N = 125;
% param.n = 2;
% test_fd_ave(param, a, 0);


% % =========== with no averaging ==============================
% param.T = 0.4;
% param.K = 4000;
% param.COEFFILE = '/u/artemyev/projects/fem2d_acoustic/coef/co_3_ave_08_250.dat';
% param.SOURCE_FREQUENCY = 20;
% param.SUPPORT_COEF = 1; % 1 for 250 fine cells, 2 for 500 fine cells
% param.SOURCE_CENTER_X = 0.5;
% param.SOURCE_CENTER_Y = 0.1;
% param.n = 250;
% test_fd_wave(param);
