function ret = run(coeffile)

% run GMsFEM solutions for
% a specific coefficient distribution
% defined in a 'coeffile' file.
% this is a more recent implementation, 
% and here we also compare the solution obtained
% with GMsFEM with the one obtained with FDM,
% and collect the L2 norm of the relative difference
% between them in a special file



% n = 250;
% 
% a = load(coeffile);
% n_values = size(a, 1) * size(a, 2);
% a = reshape(a, sqrt(n_values), n_values / sqrt(n_values));
% a = a';
% a = (a.*a)*(1e-6); % we need velocity^2 and scaled
% a = a(1:end-1, 1:end-1);
% [X,Y] = meshgrid(1/size(a,2)/2 : 1/size(a,2) : 1,...
%                  1/size(a,1)/2 : 1/size(a,1) : 1);
% [X1,Y1] = meshgrid(1/n/2 : 1/n : 1,...
%                    1/n/2 : 1/n : 1);
% a = interp2(X, Y, a, X1, Y1, 'spline');

param = [];
param.N = 25;
param.n = 10;
param.T = 0.4;
param.K = 4000;
param.GAMMA = 1;
param.COEFFILE = coeffile;
param.SOURCE_FREQUENCY = 20;
param.SUPPORT_COEF = 1; % 1 for 250 fine cells, 2 for 500 fine cells
param.SOURCE_CENTER_X = 0.5;
param.SOURCE_CENTER_Y = 0.1;

aa = ones(param.N * param.n);
aa = load_coef_file(coeffile, aa);
a = aa*(1e-6);

% finite difference solution
param.n_fd = param.N * param.n;
Ufd = test_fd_wave(param, a); % (N*n x 1) vector
norm_fd = norm(Ufd);

% open file to save data about errors
[~, coefstem, ~] = fileparts(coeffile);
fname = strcat(pwd, '/res/fd_gms_N25_n10_', coefstem, '.dat');
fid = fopen(fname, 'w');
assert(fid ~= -1, strcat('File ', fname, ' cannot be opened'));
fprintf(fid, 'nbou\tnint\trel_error in percent\n');

for nint = 7 : 7
    for nbou = 23 : 23
        if nint+nbou == 30
            param.N_BOUNDARY_BF = nbou;
            param.N_INTERIOR_BF = nint;
            % multiscale solution
            Ums = test(param, a); % (N*n x 1) vector
            diff = Ums - Ufd;
            rel_err = norm(diff) / norm_fd;
            fprintf('\n---------\n nbou = %d nint = %d rel_err = %2.4f \n------------\n\n', nbou, nint, rel_err * 100);
            fprintf(fid, '%d\t%d\t%f\n', nbou, nint, rel_err * 100);
        end
    end
end

fclose(fid);

Udiff = reshape(diff, param.N * param.n + 1, param.N * param.n + 1);
fname = strcat(pwd, '/res/fd_gms_23_7_', coefstem ,'_diff.vts');
[X, Y] = meshgrid(0 : 1 / (param.N*param.n) : 1, 0 : 1 / (param.N*param.n) : 1);
print_vts(fname, param.N, param.n, X, Y, abs(Udiff), a);

% 
% % ==========================
% param.N_BOUNDARY_BF = 5;
% test(param);
% 
% 
% 
% % ==========================
% param.N_BOUNDARY_BF = 10;
% test(param);
% 
% 
% 
% % ==========================
% param.N_BOUNDARY_BF = 1;
% param.N_INTERIOR_BF = 5;
% test(param);
% 
% 
% 
% % ==========================
% param.N_BOUNDARY_BF = 5;
% test(param);
% 
% 
% 
% % ==========================
% param.N_BOUNDARY_BF = 10;
% test(param);
% 
% 
% 
% % ==========================
% param.N_BOUNDARY_BF = 1;
% param.N_INTERIOR_BF = 10;
% test(param);
% 
% 
% 
% % ==========================
% param.N_BOUNDARY_BF = 5;
% test(param);
% 
% 
% 
% % ==========================
% param.N_BOUNDARY_BF = 10;
% test(param);
% 
% 
% % % ==========================
% % param.GAMMA = 10;
% % test(param);
% % 
% % 
% % 
% % % ==========================
% % param.GAMMA = 100;
% % test(param);


ret = 0;
end
