function coef_b = load_coef_file(filename, coef_b)

% simple function to load one of two coefficients
% for further usage in a simulation



fid = fopen(filename, 'r');
err = strcat('file ', filename, ' cannot be opened');
assert(fid ~= -1, err);

n_values = fscanf(fid, '%d', 1); % the number of values of coefficients

assert(n_values == size(coef_b, 1) * size(coef_b, 2), 'Dimensions miosmatch');
assert(size(coef_b, 1) == size(coef_b, 2), 'Dimensions miosmatch');

%coef_a = zeros(n_values, 1);
%coef_b = zeros(n_values, 1);

 for i = 1:size(coef_b, 1)
     for j = 1:size(coef_b, 2)
         coef_a = fscanf(fid, '%e', 1); % dummy
         coef_b(i, j) = fscanf(fid, '%e', 1);
     end
 end

%for i = 1:n_values
%    coef_a = fscanf(fid, '%e', 1); % dummy
%    coef_b(i) = fscanf(fid, '%e', 1);
%end

fclose(fid);
