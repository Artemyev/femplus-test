#ifndef TEST_FEMPLUS_DG_DOF_HANDLER_HPP
#define TEST_FEMPLUS_DG_DOF_HANDLER_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/rectangle.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/dg_dof_handler.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/lagrange_finite_element.hpp"
#include "femplus/dg_lagrange_finite_element.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/sym_csr_pattern.hpp"
#include "femplus/boundary_node.hpp"

#include "compare_functions.hpp"
#include "mat_coefficients.hpp"
#include "test_global_matrices.hpp"

#include <fstream>


using namespace femplus;

// =============================================================================
//
// Some important tests related to features specific to DG grids are located in
// the mesh/triangular_dg_fe_mesh.hpp and mesh/rectangular_dg_fe_mesh.hpp files.
// Look there for some examples.
//
// =============================================================================

// =============================================================================
TEST(DGDoFHandler, scalar_rect_distribute_dofs_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  std::shared_ptr<DoFHandler> dof_handler(new DGDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  // compare mesh points
  std::vector<Point> mesh_points;
  mesh_points.push_back(Point(0, 0));
  mesh_points.push_back(Point(1, 0));
  mesh_points.push_back(Point(0, 1));
  mesh_points.push_back(Point(1, 1));
  compare_points(*mesh.get(), mesh_points);
  compare_points(*fe_mesh->base_mesh(), mesh_points);
  compare_points(*dof_handler->finite_element_mesh()->base_mesh(), mesh_points);
  // compare dofs points. they should coincide with the mesh points for this
  // case
  compare_points(dof_handler->dofs(), mesh_points);

  // compare mesh vertices indices describing the mesh elements
  std::vector<MeshElementPtr> mesh_elements;
  mesh_elements.push_back(MeshElementPtr(new Rectangle(0, 1, 2, 3)));
  const int v_start_from = 0;
  compare_elements(*mesh.get(), mesh_elements, mesh_points, v_start_from);
  compare_elements(*fe_mesh->base_mesh(), mesh_elements,
                   mesh_points, v_start_from);
  compare_elements(*dof_handler->finite_element_mesh()->base_mesh(),
                   mesh_elements, mesh_points, v_start_from);

  // compare dofs indices describing the finite elements
  typedef std::shared_ptr<FiniteElement> FEPtr;
  std::vector<FEPtr> fe_elements;
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    fe_elements.push_back(FEPtr(new DGLagrangeFiniteElement(mesh->element(i),
                                                            order,
                                                            n_dofs_per_node)));
  }
  const int n_dofs_per_element = 4;
  const int dofs_array[][n_dofs_per_element] =
  { { 0, 1, 2, 3 } };
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    std::vector<int> dofs_numbers(dofs_array[i],
                                  dofs_array[i] + n_dofs_per_element);
    fe_elements[i]->set_dofs_numbers(dofs_numbers);
  }
  compare_dofs_indices(*dof_handler.get(), fe_elements);
}

// =============================================================================
TEST(DGDoFHandler, scalar_rect_distribute_dofs_1)
{
  const double x0 = -1;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  std::shared_ptr<DoFHandler> dof_handler(new DGDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  // compare mesh points
  std::vector<Point> mesh_points;
  mesh_points.push_back(Point(-1, 0));
  mesh_points.push_back(Point(0, 0));
  mesh_points.push_back(Point(1, 0));
  mesh_points.push_back(Point(-1, 5));
  mesh_points.push_back(Point(0, 5));
  mesh_points.push_back(Point(1, 5));
  mesh_points.push_back(Point(-1, 10));
  mesh_points.push_back(Point(0, 10));
  mesh_points.push_back(Point(1, 10));
  compare_points(*mesh.get(), mesh_points);
  compare_points(*fe_mesh->base_mesh(), mesh_points);
  compare_points(*dof_handler->finite_element_mesh()->base_mesh(), mesh_points);

  // compare dofs points
  std::vector<Point> dofs_points;
  dofs_points.push_back(Point(-1, 0));
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(-1, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(1, 0));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(1, 5));
  dofs_points.push_back(Point(-1, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(-1, 10));
  dofs_points.push_back(Point(0, 10));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(1, 5));
  dofs_points.push_back(Point(0, 10));
  dofs_points.push_back(Point(1, 10));
  compare_points(dof_handler->dofs(), dofs_points);

  // compare mesh vertices indices describing the mesh elements
  std::vector<MeshElementPtr> mesh_elements;
  mesh_elements.push_back(MeshElementPtr(new Rectangle(0, 1, 3, 4)));
  mesh_elements.push_back(MeshElementPtr(new Rectangle(1, 2, 4, 5)));
  mesh_elements.push_back(MeshElementPtr(new Rectangle(3, 4, 6, 7)));
  mesh_elements.push_back(MeshElementPtr(new Rectangle(4, 5, 7, 8)));
  const int v_start_from = 0;
  compare_elements(*mesh.get(),
                   mesh_elements,
                   mesh_points,
                   v_start_from);
  compare_elements(*fe_mesh->base_mesh(),
                   mesh_elements,
                   mesh_points,
                   v_start_from);
  compare_elements(*dof_handler->finite_element_mesh()->base_mesh(),
                   mesh_elements,
                   mesh_points,
                   v_start_from);

  // compare dofs indices describing the finite elements
  typedef std::shared_ptr<FiniteElement> FEPtr;
  std::vector<FEPtr> fe_elements;
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    fe_elements.push_back(FEPtr(new DGLagrangeFiniteElement(mesh->element(i),
                                                            order,
                                                            n_dofs_per_node)));
  }
  const int n_dofs_per_element = 4;
  const int dofs_array[][n_dofs_per_element] =
  { { 0, 1, 2, 3 },
    { 4, 5, 6, 7 },
    { 8, 9, 10, 11 },
    { 12, 13, 14, 15 } };
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    std::vector<int> dofs_numbers(dofs_array[i],
                                  dofs_array[i] + n_dofs_per_element);
    fe_elements[i]->set_dofs_numbers(dofs_numbers);
  }
  compare_dofs_indices(*dof_handler.get(), fe_elements);
}

// =============================================================================
TEST(DGDoFHandler, vector2_rect_distribute_dofs_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector FE with 2 dofs per each node
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  std::shared_ptr<DoFHandler> dof_handler(new DGDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  // compare mesh points. they are the same as for the scalar case
  std::vector<Point> mesh_points;
  mesh_points.push_back(Point(0, 0));
  mesh_points.push_back(Point(1, 0));
  mesh_points.push_back(Point(0, 1));
  mesh_points.push_back(Point(1, 1));
  compare_points(*mesh.get(), mesh_points);
  compare_points(*fe_mesh->base_mesh(), mesh_points);
  compare_points(*dof_handler->finite_element_mesh()->base_mesh(), mesh_points);

  // compare dofs points. they are not the same as for the scalar case.
  // there are 2 dofs per each node
  std::vector<Point> dofs_points;
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(1, 0));
  dofs_points.push_back(Point(1, 0));
  dofs_points.push_back(Point(0, 1));
  dofs_points.push_back(Point(0, 1));
  dofs_points.push_back(Point(1, 1));
  dofs_points.push_back(Point(1, 1));
  compare_points(dof_handler->dofs(), dofs_points);

  // compare mesh vertices indices describing the mesh elements - the same as
  // for the scalar case
  std::vector<MeshElementPtr> mesh_elements;
  mesh_elements.push_back(MeshElementPtr(new Rectangle(0, 1, 2, 3)));
  const int v_start_from = 0;
  compare_elements(*mesh.get(), mesh_elements, mesh_points, v_start_from);
  compare_elements(*fe_mesh->base_mesh(), mesh_elements,
                   mesh_points, v_start_from);
  compare_elements(*dof_handler->finite_element_mesh()->base_mesh(),
                   mesh_elements, mesh_points, v_start_from);

  // compare dofs indices describing the finite elements - not the same as for
  // the scalar case
  typedef std::shared_ptr<FiniteElement> FEPtr;
  std::vector<FEPtr> fe_elements;
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    fe_elements.push_back(FEPtr(new DGLagrangeFiniteElement(mesh->element(i),
                                                            order,
                                                            n_dofs_per_node)));
  }
  const int n_dofs_per_element = 8; // 2 dofs per each of 4 nodes
  const int dofs_array[][n_dofs_per_element] =
  { { 0, 1, 2, 3, 4, 5, 6, 7 } };
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    std::vector<int> dofs_numbers(dofs_array[i],
                                  dofs_array[i] + n_dofs_per_element);
    fe_elements[i]->set_dofs_numbers(dofs_numbers);
  }
  compare_dofs_indices(*dof_handler.get(), fe_elements);
}

// =============================================================================
TEST(DGDoFHandler, vector2_rect_distribute_dofs_1)
{
  const double x0 = -1;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector FE with 2 components
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  std::shared_ptr<DoFHandler> dof_handler(new DGDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  // compare mesh points. they are the same as for the scalar case
  std::vector<Point> mesh_points;
  mesh_points.push_back(Point(-1, 0));
  mesh_points.push_back(Point(0, 0));
  mesh_points.push_back(Point(1, 0));
  mesh_points.push_back(Point(-1, 5));
  mesh_points.push_back(Point(0, 5));
  mesh_points.push_back(Point(1, 5));
  mesh_points.push_back(Point(-1, 10));
  mesh_points.push_back(Point(0, 10));
  mesh_points.push_back(Point(1, 10));
  compare_points(*mesh.get(), mesh_points);
  compare_points(*fe_mesh->base_mesh(), mesh_points);
  compare_points(*dof_handler->finite_element_mesh()->base_mesh(), mesh_points);

  // compare dofs points. they are not the same as for the scalar case. now we
  // have two times more dofs, but every other dof corresponds to the same
  // physical point
  std::vector<Point> dofs_points;
  dofs_points.push_back(Point(-1, 0));
  dofs_points.push_back(Point(-1, 0));
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(-1, 5));
  dofs_points.push_back(Point(-1, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(0, 0));
  dofs_points.push_back(Point(1, 0));
  dofs_points.push_back(Point(1, 0));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(1, 5));
  dofs_points.push_back(Point(1, 5));
  dofs_points.push_back(Point(-1, 5));
  dofs_points.push_back(Point(-1, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(-1, 10));
  dofs_points.push_back(Point(-1, 10));
  dofs_points.push_back(Point(0, 10));
  dofs_points.push_back(Point(0, 10));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(0, 5));
  dofs_points.push_back(Point(1, 5));
  dofs_points.push_back(Point(1, 5));
  dofs_points.push_back(Point(0, 10));
  dofs_points.push_back(Point(0, 10));
  dofs_points.push_back(Point(1, 10));
  dofs_points.push_back(Point(1, 10));
  compare_points(dof_handler->dofs(), dofs_points);

  // compare mesh vertices indices describing the mesh elements. they are the
  // same as for the scalar case
  std::vector<MeshElementPtr> mesh_elements;
  mesh_elements.push_back(MeshElementPtr(new Rectangle(0, 1, 3, 4)));
  mesh_elements.push_back(MeshElementPtr(new Rectangle(1, 2, 4, 5)));
  mesh_elements.push_back(MeshElementPtr(new Rectangle(3, 4, 6, 7)));
  mesh_elements.push_back(MeshElementPtr(new Rectangle(4, 5, 7, 8)));
  const int v_start_from = 0;
  compare_elements(*mesh.get(),
                   mesh_elements,
                   mesh_points,
                   v_start_from);
  compare_elements(*fe_mesh->base_mesh(),
                   mesh_elements,
                   mesh_points,
                   v_start_from);
  compare_elements(*dof_handler->finite_element_mesh()->base_mesh(),
                   mesh_elements,
                   mesh_points,
                   v_start_from);

  // compare dofs indices describing the finite elements. they are not the same
  // as for the scalar case
  typedef std::shared_ptr<FiniteElement> FEPtr;
  std::vector<FEPtr> fe_elements;
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    fe_elements.push_back(FEPtr(new DGLagrangeFiniteElement(mesh->element(i),
                                                            order,
                                                            n_dofs_per_node)));
  }
  const int n_dofs_per_element = 8; // 4 vertices, 2 dofs per vertex
  const int dofs_array[][n_dofs_per_element] =
  { { 0, 1, 2, 3, 4, 5, 6, 7 },
    { 8, 9, 10, 11, 12, 13, 14, 15 },
    { 16, 17, 18, 19, 20, 21, 22, 23 },
    { 24, 25, 26, 27, 28, 29, 30, 31 } };
  for (int i = 0; i < mesh->n_elements(); ++i)
  {
    std::vector<int> dofs_numbers(dofs_array[i],
                                  dofs_array[i] + n_dofs_per_element);
    fe_elements[i]->set_dofs_numbers(dofs_numbers);
  }
  compare_dofs_indices(*dof_handler.get(), fe_elements);
}

// =============================================================================
TEST(DGDoFHandler, tria_distribute_dofs)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_mesh_0.msh"));

  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, 1));

  std::shared_ptr<DoFHandler> dof_handler(new DGDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();

  std::cout << "this test doesn't check anything" << std::endl;
}

// =============================================================================
// edge numeration is in fact performed in DGFiniteElementMesh class, therefore
// this function is tested there
TEST(DGDoFHandler, numerate_edges)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_mesh_0.msh"));

  const int order = 1;
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order));

  DGDoFHandler dof_handler(fe_mesh);
  // exception because dofs were not distributed
  EXPECT_ANY_THROW(dof_handler.numerate_edges());

  dof_handler.distribute_dofs();
  dof_handler.numerate_edges(); // now should be okay
}

// =============================================================================
TEST(DGDoFHandler, scalar_rect_distribute_boudnary_dofs_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  // exception because DG edges were not numerated
  EXPECT_ANY_THROW(dof_handler.distribute_boundary_dofs());
  dof_handler.numerate_edges();
  dof_handler.distribute_boundary_dofs(); // now should be okay

  // compare boundary dofs indices
  const int dofs_ind[] = { 0, 1, 2, 3 };
  std::vector<int> expect_bdofs_indices(dofs_ind, dofs_ind+4);
  std::vector<BoundaryNode> actual_bdofs;
  dof_handler.boundary_dofs(actual_bdofs);
  EXPECT_EQ(actual_bdofs.size(), expect_bdofs_indices.size());
  for (size_t i = 0; i < expect_bdofs_indices.size(); ++i)
    EXPECT_EQ(expect_bdofs_indices[i], actual_bdofs[i].number());
}

// =============================================================================
TEST(DGDoFHandler, scalar_rect_distribute_boundary_dofs_1)
{
  const double x0 = -1;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  // exception because DG edges were not numerated
  EXPECT_ANY_THROW(dof_handler.distribute_boundary_dofs());
  dof_handler.numerate_edges();
  dof_handler.distribute_boundary_dofs(); // now should be okay

  // compare boundary dofs indices
  const int dofs_ind[] = { 0, 1, 2, 4, 5, 7, 8, 10, 11, 13, 14, 15 };
  std::vector<int> expect_bdofs_indices(dofs_ind, dofs_ind+12);
  std::vector<BoundaryNode> actual_bdofs;
  dof_handler.boundary_dofs(actual_bdofs);
  EXPECT_EQ(actual_bdofs.size(), expect_bdofs_indices.size());
  for (size_t i = 0; i < expect_bdofs_indices.size(); ++i)
    EXPECT_EQ(expect_bdofs_indices[i], actual_bdofs[i].number());
}

// =============================================================================
TEST(DGDoFHandler, vector2_rect_distribute_boundary_dofs_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector FE with 2 components
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  // exception because DG edges were not numerated
  EXPECT_ANY_THROW(dof_handler.distribute_boundary_dofs());
  dof_handler.numerate_edges();
  dof_handler.distribute_boundary_dofs(); // now should be okay

  // compare boundary dofs indices
  const int dofs_ind[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
  std::vector<int> expect_bdofs_indices(dofs_ind, dofs_ind+8);
  std::vector<BoundaryNode> actual_bdofs;
  dof_handler.boundary_dofs(actual_bdofs);
  EXPECT_EQ(actual_bdofs.size(), expect_bdofs_indices.size());
  for (size_t i = 0; i < expect_bdofs_indices.size(); ++i)
    EXPECT_EQ(expect_bdofs_indices[i], actual_bdofs[i].number());
}

// =============================================================================
TEST(DGDoFHandler, vector2_rect_distribute_boundary_dofs_1)
{
  const double x0 = -1;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2; // vector FE with 2 components
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  // exception because DG edges were not numerated
  EXPECT_ANY_THROW(dof_handler.distribute_boundary_dofs());
  dof_handler.numerate_edges();
  dof_handler.distribute_boundary_dofs(); // now should be okay

  // compare boundary dofs indices
  const int dofs_ind[] = { 0, 1, 2, 3, 4, 5, 8, 9, 10, 11, 14, 15, 16, 17,
                           20, 21, 22, 23, 26, 27, 28, 29, 30, 31 };
  std::vector<int> expect_bdofs_indices(dofs_ind, dofs_ind+24);
  std::vector<BoundaryNode> actual_bdofs;
  dof_handler.boundary_dofs(actual_bdofs);
  EXPECT_EQ(actual_bdofs.size(), expect_bdofs_indices.size());
  for (size_t i = 0; i < expect_bdofs_indices.size(); ++i)
    EXPECT_EQ(expect_bdofs_indices[i], actual_bdofs[i].number());
}

// =============================================================================
TEST(DGDoFHandler, rect_scalar_build_mass_matrix)
{
  const double x0 = -5;
  const double x1 = 7;
  const double y0 = 8;
  const double y1 = 9;
  const int    nx = 2;
  const int    ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  std::shared_ptr<Pattern> mass_pattern(new CSRPattern(connections));
  std::shared_ptr<PetscMatrix> mass_matrix(new PetscMatrix(*mass_pattern.get()));

  ConstantFunction coef(-3.5);
  dof_handler.build_mass_matrix(coef, *mass_matrix.get());
}

// =============================================================================
TEST(DGDoFHandler, rect_scalar_build_stif_matrix)
{
  const double x0 = -5;
  const double x1 = 7;
  const double y0 = 8;
  const double y1 = 9;
  const int    nx = 2;
  const int    ny = 2;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1; // scalar FE
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  DGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<std::set<int> > connections;
  dof_handler.dg_dofs_connections(connections);
  std::shared_ptr<Pattern> dg_pattern(new CSRPattern(connections));
  std::shared_ptr<PetscMatrix> dg_matrix(new PetscMatrix(*dg_pattern.get()));

  ConstantFunction coef(-3.5);
  dof_handler.build_stiffness_matrix(coef, *dg_matrix.get());
}

// =============================================================================
TEST(DGDoFHandler, triangles_medium_size_mesh)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_mesh_4.msh"));
  const int n_vert = 13;
  const int n_elem = 16;

  EXPECT_EQ(mesh->n_vertices(), n_vert);
  EXPECT_EQ(mesh->n_elements(), n_elem);

  std::vector<std::set<int> > ver_connect;
  mesh->vertices_direct_connections(ver_connect);

  SymCSRPattern pattern(ver_connect);

  const int row[] = { 0, 0, 0, 0, 0, 2, 4, 6, 8, 11, 15, 19, 23, 28 };
  const int col[] = { 0, 1, 1, 2, 2, 3, 0, 3, 4, 5, 6, 3, 6, 7, 8,
                      2, 5, 6, 8, 1, 4, 5, 8, 0, 4, 7, 8, 9 };

  for (int i = 0; i < pattern.size() + 1; ++i)
    EXPECT_EQ(row[i], pattern.row(i));
  for (int i = 0; i < pattern.n_nonzero_elements(); ++i)
    EXPECT_EQ(col[i], pattern.col(i));

  mesh->numerate_edges();
  const int edges[][2] = { { 0, 4 },
                           { 1, 4 },
                           { 1, 5 },
                           { 2, 5 },
                           { 2, 6 },
                           { 3, 6 },
                           { 0, 7 },
                           { 3, 7 },
                           { 4, 8 },
                           { 5, 8 },
                           { 6, 8 },
                           { 3, 9 },
                           { 6, 9 },
                           { 7, 9 },
                           { 8, 9 },
                           { 2, 10 },
                           { 5, 10 },
                           { 6, 10 },
                           { 8, 10 },
                           { 1, 11 },
                           { 4, 11 },
                           { 5, 11 },
                           { 8, 11 },
                           { 0, 12 },
                           { 4, 12 },
                           { 7, 12 },
                           { 8, 12 },
                           { 9, 12 } };
  for (int i = 0; i < mesh->n_edges(); ++i)
    for (int j = 0; j < mesh->edge(i)->n_vertices(); ++j)
      EXPECT_EQ(edges[i][j], mesh->edge(i)->vertex_number(j));

  const int order = 1;
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order));

  const double gamma = 1;
  DGDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();

  EXPECT_EQ(dof_handler.n_dofs(), n_elem * 3);
  for (int i = 0, k = 0; i < fe_mesh->n_elements(); ++i)
  {
    for (int j = 0; j < fe_mesh->element(i)->n_dofs(); ++j, ++k)
      EXPECT_EQ(fe_mesh->element(i)->dof_number(j), k);
  }

  const int exp_dof2ver[] = { 12, 9, 7,
                              8, 9, 12,
                              3, 9, 6,
                              0, 12, 7,
                              2, 10, 5,
                              1, 11, 4,
                              6, 9, 8,
                              6, 8, 10,
                              5, 10, 8,
                              4, 8, 12,
                              5, 8, 11,
                              4, 11, 8,
                              0, 4, 12,
                              1, 5, 11,
                              3, 7, 9,
                              2, 6, 10 };

  std::vector<int> dof_to_ver;
  dof_handler.dofs_to_vertices(dof_to_ver);
  for (int i = 0; i < (int)dof_to_ver.size(); ++i)
  {
    EXPECT_EQ(dof_to_ver[i], exp_dof2ver[i]);
    EXPECT_TRUE(math::norm(dof_handler.dof(i) - mesh->vertex(dof_to_ver[i]))
                < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE);
  }

  dof_handler.numerate_edges();
}

// =============================================================================
//
//                                TEST DG MATRICES
//
// =============================================================================
//
// Mass matrix, vector2, DG, rectangles, coefficient = 1
//
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_coef1_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_coef1_n_2_2_h_5_5.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_coef1_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_coef1_n_2_2_h_3_7.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_coef1_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_coef1_n_10_10_h_5_5.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_coef1_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_coef1_n_10_10_h_3_7.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}

// =============================================================================
//
// Mass matrix, vector2, DG, rectangles, coefficient is not constant
//
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_n_2_2_h_5_5.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 2e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_n_2_2_h_3_7.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 6e-11;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_n_10_10_h_5_5.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 3e-11;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRhoPerElem coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_n_10_10_h_3_7.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}

// =============================================================================
//
// Mass matrix: a special case with a physical coefficient
//
// =============================================================================
TEST(DGDoFHandler, build_mass_matrix_vector2_n_3_3_h_10_10_real_rho)
{
  const double x0 = 0;
  const double x1 = 30;
  const double y0 = 0;
  const double y1 = 30;
  const int    nx = 3;
  const int    ny = 3;
  const double hx = 10;
  const double hy = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 4e-12;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRealRho_3_3 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_n_3_3_h_10_10_real_rho.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node, coef, fname, tolerance);
}

// =============================================================================
//
// Bare stiffness matrix (with no contributions from edges), vector2, DG,
// rectangles, coefficient = 1
//
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_coef1_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_coef1_n_2_2_h_5_5.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           ConstantFunction(coef), fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_coef1_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_coef1_n_2_2_h_3_7.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           ConstantFunction(coef), fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_coef1_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_coef1_n_10_10_h_5_5.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           ConstantFunction(coef), fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_coef1_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;
  const Vector coef(6, 1.0);

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_coef1_n_10_10_h_3_7.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           ConstantFunction(coef), fname, tolerance);
}

// =============================================================================
//
// Bare stiffness matrix (with no contributions from edges), vector2, DG,
// rectangles, coefficient is not constant
//
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_n_2_2_h_5_5)
{
  const double x0 = -1;
  const double x1 = 9;
  const double y0 = 0;
  const double y1 = 10;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_n_2_2_h_5_5.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           coef, fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_n_2_2_h_3_7)
{
  const double x0 = 1;
  const double x1 = 7;
  const double y0 = 0;
  const double y1 = 14;
  const int    nx = 2;
  const int    ny = 2;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_n_2_2_h_3_7.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           coef, fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_n_10_10_h_5_5)
{
  const double x0 = -1;
  const double x1 = 49;
  const double y0 = 0;
  const double y1 = 50;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 5;
  const double hy = 5;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_n_10_10_h_5_5.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           coef, fname, tolerance);
}
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_n_10_10_h_3_7)
{
  const double x0 = 1;
  const double x1 = 31;
  const double y0 = 0;
  const double y1 = 70;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 7;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefElast_v0 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_n_10_10_h_3_7.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           coef, fname, tolerance);
}

// =============================================================================
//
// Bare stiffness matrix (with no contributions from edges):
// A special cases with physical coefficients
//
// =============================================================================
TEST(DGDoFHandler, build_bare_stif_matrix_vector2_n_3_3_h_10_10_real_elast)
{
  const double x0 = 0;
  const double x1 = 30;
  const double y0 = 0;
  const double y1 = 30;
  const int    nx = 3;
  const int    ny = 3;
  const double hx = 10;
  const double hy = 10;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();
  const CoefRealElast_3_3 coef(mesh);

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_bare_S_DG_n_3_3_h_10_10_real_elast.txt";
  test_bare_stif_matrix_DG(mesh, order, n_dofs_per_node,
                           coef, fname, tolerance);
}

// =============================================================================
//
// DG matrix with contributions from interior edges only,
// vector2, DG, rectangles, coefficient = 1
//
// =============================================================================
TEST(DGDoFHandler, build_dg_matrix_edge_only_vector2_coef1_n_2_1_h_1_1)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 1.;
      const double vp = 1.;
      const double vs = 1.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 2;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 2;
  const int    ny = 1;
  const double hx = 1;
  const double hy = 1;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double gamma = 1;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_edge_DG_coef1_n_2_1_h_1_1.txt";

  const bool show_mat = false;
  const bool compare  = true;
  test_dg_matrix_edge_DG(mesh, order, n_dofs_per_node, gamma,
                         CoefC(), fname, tolerance, show_mat, compare);
}

// =============================================================================
TEST(DGDoFHandler, build_dg_matrix_edge_only_vector2_coef1_n_1_2_h_1_1)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 1.;
      const double vp = 1.;
      const double vs = 1.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 2;
  const int    nx = 1;
  const int    ny = 2;
  const double hx = 1;
  const double hy = 1;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double gamma = 1;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_edge_DG_coef1_n_1_2_h_1_1.txt";

  const bool show_mat = false;
  const bool compare  = true;
  test_dg_matrix_edge_DG(mesh, order, n_dofs_per_node, gamma,
                         CoefC(), fname, tolerance, show_mat, compare);
}

// =============================================================================
TEST(DGDoFHandler, build_dg_matrix_edge_only_vector2_n_10_10_h_3_2_gam_25)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 1e+3;
      const double vp = 3e+3;
      const double vs = 2e+3;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 30;
  const double y0 = 0;
  const double y1 = 20;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 3;
  const double hy = 2;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double gamma = 25;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_edge_DG_n_10_10_h_3_2_gam_25.txt";

  const bool show_mat = false;
  const bool compare  = true;
  test_dg_matrix_edge_DG(mesh, order, n_dofs_per_node, gamma,
                         CoefC(), fname, tolerance, show_mat, compare);
}

// =============================================================================
//
// Almost complete DG matrix (there is no contribution from boundary edges),
// vector2, DG, rectangles, coefficient = 1
//
// =============================================================================
TEST(DGDoFHandler, build_dg_matrix_wo_bound_vector2_n_3_3_h_1_1_gam_25)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 2.;
      const double vp = 1.;
      const double vs = 1.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 3;
  const double y0 = 0;
  const double y1 = 3;
  const int    nx = 3;
  const int    ny = 3;
  const double hx = 1;
  const double hy = 1;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double gamma = 25;
  const double tolerance = 1e-14;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_stif_wo_bound_DG_n_3_3_h_1_1_gam_25_rho_2.txt";

  test_dg_matrix_wo_bound_DG(mesh, order, n_dofs_per_node, gamma,
                         CoefC(), fname, tolerance);
}

// =============================================================================
TEST(DGDoFHandler, build_dg_matrix_wo_bound_DG_n_3_3_h_200_200_gam_25)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 2500.;
      const double vp = 3000.;
      const double vs = 2000.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 600;
  const double y0 = 0;
  const double y1 = 600;
  const int    nx = 3;
  const int    ny = 3;
  const double hx = 200.;
  const double hy = 200.;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double gamma = 25;
  const double tolerance = 4e-5;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_stif_wo_bound_DG_n_3_3_h_200_200_gam_25.txt";

  test_dg_matrix_wo_bound_DG(mesh, order, n_dofs_per_node, gamma,
                         CoefC(), fname, tolerance);
}

// =============================================================================
TEST(DGDoFHandler, build_dg_matrix_wo_bound_DG_n_10_10_h_200_200_gam_25)
{
  class CoefC: public Function
  {
  public:
    virtual Vector values(const Point &point) const {
      const double rho = 2500.;
      const double vp = 3000.;
      const double vs = 2000.;
      Vector coefvalues(6);
      coefvalues(0) = rho*vp*vp;
      coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
      coefvalues(2) = 0;
      coefvalues(3) = rho*vp*vp;
      coefvalues(4) = 0;
      coefvalues(5) = rho*vs*vs;
      return coefvalues;
    }
  };

  const double x0 = 0;
  const double x1 = 2000;
  const double y0 = 0;
  const double y1 = 2000;
  const int    nx = 10;
  const int    ny = 10;
  const double hx = 200;
  const double hy = 200;
  const int    order = 1;
  const int    n_dofs_per_node = 2;
  const double gamma = 25;
  const double tolerance = 4e-5;

  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  EXPECT_DOUBLE_EQ((x1-x0)/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_stif_wo_bound_DG_n_10_10_h_200_200_gam_25.txt";

  test_dg_matrix_wo_bound_DG(mesh, order, n_dofs_per_node, gamma,
                         CoefC(), fname, tolerance);
}

#endif // TEST_FEMPLUS_DG_DOF_HANDLER_HPP
