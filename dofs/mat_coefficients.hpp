#ifndef FEMPLUS_TEST_MAT_COEFFICIENTS_HPP
#define FEMPLUS_TEST_MAT_COEFFICIENTS_HPP

#include "femplus/function.hpp"
#include "femplus/mesh.hpp"

using namespace femplus;

// =============================================================================
class CoefRhoPerElem: public Function
{
public:
  CoefRhoPerElem(std::shared_ptr<Mesh> mesh)
    : _mesh(mesh)
  { }

  double value(const Point &point) const
  {
    // index of the mesh element containing the point
    const int elem = _mesh->element_contains_point(point);
    require(elem >= 0 && elem < _mesh->n_elements(),
            "The number of the element containing a given point (" +
            d2s<int>(elem) + ") is out of range [0, " +
            d2s<int>(_mesh->n_elements()) + ")");

    return (elem+1)*1e+3;
  }
private:
  std::shared_ptr<Mesh> _mesh;
};
// =============================================================================
class CoefElast_v0: public Function
{
public:
  CoefElast_v0(std::shared_ptr<Mesh> mesh)
    : _mesh(mesh)
  { }

  Vector values(const Point &point) const
  {
    // index of the mesh element containing the point
    const int elem = _mesh->element_contains_point(point);
    require(elem >= 0 && elem < _mesh->n_elements(),
            "The number of the element containing a given point (" +
            d2s<int>(elem) + ") is out of range [0, " +
            d2s<int>(_mesh->n_elements()) + ")");

    Vector coef(6);
    coef(0) = (elem+1)*1e+3;
    coef(1) = (elem+2)*1e+3;
    coef(2) = (elem+3)*1e+3;
    coef(3) = (elem+4)*1e+3;
    coef(4) = (elem+5)*1e+3;
    coef(5) = (elem+6)*1e+3;

    return coef;
  }
private:
  std::shared_ptr<Mesh> _mesh;
};
// =============================================================================
class CoefRealRho_3_3: public Function
{
public:
  CoefRealRho_3_3(std::shared_ptr<Mesh> mesh)
    : _mesh(mesh)
  {
    require(_mesh->n_elements() == 9,
            "This class is implemented for a mesh with 9 elements, but the "
            "given mesh has " + d2s<int>(_mesh->n_elements()) + " elements");
  }

  double value(const Point &point) const
  {
    // distribution of rho per elements
    const double rho[] =
    { 1.9648885351992767e+03, 1.9571669482429454e+03, 1.1418863386272153e+03,
      1.1576130816775483e+03, 1.4853756487228411e+03, 1.4217612826262748e+03,
      1.9705927817606157e+03, 1.8002804688888002e+03, 1.9157355251890672e+03 };
    // number of the mesh element containing the point
    const int elem = _mesh->element_contains_point(point);
    require(elem >= 0 && elem < _mesh->n_elements(),
            "The number of the element containing a given point (" +
            d2s<int>(elem) + ") is out of range [0, " +
            d2s<int>(_mesh->n_elements()) + ")");

    return rho[elem];
  }
private:
  std::shared_ptr<Mesh> _mesh;
};
// =============================================================================
class CoefRealElast_3_3: public Function
{
public:
  CoefRealElast_3_3(std::shared_ptr<Mesh> mesh)
    : _mesh(mesh)
  {
    require(_mesh->n_elements() == 9,
            "This class is implemented for a mesh with 9 elements, but the "
            "given mesh has " + d2s<int>(_mesh->n_elements()) + " elements");
  }

  Vector values(const Point &point) const
  {
    // distribution of rho per elements
    const double c11[] =
    { 2.8011843935507927e+10, 2.7901763447541080e+10, 1.6278960020737307e+10,
      1.6503163615011463e+10, 2.1175812323322567e+10, 2.0268913197376701e+10,
      2.8093164815335690e+10, 2.5665158420572514e+10, 2.7311108794200378e+10 };
    const double c13[] =
    { 2.2043691498693645e+10, 2.1957064558947956e+10, 1.2810594455791004e+10,
      1.2987029640724079e+10, 1.6664132327891809e+10, 1.5950455477527653e+10,
      2.2107686300015995e+10, 2.0196986524369671e+10, 2.1492253709991104e+10 };
    const double c15[] =
    { 5.0411180259072638e+09, 5.0213075224121008e+09, 2.9296235903819838e+09,
      2.9699721223519177e+09, 3.8108797643633213e+09, 3.6476707467059708e+09,
      5.0557528408850355e+09, 4.6187995709811058e+09, 4.9150110634250708e+09 };
    const double c33[] =
    { 5.3850128173378418e+10, 5.3638508816935814e+10, 3.1294765373685188e+10,
      3.1725775639071224e+10, 4.0708502104027931e+10, 3.8965074063912216e+10,
      5.4006459895487785e+10, 4.9338846586460236e+10, 5.2503030950436607e+10 };
    const double c35[] =
    { 1.7335484537305195e+10, 1.7267359832576656e+10, 1.0074440667762629e+10,
      1.0213191902800787e+10, 1.3104919759691599e+10, 1.2543673744937052e+10,
      1.7385810994144661e+10, 1.5883208473232395e+10, 1.6901825716598820e+10 };
    const double c55[] =
    { 1.6541905355708908e+10, 1.6476899245520296e+10, 9.6132556133178673e+09,
      9.7456551313728580e+09, 1.2505006242685417e+10, 1.1969452798109951e+10,
      1.6589927981447182e+10, 1.5156111197457584e+10, 1.6128098452685457e+10 };
    // number of the mesh element containing the point
    const int elem = _mesh->element_contains_point(point);
    require(elem >= 0 && elem < _mesh->n_elements(),
            "The number of the element containing a given point (" +
            d2s<int>(elem) + ") is out of range [0, " +
            d2s<int>(_mesh->n_elements()) + ")");

    Vector coef(6);
    coef(0) = c11[elem];
    coef(1) = c13[elem];
    coef(2) = c15[elem];
    coef(3) = c33[elem];
    coef(4) = c35[elem];
    coef(5) = c55[elem];
    return coef;
  }
private:
  std::shared_ptr<Mesh> _mesh;
};


#endif // FEMPLUS_TEST_MAT_COEFFICIENTS_HPP
