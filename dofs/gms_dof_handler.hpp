#ifndef FEMPLUS_TEST_GMS_DOF_HANDLER_HPP
#define FEMPLUS_TEST_GMS_DOF_HANDLER_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/gms_dof_handler.hpp"
#include "femplus/mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/overlap_coarse_fe_mesh.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/coarse_finite_element.hpp"
#include "femplus/dense_pattern.hpp"

#include "test_global_matrices.hpp"

using namespace femplus;


// =============================================================================
//
//                     TEST GMsFEM MATRICES as DG MATRICES, i.e.
//                      with fine mesh 1x1 inside coarse cells
//
// =============================================================================
//
// Mass matrix, vector2, GMsFEM, rectangles, coefficient = 1
//
#if 0 // uncomment
// =============================================================================
TEST(GMsDoFHandler, build_mass_matrix_vector2_coef1_n_2_2_h_3_7)
{
  const double x0 = 0;
  const double x1 = 6;
  const double y0 = 0;
  const double y1 = 14;
  const int    Nx = 2;
  const int    Ny = 2;
  const int    nx = 1;
  const int    ny = 1;
  const double hx = 3;
  const double hy = 7;
  const int fe_coarse_order = 1;
  const int fe_fine_order = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const int n_overlap_elements = 1;
  const double gamma = 0.9;
  const double tolerance = 1e-14;

  const double rho = 1.;
  const double vp  = 3.;
  const double vs  = 2.;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  std::shared_ptr<RectangularMesh> coarse_base(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  std::shared_ptr<CoarseMesh> coarse_mesh(new CoarseRectangularMesh(coarse_base, nx, ny));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();

  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    fe_coarse_order,
                                    fe_fine_order,
                                    n_fine_dofs_per_fine_node));

  FunctionPtr coef_mass(new ConstantFunction(rho));
  FunctionPtr coef_stif(new Isotropic(rho, vp, vs));
  std::vector<FunctionPtr> coefficients(2);
  coefficients[0] = coef_mass;
  coefficients[1] = coef_stif;
  fe_mesh->compute_matrices(coefficients);

  EXPECT_DOUBLE_EQ((x1-x0)/Nx/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/Ny/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_coef1_n_2_2_h_3_7.txt";

  test_mass_matrix_GMsFEM(*fe_mesh.get(),
                          *coef_mass.get(), fname, tolerance);
}


// =============================================================================
TEST(GMsDoFHandler_overlap, build_mass_matrix_vector2_coef1_n_2_2_h_3_7)
{
  const double x0 = 0;
  const double x1 = 6;
  const double y0 = 0;
  const double y1 = 14;
  const int    Nx = 2;
  const int    Ny = 2;
  const int    nx = 1;
  const int    ny = 1;
  const double hx = 3;
  const double hy = 7;
  const int fe_coarse_order = 1;
  const int fe_fine_order = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const int n_overlap_elements = 1;
  const double gamma = 0.9;
  const double tolerance = 1e-14;

  const double rho = 1.;
  const double vp  = 3.;
  const double vs  = 2.;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  std::shared_ptr<RectangularMesh> coarse_base(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  std::shared_ptr<CoarseMesh> coarse_mesh(new CoarseRectangularMesh(coarse_base, nx, ny));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();

  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new OverlapCoarseFEMesh(coarse_mesh,
                                fe_coarse_order,
                                fe_fine_order,
                                n_fine_dofs_per_fine_node,
                                n_overlap_elements));

  FunctionPtr coef_mass(new ConstantFunction(rho));
  FunctionPtr coef_stif(new Isotropic(rho, vp, vs));
  std::vector<FunctionPtr> coefficients(2);
  coefficients[0] = coef_mass;
  coefficients[1] = coef_stif;
  fe_mesh->compute_matrices(coefficients);

  EXPECT_DOUBLE_EQ((x1-x0)/Nx/nx, hx);
  EXPECT_DOUBLE_EQ((y1-y0)/Ny/ny, hy);
  const std::string fname = TESTFILES_DIR +
                            "/kai_global_M_DG_coef1_n_2_2_h_3_7.txt";
  test_mass_matrix_DG(mesh, order, n_dofs_per_node,
                      ConstantFunction(1.), fname, tolerance);
}

//==============================================================================
TEST(GMsDoFHandler_overlap, write_solution_vtu_in_vert)
{
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 2;
  const int _n_coarse_y = 2;
  const int _n_fine_x = 2;
  const int _n_fine_y = 2;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const int _n_overlap_elements = 1;
  const double gamma = 0.9;
  const double rho = 1000.;
  const double vp  = 3000.;
  const double vs  = 2000.;
  const int _n_boundary_bf = 1;
  const int _n_interior_bf = 1;
  Eigensolver eigensolver_boundary = LAPACK;
  Eigensolver eigensolver_interior = JADAMILU;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();

  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new OverlapCoarseFEMesh(coarse_mesh,
                                FE_ORDER_COARSE,
                                FE_ORDER_FINE,
                                n_fine_dofs_per_fine_node,
                                _n_overlap_elements));

  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior);

  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();

  const int N = _n_coarse_x*_n_coarse_y*(_n_fine_x+1)*(_n_fine_y+1);
  std::vector<Vector> solutions(n_fine_dofs_per_fine_node, Vector(N));
  for (int i = 0; i < N; ++i)
  {
    for (int j = 0; j < n_fine_dofs_per_fine_node; ++j)
      solutions[j](i) = (double)j;
  }

  std::vector<std::string> names(n_fine_dofs_per_fine_node);
  for (int j = 0; j < n_fine_dofs_per_fine_node; ++j)
    names[j] = "sol_" + d2s<int>(j);

  const std::string fname = "test_write_vtu.vtu";
  dof_handler.write_solution_vtu_in_vert(fname, solutions, names);
}

// =============================================================================
//
// Let's compare my global mass and DG matrices with Kai's ones. To do this we
// take Kai's R-matrices corresponding to each coarse element.
//
// =============================================================================
TEST(GMsDoFHandler, mass_matrix_vector)
{
  // firstly we compute everything like usual. then we'll replace computed R
  // matrices with the ones computed in Matlab code

  // R-matrices were computed for specific parameters. we use the same
  // parameters here
  const double _x_beg = 0.;
  const double _x_end = 250.;
  const double _y_beg = 0.;
  const double _y_end = 250.;
  const int _n_coarse_x = 5;
  const int _n_coarse_y = 5;
  const int _n_fine_x = 5;
  const int _n_fine_y = 5;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const double rho = 1e+3;
  const double vp  = 3e+3;
  const double vs  = 2e+3;
  const int _n_boundary_bf = 5;
  const int _n_interior_bf = 5;
  Eigensolver eigensolver_boundary = JADAMILU;
  Eigensolver eigensolver_interior = JADAMILU;
  const double gamma = 0.5;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    FE_ORDER_COARSE,
                                    FE_ORDER_FINE,
                                    n_fine_dofs_per_fine_node));
  t1 = get_wall_time();
  std::cout << "build finite element mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. now we replace our R-matrices by the ones computed in Matlab code
  for (int i = 0; i < _n_coarse_y; ++i)
  {
    for (int j = 0; j < _n_coarse_x; ++j)
    {
      const int num = i*_n_coarse_x + j;

      std::string fname = TESTFILES_DIR + "/R/R_" + d2s<int>(num) + ".txt";
      fe_mesh->element(num)->read_R(fname);

//      fname = TESTFILES_DIR + "/R/M_fine_" + d2s<int>(num) + ".txt";
//      fe_mesh->element(num)->read_M(fname);
    }
  }

  // 6. build mass matrix
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr _mass(new PetscMatrix(mass_pattern));
  dof_handler.build_mass_matrix(*coef_mass.get(), *_mass.get());

  // 7. read the mass matrix computed in Matlab code, and compare it with ours
  const std::string fname = TESTFILES_DIR + "/R/M_coarse.txt";
  const double tol = 1e-12;
  const bool absolute = true;
  compare_matrices(*_mass.get(), fname, tol, absolute);
}
#endif

//==============================================================================
TEST(GMsDoFHandler, solution_representation_vector2)
{
  // firstly we compute everything like usual. then we'll replace computed R
  // matrices with the ones computed in Matlab code

  // R-matrices were computed for specific parameters. we use the same
  // parameters here
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const double rho = 2.5e+3;
  const double vp  = 1e+3;
  const double vs  = 1e+3;
  const int _n_boundary_bf = 10;
  const int _n_interior_bf = 10;
  Eigensolver eigensolver_boundary = JADAMILU;
  Eigensolver eigensolver_interior = JADAMILU;
  const double gamma = 0.5;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    FE_ORDER_COARSE,
                                    FE_ORDER_FINE,
                                    n_fine_dofs_per_fine_node));
  t1 = get_wall_time();
  std::cout << "build finite element mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool bound_edge_mass_matrix = true;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         bound_edge_mass_matrix);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. now we replace our R-matrices by the ones computed in Matlab code
  const std::string R_dir = "/u/artemyev/projects/gmsfem_kaigao/sources_no_oversamp/"
                            "R_nb" + d2s<int>(_n_boundary_bf) + "_ni" +
                            d2s<int>(_n_interior_bf) + "/";
  fe_mesh->read_R(R_dir);

  // 6. now read the coarse-scale solution obtained in Matlab code
  const std::string U_filename = R_dir + "/u_coarse.txt";
  std::ifstream in(U_filename.c_str());
  require(in, "File " + U_filename + " can't be opened");
  int n_;
  in >> n_;
  require(n_ == dof_handler.n_dofs(), "n_ is wrong");
  Vector u_coarse(n_);
  for (int i = 0; i < n_; ++i)
    in >> u_coarse(i);
  in.close();

  // 7. write the solution as if it was obtained in a normal way
  dof_handler.write_coarsescale_solution_vtu("artificial_solution_2.vtu",
                                             u_coarse, "name");
}
#if 0 // it takes too long
//==============================================================================
TEST(GMsDoFHandler, solver_M_K_F_R_vector2)
{
  // firstly we compute everything like usual. then we'll replace computed R
  // matrices with the ones computed in Matlab code

  // R-matrices were computed for specific parameters. we use the same
  // parameters here
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const double rho = 2.5e+3;
  const double vp  = 1e+3;
  const double vs  = 1e+3;
  const int _n_boundary_bf = 10;
  const int _n_interior_bf = 10;
  Eigensolver eigensolver_boundary = LAPACK;
  Eigensolver eigensolver_interior = LAPACK;
  const double gamma = 0.5;
  const double _time_end = 0.15;
  const int nt = 500;
  const double dt = _time_end / nt;
  const double f = 10.;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  class Ricker: public Function
  {
  public:
    Ricker(double f_) : f(f_) { }
    double value(const Point &p) const {
      const double t = _time;
      const double t0 = 1./f;
      const double pi = math::PI;
      const double part = pi*pi*f*f*(t-t0)*(t-t0);
      const double R = (1. - 2.*part) * exp(-part);
      return R;
    }
  private:
    double f; // source frequency
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    FE_ORDER_COARSE,
                                    FE_ORDER_FINE,
                                    n_fine_dofs_per_fine_node));
  t1 = get_wall_time();
  std::cout << "build finite element mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. now we replace our R-matrices by the ones computed in Matlab code
  const std::string R_dir = "/u/artemyev/projects/gmsfem_kaigao/sources_no_oversamp/"
                            "R_nb" + d2s<int>(_n_boundary_bf) + "_ni" +
                            d2s<int>(_n_interior_bf) + "/";
  fe_mesh->read_R(R_dir);

  // 6. read the coarse scale RHS
  const std::string F_filename = R_dir + "/F_coarse.txt";
  std::ifstream in(F_filename.c_str());
  require(in, "File " + F_filename + " can't be opened");
  int n_;
  in >> n_;
  require(n_ == dof_handler.n_dofs(), "n_ is wrong");
  Vector globalRHS(n_);
  for (int i = 0; i < n_; ++i)
    in >> globalRHS(i);
  in.close();
  std::cout << "globalRHS matrix" << std::endl;

  // 7. read the coarse scale DG matrix
  const std::string K_filename = R_dir + "/K_coarse.txt";
  in.open(K_filename.c_str());
  require(in, "File " + K_filename + " can't be opened");
  int N_;
  in >> N_;
  require(N_ == dof_handler.n_dofs(), "n_ is wrong");
  DensePattern DG_pat(N_, N_);
  PetscMatrixPtr dg_mat(new PetscMatrix(DG_pat));
  double value;
  for (int i = 0; i < N_; ++i) {
    for (int j = 0; j < N_; ++j) {
      in >> value;
      dg_mat->insert_value(i, j, value);
    }
  }
  in.close();
  dg_mat->final_assembly();
  std::cout << "Dg matrix" << std::endl;

  // 8. read and combine the coarse scale mass matrix
  const std::string M_filename = R_dir + "/M_coarse.txt";
  fe_mesh->read_M(M_filename);
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr _mass(new PetscMatrix(mass_pattern));
  dof_handler.build_mass_matrix(*coef_mass.get(), *_mass.get());
  _mass->final_assembly();
  t1 = get_wall_time();
  std::cout << "build mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 9. inv_N
  const int N = dof_handler.n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr _inv_N(new PetscMatrix(dense));
  dof_handler.build_inv_N_matrix(dt, *_inv_N.get());
  _inv_N->final_assembly();

  // 10. P matrix
  PetscMatrixPtr _P(new PetscMatrix(dense));
  dof_handler.build_P_matrix(dt, *_P.get());
  _P->final_assembly();

  // 11. online stage
  Vector solution(N, 0.);   // numerical solution on the current (n-th) time step
  Vector solution_1(N, 0.); // numerical solution on the (n-1)-th time step
  Vector solution_2(N, 0.); // numerical solution on the (n-2)-th time step

  Ricker ricker(f); // Ricker wavelet - RHS distribution in time

  for (int time_step = 2; time_step <= nt; ++time_step)
  {
    const double time = time_step * dt; // current time

    ricker.set_time(time - dt);
    const double r = ricker.value(Point()) * 1e+6; // point doesn't matter - only time

    const Vector tmp1 = Mv(dg_mat, solution_1); // K U_1
    const Vector tmp2 = Mv(_P,     solution_2); // P U_2
    const Vector tmp3 = Mv(_mass,  solution_1); // M U_1
    // 2M U_1 - P U_2 - dt^2 (K U_1 - F)
    const Vector tmp4 = 2.*tmp3 - tmp2 - dt*dt*(tmp1 - r*globalRHS);

    solution = Mv(_inv_N, tmp4);

    // reassign the solutions on the previuos time steps
    solution_2 = solution_1;
    solution_1 = solution;

    if (time_step % 10 == 0)
    {
      std::cout << "time step " << time_step
                << " norm(solution) = " << math::L2_norm(solution)
                << " norm(gl_rhs) = " << math::L2_norm(globalRHS) << std::endl;

      const std::string fname = "elas_3_rect_sol_N_" + d2s<int>(_n_coarse_x) +
                                "_n_" + d2s<int>(_n_fine_x) +
                                "_t" + d2s<int>(time_step) + ".vtu";
      dof_handler.write_coarsescale_solution_vtu(fname,
                                                 solution,
                                                 "coarse_solution");
    }
  } // time loop
}
#endif // it takes too long

#if 0 // it takes some time (not too long)
//==============================================================================
TEST(GMsDoFHandler, solver_K_F_R_vector2)
{
  // firstly we compute everything like usual. then we'll replace computed R
  // matrices with the ones computed in Matlab code

  // R-matrices were computed for specific parameters. we use the same
  // parameters here
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const double rho = 2.5e+3;
  const double vp  = 1e+3;
  const double vs  = 1e+3;
  const int _n_boundary_bf = 10;
  const int _n_interior_bf = 10;
  Eigensolver eigensolver_boundary = LAPACK;
  Eigensolver eigensolver_interior = LAPACK;
  const double gamma = 0.5;
  const double _time_end = 0.15;
  const int nt = 500;
  const double dt = _time_end / nt;
  const double f = 10.;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  class Ricker: public Function
  {
  public:
    Ricker(double f_) : f(f_) { }
    double value(const Point &p) const {
      const double t = _time;
      const double t0 = 1./f;
      const double pi = math::PI;
      const double part = pi*pi*f*f*(t-t0)*(t-t0);
      const double R = (1. - 2.*part) * exp(-part);
      return R;
    }
  private:
    double f; // source frequency
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    FE_ORDER_COARSE,
                                    FE_ORDER_FINE,
                                    n_fine_dofs_per_fine_node));
  t1 = get_wall_time();
  std::cout << "build finite element mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. now we replace our R-matrices by the ones computed in Matlab code
  const std::string R_dir = "/u/artemyev/projects/gmsfem_kaigao/sources_no_oversamp/"
                            "R_nb" + d2s<int>(_n_boundary_bf) + "_ni" +
                            d2s<int>(_n_interior_bf) + "/";
  fe_mesh->read_R(R_dir);

  // 6. read the coarse scale RHS
  const std::string F_filename = R_dir + "/F_coarse.txt";
  std::ifstream in(F_filename.c_str());
  require(in, "File " + F_filename + " can't be opened");
  int n_;
  in >> n_;
  require(n_ == dof_handler.n_dofs(), "n_ is wrong");
  Vector globalRHS(n_);
  for (int i = 0; i < n_; ++i)
    in >> globalRHS(i);
  in.close();
  std::cout << "globalRHS matrix" << std::endl;

  // 7. read the coarse scale DG matrix
  const std::string K_filename = R_dir + "/K_coarse.txt";
  in.open(K_filename.c_str());
  require(in, "File " + K_filename + " can't be opened");
  int N_;
  in >> N_;
  require(N_ == dof_handler.n_dofs(), "n_ is wrong");
  DensePattern DG_pat(N_, N_);
  PetscMatrixPtr dg_mat(new PetscMatrix(DG_pat));
  double value;
  for (int i = 0; i < N_; ++i) {
    for (int j = 0; j < N_; ++j) {
      in >> value;
      dg_mat->insert_value(i, j, value);
    }
  }
  in.close();
  dg_mat->final_assembly();
  std::cout << "Dg matrix" << std::endl;

  // 8. build the coarse scale mass matrix
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr _mass(new PetscMatrix(mass_pattern));
  dof_handler.build_mass_matrix(*coef_mass.get(), *_mass.get());
  _mass->final_assembly();
  t1 = get_wall_time();
  std::cout << "build mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 9. inv_N
  const int N = dof_handler.n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr _inv_N(new PetscMatrix(dense));
  dof_handler.build_inv_N_matrix(dt, *_inv_N.get());
  _inv_N->final_assembly();

  // 10. P matrix
  PetscMatrixPtr _P(new PetscMatrix(dense));
  dof_handler.build_P_matrix(dt, *_P.get());
  _P->final_assembly();

  // 11. online stage
  Vector solution(N, 0.);   // numerical solution on the current (n-th) time step
  Vector solution_1(N, 0.); // numerical solution on the (n-1)-th time step
  Vector solution_2(N, 0.); // numerical solution on the (n-2)-th time step

  Ricker ricker(f); // Ricker wavelet - RHS distribution in time

  for (int time_step = 2; time_step <= nt; ++time_step)
  {
    const double time = time_step * dt; // current time

    ricker.set_time(time - dt);
    const double r = ricker.value(Point()) * 1e+6; // point doesn't matter - only time

    const Vector tmp1 = Mv(dg_mat, solution_1); // K U_1
    const Vector tmp2 = Mv(_P,     solution_2); // P U_2
    const Vector tmp3 = Mv(_mass,  solution_1); // M U_1
    // 2M U_1 - P U_2 - dt^2 (K U_1 - F)
    const Vector tmp4 = 2.*tmp3 - tmp2 - dt*dt*(tmp1 - r*globalRHS);

    solution = Mv(_inv_N, tmp4);

    // reassign the solutions on the previuos time steps
    solution_2 = solution_1;
    solution_1 = solution;

    if (time_step % 10 == 0)
    {
      std::cout << "time step " << time_step
                << " norm(solution) = " << math::L2_norm(solution)
                << " norm(gl_rhs) = " << math::L2_norm(globalRHS) << std::endl;

      const std::string fname = "elas_4_rect_sol_N_" + d2s<int>(_n_coarse_x) +
                                "_n_" + d2s<int>(_n_fine_x) +
                                "_t" + d2s<int>(time_step) + ".vtu";
      dof_handler.write_coarsescale_solution_vtu(fname,
                                                 solution,
                                                 "coarse_solution");
    }
  } // time loop
}
#endif // it takes some time (not too long)

#if 0 //  it takes some time (not too long)
//==============================================================================
TEST(GMsDoFHandler, solver_K_R_vector2)
{
  // firstly we compute everything like usual. then we'll replace computed R
  // matrices with the ones computed in Matlab code

  // R-matrices were computed for specific parameters. we use the same
  // parameters here
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const double rho = 2.5e+3;
  const double vp  = 1e+3;
  const double vs  = 1e+3;
  const int _n_boundary_bf = 10;
  const int _n_interior_bf = 10;
  Eigensolver eigensolver_boundary = LAPACK;
  Eigensolver eigensolver_interior = LAPACK;
  const double gamma = 0.5;
  const double _time_end = 0.15;
  const int nt = 500;
  const double dt = _time_end / nt;
  const double f = 10.;
  const double hx = (_x_end-_x_beg)/(_n_coarse_x*_n_fine_x);
  const double h = 7*hx;
  require(2*h < (_x_end-_x_beg), "Too large source support");
  const double xc = 0.5*(_x_end-_x_beg);
  const double yc = 0.5*(_y_end-_y_beg);
  const double theta = 0.5*math::PI;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  class SourceGaussElasticRectangle: public Function
  {
  public:
    SourceGaussElasticRectangle(double h_, double xc_, double yc_, double theta_)
      : h(h_), xc(xc_), yc(yc_), theta(theta_) { }
    Vector values(const Point &p) const {
      const double x = p.x();
      const double y = p.y();
      const double Px = cos(theta);
      const double Py = sin(theta);
#if 1 // from my understanding
      const double G = exp(-((x-xc)*(x-xc)+(y-yc)*(y-yc))/(h*h));
#else // from Kai's code
      double G = 0;
      const double tmp = (x-xc)*(x-xc) + (y-yc)*(y-yc);
      const double dist_to_center = sqrt(tmp);
      if (dist_to_center <= 2.*h)
        G = exp(-tmp/10.0);
#endif
      Vector source(2);
      source(0) = G * Px;
      source(1) = G * Py;
      return source;
    }
  private:
    // source support, x-coord of center, y-coord of center, polar angle
    double h, xc, yc, theta;
  };

  class Ricker: public Function
  {
  public:
    Ricker(double f_) : f(f_) { }
    double value(const Point &p) const {
      const double t = _time;
      const double t0 = 1./f;
      const double pi = math::PI;
      const double part = pi*pi*f*f*(t-t0)*(t-t0);
      const double R = (1. - 2.*part) * exp(-part);
      return R;
    }
  private:
    double f; // source frequency
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    FE_ORDER_COARSE,
                                    FE_ORDER_FINE,
                                    n_fine_dofs_per_fine_node));
  t1 = get_wall_time();
  std::cout << "build finite element mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. now we replace our R-matrices by the ones computed in Matlab code
  const std::string R_dir = "/u/artemyev/projects/gmsfem_kaigao/sources_no_oversamp/"
                            "R_nb" + d2s<int>(_n_boundary_bf) + "_ni" +
                            d2s<int>(_n_interior_bf) + "/";
  fe_mesh->read_R(R_dir);

  // 6. build the coarse scale RHS
  SourceGaussElasticRectangle rhs_function(h, xc, yc, theta);
  Vector globalRHS;
  dof_handler.build_rhs(rhs_function, globalRHS); // with Kai's approach
  std::cout << "globalRHS matrix" << std::endl;

  // 7. read the coarse scale DG matrix
  const std::string K_filename = R_dir + "/K_coarse.txt";
  std::ifstream in(K_filename.c_str());
  require(in, "File " + K_filename + " can't be opened");
  int N_;
  in >> N_;
  require(N_ == dof_handler.n_dofs(), "n_ is wrong");
  DensePattern DG_pat(N_, N_);
  PetscMatrixPtr dg_mat(new PetscMatrix(DG_pat));
  double value;
  for (int i = 0; i < N_; ++i) {
    for (int j = 0; j < N_; ++j) {
      in >> value;
      dg_mat->insert_value(i, j, value);
    }
  }
  in.close();
  dg_mat->final_assembly();
  std::cout << "Dg matrix" << std::endl;

  // 8. build the coarse scale mass matrix
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr _mass(new PetscMatrix(mass_pattern));
  dof_handler.build_mass_matrix(*coef_mass.get(), *_mass.get());
  _mass->final_assembly();
  t1 = get_wall_time();
  std::cout << "build mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 9. inv_N
  const int N = dof_handler.n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr _inv_N(new PetscMatrix(dense));
  dof_handler.build_inv_N_matrix(dt, *_inv_N.get());
  _inv_N->final_assembly();

  // 10. P matrix
  PetscMatrixPtr _P(new PetscMatrix(dense));
  dof_handler.build_P_matrix(dt, *_P.get());
  _P->final_assembly();

  // 11. online stage
  Vector solution(N, 0.);   // numerical solution on the current (n-th) time step
  Vector solution_1(N, 0.); // numerical solution on the (n-1)-th time step
  Vector solution_2(N, 0.); // numerical solution on the (n-2)-th time step

  Ricker ricker(f); // Ricker wavelet - RHS distribution in time

  for (int time_step = 2; time_step <= nt; ++time_step)
  {
    const double time = time_step * dt; // current time

    ricker.set_time(time - dt);
    const double r = ricker.value(Point()) * 1e+6; // point doesn't matter - only time

    const Vector tmp1 = Mv(dg_mat, solution_1); // K U_1
    const Vector tmp2 = Mv(_P,     solution_2); // P U_2
    const Vector tmp3 = Mv(_mass,  solution_1); // M U_1
    // 2M U_1 - P U_2 - dt^2 (K U_1 - F)
    const Vector tmp4 = 2.*tmp3 - tmp2 - dt*dt*(tmp1 - r*globalRHS);

    solution = Mv(_inv_N, tmp4);

    // reassign the solutions on the previuos time steps
    solution_2 = solution_1;
    solution_1 = solution;

    if (time_step % 10 == 0)
    {
      std::cout << "time step " << time_step
                << " norm(solution) = " << math::L2_norm(solution)
                << " norm(gl_rhs) = " << math::L2_norm(globalRHS) << std::endl;

      const std::string fname = "elas_5_rect_sol_N_" + d2s<int>(_n_coarse_x) +
                                "_n_" + d2s<int>(_n_fine_x) +
                                "_t" + d2s<int>(time_step) + ".vtu";
      dof_handler.write_coarsescale_solution_vtu(fname,
                                                 solution,
                                                 "coarse_solution");
    }
  } // time loop
}
#endif // it takes some time (not too long)

//==============================================================================
TEST(GMsDoFHandler, solver_R_vector2)
{
  // firstly we compute everything like usual. then we'll replace computed R
  // matrices with the ones computed in Matlab code

  // R-matrices were computed for specific parameters. we use the same
  // parameters here
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const double rho = 2.5e+3;
  const double vp  = 1e+3;
  const double vs  = 1e+3;
  const int _n_boundary_bf = 10;
  const int _n_interior_bf = 10;
  Eigensolver eigensolver_boundary = LAPACK;
  Eigensolver eigensolver_interior = LAPACK;
  const double gamma = 0.5;
  const double _time_end = 0.15;
  const int nt = 500;
  const double dt = _time_end / nt;
  const double f = 10.;
  const double hx = (_x_end-_x_beg)/(_n_coarse_x*_n_fine_x);
  const double h = 7*hx;
  require(2*h < (_x_end-_x_beg), "Too large source support");
  const double xc = 0.5*(_x_end-_x_beg);
  const double yc = 0.5*(_y_end-_y_beg);
  const double theta = 0.5*math::PI;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  class SourceGaussElasticRectangle: public Function
  {
  public:
    SourceGaussElasticRectangle(double h_, double xc_, double yc_, double theta_)
      : h(h_), xc(xc_), yc(yc_), theta(theta_) { }
    Vector values(const Point &p) const {
      const double x = p.x();
      const double y = p.y();
      const double Px = cos(theta);
      const double Py = sin(theta);
#if 1 // from my understanding
      const double G = exp(-((x-xc)*(x-xc)+(y-yc)*(y-yc))/(h*h));
#else // from Kai's code
      double G = 0;
      const double tmp = (x-xc)*(x-xc) + (y-yc)*(y-yc);
      const double dist_to_center = sqrt(tmp);
      if (dist_to_center <= 2.*h)
        G = exp(-tmp/10.0);
#endif
      Vector source(2);
      source(0) = G * Px;
      source(1) = G * Py;
      return source;
    }
  private:
    // source support, x-coord of center, y-coord of center, polar angle
    double h, xc, yc, theta;
  };

  class Ricker: public Function
  {
  public:
    Ricker(double f_) : f(f_) { }
    double value(const Point &p) const {
      const double t = _time;
      const double t0 = 1./f;
      const double pi = math::PI;
      const double part = pi*pi*f*f*(t-t0)*(t-t0);
      const double R = (1. - 2.*part) * exp(-part);
      return R;
    }
  private:
    double f; // source frequency
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    FE_ORDER_COARSE,
                                    FE_ORDER_FINE,
                                    n_fine_dofs_per_fine_node));
  t1 = get_wall_time();
  std::cout << "build finite element mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool bound_edge_mass_matrix = true;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         bound_edge_mass_matrix);

  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. now we replace our R-matrices by the ones computed in Matlab code
  const std::string R_dir = "/u/artemyev/projects/gmsfem_kaigao/sources_no_oversamp/"
                            "R_nb" + d2s<int>(_n_boundary_bf) + "_ni" +
                            d2s<int>(_n_interior_bf) + "/";
  fe_mesh->read_R(R_dir);

  // 6. build the coarse scale RHS
  SourceGaussElasticRectangle rhs_function(h, xc, yc, theta);
  Vector globalRHS;
  dof_handler.build_rhs_Kai(rhs_function, globalRHS); // with Kai's approach
  std::cout << "globalRHS matrix" << std::endl;

  // 7. build the coarse scale DG matrix
  std::vector<std::set<int> > connections;
  dof_handler.dg_dofs_connections(connections);
  CSRPattern dg_pattern(connections);
  PetscMatrixPtr dg_mat(new PetscMatrix(dg_pattern));
  dof_handler.build_stiffness_matrix(*coef_stif.get(), *dg_mat.get());
  dof_handler.add_interior_edges_matrices(*coef_stif.get(), *dg_mat.get());
  dg_mat->final_assembly();
  std::cout << "Dg matrix" << std::endl;

  // 8. build the coarse scale mass matrix
  connections.clear();
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr _mass(new PetscMatrix(mass_pattern));
  dof_handler.build_mass_matrix(*coef_mass.get(), *_mass.get());
  _mass->final_assembly();
  t1 = get_wall_time();
  std::cout << "build mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 9. inv_N
  const int N = dof_handler.n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr _inv_N(new PetscMatrix(dense));
  dof_handler.build_inv_N_matrix(dt, *_inv_N.get());
  _inv_N->final_assembly();

  // 10. P matrix
  PetscMatrixPtr _P(new PetscMatrix(dense));
  dof_handler.build_P_matrix(dt, *_P.get());
  _P->final_assembly();

  // 11. online stage
  Vector solution(N, 0.);   // numerical solution on the current (n-th) time step
  Vector solution_1(N, 0.); // numerical solution on the (n-1)-th time step
  Vector solution_2(N, 0.); // numerical solution on the (n-2)-th time step

  Ricker ricker(f); // Ricker wavelet - RHS distribution in time

  for (int time_step = 2; time_step <= nt; ++time_step)
  {
    const double time = time_step * dt; // current time

    ricker.set_time(time - dt);
    const double r = ricker.value(Point()) * 1e+6; // point doesn't matter - only time

    const Vector tmp1 = Mv(dg_mat, solution_1); // K U_1
    const Vector tmp2 = Mv(_P,     solution_2); // P U_2
    const Vector tmp3 = Mv(_mass,  solution_1); // M U_1
    // 2M U_1 - P U_2 - dt^2 (K U_1 - F)
    const Vector tmp4 = 2.*tmp3 - tmp2 - dt*dt*(tmp1 - r*globalRHS);

    solution = Mv(_inv_N, tmp4);

    // reassign the solutions on the previuos time steps
    solution_2 = solution_1;
    solution_1 = solution;

    if (time_step % 10 == 0)
    {
      std::cout << "time step " << time_step
                << " norm(solution) = " << math::L2_norm(solution)
                << " norm(gl_rhs) = " << math::L2_norm(globalRHS) << std::endl;

      const std::string fname = "elas_6_rect_sol_N_" + d2s<int>(_n_coarse_x) +
                                "_n_" + d2s<int>(_n_fine_x) +
                                "_t" + d2s<int>(time_step) + ".vtu";
      dof_handler.write_coarsescale_solution_vtu(fname,
                                                 solution,
                                                 "coarse_solution");
    }
  } // time loop
}

#endif // FEMPLUS_TEST_GMS_DOF_HANDLER_HPP
