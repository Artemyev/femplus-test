#ifndef FEMPLUS_TEST_CG_OVERLAP_DOF_HANDLER_HPP
#define FEMPLUS_TEST_CG_OVERLAP_DOF_HANDLER_HPP

#include "config.hpp"
#include "femplus/cg_overlap_dof_handler.hpp"
#include "femplus/mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/coarse_rectangle.hpp"
#include "femplus/overlap_coarse_element.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/finite_element.hpp"
#include "gtest/gtest.h"

using namespace femplus;

// =============================================================================
TEST(CGOverlapDoFHandler, scalar_distribute_dofs_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 1;
  const int ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  CGOverlapDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  EXPECT_EQ(dof_handler.n_dofs(), 4);
  EXPECT_EQ(dof_handler.n_boundary_dofs(), 4);

  const int dofs[][4] = { { 0, 1, 2, 3 } };
  const int b_dofs[] = { 0, 1, 2, 3 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler.n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler.boundary_dof_number(bd), b_dofs[bd]);
}

// =============================================================================
TEST(CGOverlapDoFHandler, vector2_distribute_dofs_0)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 1;
  const int ny = 1;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGOverlapDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();

  EXPECT_EQ(dof_handler.n_dofs(), 8);
  EXPECT_EQ(dof_handler.n_boundary_dofs(), 8);

  const int dofs[][8] = { { 0, 1, 2, 3, 4, 5, 6, 7 } };
  const int b_dofs[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    for (int d = 0; d < fe_mesh->element(el)->n_dofs(); ++d)
      EXPECT_EQ(fe_mesh->element(el)->dof_number(d), dofs[el][d]);
  }

  for (int bd = 0; bd < dof_handler.n_boundary_dofs(); ++bd)
    EXPECT_EQ(dof_handler.boundary_dof_number(bd), b_dofs[bd]);
}

// =============================================================================
TEST(CGOverlapDoFHandler, scalar_extract)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  // we create an overlapping coarse element which is based on the given
  // coarse element and extends the fine mesh from there on the given number of
  // cells. we first define the bigger coarse element, and then we build the
  // fine mesh
  const int n_elems_overlap = 1;
  OverlapCoarseElement over_coarse_rect(coarse_rect, n_elems_overlap);
  over_coarse_rect.build_fine_mesh();

  // create a new fine scale finite element mesh based on the fine mesh
  // of the overlapping coarse element
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 1; // scalar FE
  std::shared_ptr<FiniteElementMesh> overlap_fe_mesh(
        new LagrangeMesh(over_coarse_rect.fine_mesh(),
                         fine_mesh_order,
                         n_fine_dofs_per_fine_node));
  // we create a special CG dof handler handling overlapping dofs
  CGOverlapDoFHandler overlap_dof_handler(overlap_fe_mesh);
  overlap_dof_handler.distribute_dofs();
  overlap_dof_handler.extract_original_dofs();
  expect(overlap_dof_handler.n_dofs() > 0,
         "The fine scale degrees of freedom on the overlapping mesh are not "
         "initialized (distributed)");
  expect(overlap_dof_handler.n_boundary_dofs() > 0,
         "The fine scale boundary degrees of freedom on the overlapping mesh "
         "are not known");

  // extract the mesh for the original domain
  std::shared_ptr<Mesh> orig_mesh(over_coarse_rect.extract_original_mesh());
  coarse_rect.set_fine_mesh(orig_mesh);
  // then we create a new finite element mesh based on a fine mesh of the coarse
  // element
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(orig_mesh,
                               fine_mesh_order,
                               n_fine_dofs_per_fine_node));
  // then we create a standard (CG) dof handler for this fine scale finite
  // element mesh
  CGDoFHandler dof_handler_fine(fe_mesh);
  // and finally we initialize (distribute) all degrees of freedom in the
  // computational domain
  dof_handler_fine.distribute_dofs();
  expect(dof_handler_fine.n_dofs() > 0,
         "The fine scale degrees of freedom are not initialized (distributed)");
  expect(dof_handler_fine.n_boundary_dofs() > 0,
         "The fine scale boundary degrees of freedom are not known");

  // and finally we check that the degrees of freedom from the finite element
  // built on top of the original mesh coincide with the degrees of freedom in
  // the same region but as a part of overlapping finite element mesh
  overlap_dof_handler.check_original_dofs(dof_handler_fine);

  Vector overlap_solution(overlap_dof_handler.n_dofs(), -1.);
  const double val[] = { 7.5, 8.5, -2.5, 1.5 };
  overlap_solution(5) = val[0];
  overlap_solution(6) = val[1];
  overlap_solution(9) = val[2];
  overlap_solution(10)= val[3];

  Vector extracted = overlap_dof_handler.extract(overlap_solution);
  EXPECT_EQ(extracted.size(), dof_handler_fine.n_dofs());
  for (int i = 0; i < extracted.size(); ++i)
    EXPECT_DOUBLE_EQ(extracted(i), val[i]);
}

// =============================================================================
TEST(CGOverlapDoFHandler, vector2_extract)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int    nx = 1;
  const int    ny = 1;
  std::shared_ptr<Mesh> mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));

  const int number = 0;
  Rectangle rect;
  CoarseRectangle coarse_rect(number, mesh, rect);

  // we create an overlapping coarse element which is based on the given
  // coarse element and extends the fine mesh from there on the given number of
  // cells. we first define the bigger coarse element, and then we build the
  // fine mesh
  const int n_elems_overlap = 1;
  OverlapCoarseElement over_coarse_rect(coarse_rect, n_elems_overlap);
  over_coarse_rect.build_fine_mesh();

  // create a new fine scale finite element mesh based on the fine mesh
  // of the overlapping coarse element
  const int fine_mesh_order = 1;
  const int n_fine_dofs_per_fine_node = 2; // vector 2D FE
  std::shared_ptr<FiniteElementMesh> overlap_fe_mesh(
        new LagrangeMesh(over_coarse_rect.fine_mesh(),
                         fine_mesh_order,
                         n_fine_dofs_per_fine_node));
  // we create a special CG dof handler handling overlapping dofs
  CGOverlapDoFHandler overlap_dof_handler(overlap_fe_mesh);
  overlap_dof_handler.distribute_dofs();
  overlap_dof_handler.extract_original_dofs();
  expect(overlap_dof_handler.n_dofs() > 0,
         "The fine scale degrees of freedom on the overlapping mesh are not "
         "initialized (distributed)");
  expect(overlap_dof_handler.n_boundary_dofs() > 0,
         "The fine scale boundary degrees of freedom on the overlapping mesh "
         "are not known");

  // extract the mesh for the original domain
  std::shared_ptr<Mesh> orig_mesh(over_coarse_rect.extract_original_mesh());
  coarse_rect.set_fine_mesh(orig_mesh);
  // then we create a new finite element mesh based on a fine mesh of the coarse
  // element
  std::shared_ptr<FiniteElementMesh>
      fe_mesh(new LagrangeMesh(orig_mesh,
                               fine_mesh_order,
                               n_fine_dofs_per_fine_node));
  // then we create a standard (CG) dof handler for this fine scale finite
  // element mesh
  CGDoFHandler dof_handler_fine(fe_mesh);
  // and finally we initialize (distribute) all degrees of freedom in the
  // computational domain
  dof_handler_fine.distribute_dofs();
  expect(dof_handler_fine.n_dofs() > 0,
         "The fine scale degrees of freedom are not initialized (distributed)");
  expect(dof_handler_fine.n_boundary_dofs() > 0,
         "The fine scale boundary degrees of freedom are not known");

  // and finally we check that the degrees of freedom from the finite element
  // built on top of the original mesh coincide with the degrees of freedom in
  // the same region but as a part of overlapping finite element mesh
  overlap_dof_handler.check_original_dofs(dof_handler_fine);

  Vector overlap_solution(overlap_dof_handler.n_dofs(), -1.);
  const double val[] = { 7.5, 8.5, -2.5, 1.5, 0, 4.3, 7.1, 14.7 };
  overlap_solution(10) = val[0];
  overlap_solution(11) = val[1];
  overlap_solution(12) = val[2];
  overlap_solution(13) = val[3];
  overlap_solution(18) = val[4];
  overlap_solution(19) = val[5];
  overlap_solution(20) = val[6];
  overlap_solution(21) = val[7];

  Vector extracted = overlap_dof_handler.extract(overlap_solution);
  EXPECT_EQ(extracted.size(), dof_handler_fine.n_dofs());
  for (int i = 0; i < extracted.size(); ++i)
    EXPECT_DOUBLE_EQ(extracted(i), val[i]);
}

// =============================================================================
TEST(CGOverlapDoFHandler, build_mass_matrix_scalar)
{
  double x0 = 0;
  double x1 = 16;
  double y0 = 0;
  double y1 = 18;
  const int nx = 8;
  const int ny = 9;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler cg_dof_handler(fe_mesh);
  cg_dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  cg_dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  cg_dof_handler.build_mass_matrix(ConstantFunction(5.), mass);
  mass.final_assembly();

  x0 = 6;
  x1 = 10;
  y0 = 6;
  y1 = 12;
  const int ex_nx = 2;
  const int ex_ny = 3;
  const int n_over = 3;
  const int OVER_DOMAIN = 101;
  MeshPtr ex_mesh(new RectangularMesh(x0, x1, y0, y1, ex_nx, ex_ny));
  ex_mesh->build_in_extended_domain(n_over, OVER_DOMAIN);

  FEMeshPtr ex_fe_mesh(new LagrangeMesh(ex_mesh, order, n_dofs_per_node));

  CGOverlapDoFHandler ex_dof_handler(ex_fe_mesh);
  ex_dof_handler.distribute_dofs();

  connections.clear();
  ex_dof_handler.dofs_connections(connections);
  CSRPattern ex_pattern(connections);
  PetscMatrix ex_mass(ex_pattern);
  ex_dof_handler.build_mass_matrix(ConstantFunction(5.), ex_mass);
  ex_mass.final_assembly();

  // these two matrices should coincide
  const int N = (nx+1)*(ny+1);
  EXPECT_EQ(mass.size(), N);
  EXPECT_EQ(ex_mass.size(), N);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      EXPECT_DOUBLE_EQ(mass(i, j), ex_mass(i, j));
}

// =============================================================================
TEST(CGOverlapDoFHandler, build_mass_matrix_vector2)
{
  double x0 = 0;
  double x1 = 100;
  double y0 = 0;
  double y1 = 6;
  const int nx = 10;
  const int ny = 6;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler cg_dof_handler(fe_mesh);
  cg_dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  cg_dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix mass(pattern);
  cg_dof_handler.build_mass_matrix(ConstantFunction(5.), mass);
  mass.final_assembly();

  x0 = 20;
  x1 = 80;
  y0 = 2;
  y1 = 4;
  const int ex_nx = 6;
  const int ex_ny = 2;
  const int n_over = 2;
  const int OVER_DOMAIN = 101;
  MeshPtr ex_mesh(new RectangularMesh(x0, x1, y0, y1, ex_nx, ex_ny));
  ex_mesh->build_in_extended_domain(n_over, OVER_DOMAIN);

  FEMeshPtr ex_fe_mesh(new LagrangeMesh(ex_mesh, order, n_dofs_per_node));

  CGOverlapDoFHandler ex_dof_handler(ex_fe_mesh);
  ex_dof_handler.distribute_dofs();

  connections.clear();
  ex_dof_handler.dofs_connections(connections);
  CSRPattern ex_pattern(connections);
  PetscMatrix ex_mass(ex_pattern);
  ex_dof_handler.build_mass_matrix(ConstantFunction(5.), ex_mass);
  ex_mass.final_assembly();

  // these two matrices should coincide
  const int N = 2*(nx+1)*(ny+1);
  EXPECT_EQ(mass.size(), N);
  EXPECT_EQ(ex_mass.size(), N);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      EXPECT_DOUBLE_EQ(mass(i, j), ex_mass(i, j));
}

// =============================================================================
TEST(CGOverlapDoFHandler, build_stif_matrix_scalar)
{
  double x0 = 0;
  double x1 = 16;
  double y0 = 0;
  double y1 = 18;
  const int nx = 8;
  const int ny = 9;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler cg_dof_handler(fe_mesh);
  cg_dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  cg_dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix stif(pattern);
  cg_dof_handler.build_stiffness_matrix(ConstantFunction(50.), stif);
  stif.final_assembly();

  x0 = 6;
  x1 = 10;
  y0 = 6;
  y1 = 12;
  const int ex_nx = 2;
  const int ex_ny = 3;
  const int n_over = 3;
  const int OVER_DOMAIN = 101;
  MeshPtr ex_mesh(new RectangularMesh(x0, x1, y0, y1, ex_nx, ex_ny));
  ex_mesh->build_in_extended_domain(n_over, OVER_DOMAIN);

  FEMeshPtr ex_fe_mesh(new LagrangeMesh(ex_mesh, order, n_dofs_per_node));

  CGOverlapDoFHandler ex_dof_handler(ex_fe_mesh);
  ex_dof_handler.distribute_dofs();

  connections.clear();
  ex_dof_handler.dofs_connections(connections);
  CSRPattern ex_pattern(connections);
  PetscMatrix ex_stif(ex_pattern);
  ex_dof_handler.build_stiffness_matrix(ConstantFunction(50.), ex_stif);
  ex_stif.final_assembly();

  // these two matrices should coincide
  const int N = (nx+1)*(ny+1);
  EXPECT_EQ(stif.size(), N);
  EXPECT_EQ(ex_stif.size(), N);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      EXPECT_DOUBLE_EQ(stif(i, j), ex_stif(i, j));
}

// =============================================================================
TEST(CGOverlapDoFHandler, build_stif_matrix_vector2)
{
  class CoefStif: public Function
  {
  public:
    virtual Vector values(const Point &p) const
    {
      Vector v(6);
      v(0) = -1.; v(1) = 15.5; v(2) = 1.234; v(3) = 1.; v(4) = 3.; v(5) = 11.;
      return v;
    }
  };

  double x0 = 0;
  double x1 = 100;
  double y0 = 0;
  double y1 = 6;
  const int nx = 10;
  const int ny = 6;
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order, n_dofs_per_node));

  CGDoFHandler cg_dof_handler(fe_mesh);
  cg_dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  cg_dof_handler.dofs_connections(connections);
  CSRPattern pattern(connections);
  PetscMatrix stif(pattern);
  cg_dof_handler.build_stiffness_matrix(CoefStif(), stif);
  stif.final_assembly();

  x0 = 20;
  x1 = 80;
  y0 = 2;
  y1 = 4;
  const int ex_nx = 6;
  const int ex_ny = 2;
  const int n_over = 2;
  const int OVER_DOMAIN = 101;
  MeshPtr ex_mesh(new RectangularMesh(x0, x1, y0, y1, ex_nx, ex_ny));
  ex_mesh->build_in_extended_domain(n_over, OVER_DOMAIN);

  FEMeshPtr ex_fe_mesh(new LagrangeMesh(ex_mesh, order, n_dofs_per_node));

  CGOverlapDoFHandler ex_dof_handler(ex_fe_mesh);
  ex_dof_handler.distribute_dofs();

  connections.clear();
  ex_dof_handler.dofs_connections(connections);
  CSRPattern ex_pattern(connections);
  PetscMatrix ex_stif(ex_pattern);
  ex_dof_handler.build_stiffness_matrix(CoefStif(), ex_stif);
  ex_stif.final_assembly();

  // these two matrices should coincide
  const int N = 2*(nx+1)*(ny+1);
  EXPECT_EQ(stif.size(), N);
  EXPECT_EQ(ex_stif.size(), N);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      EXPECT_DOUBLE_EQ(stif(i, j), ex_stif(i, j));
}


#endif // FEMPLUS_TEST_CG_OVERLAP_DOF_HANDLER_HPP
