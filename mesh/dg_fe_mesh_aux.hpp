#ifndef FEMPLUS_TEST_DG_FE_MESH_AUX_HPP
#define FEMPLUS_TEST_DG_FE_MESH_AUX_HPP

#include "femplus/dg_lagrange_finite_element.hpp"
#include "femplus/dg_finite_element_mesh.hpp"

using namespace femplus;

void show_edge_info(const std::vector<int> edges, const std::string &type,
                    const DGFiniteElementMesh *dg_fe_mesh)
{
  std::vector<int>::const_iterator edge = edges.begin();
  std::vector<int>::const_iterator end = edges.end();

  // pass through boundary or interior edges of the base mesh
  for (; edge != end; ++edge)
  {
    std::cout << type << " edge " << *edge << "\n";

    std::vector<int> dg_edges_indices;
    dg_fe_mesh->associated_edges_numbers(*edge, dg_edges_indices);
    if (type == "boundary")
      EXPECT_EQ(dg_edges_indices.size(), 1);
    else if (type == "interior")
      EXPECT_EQ(dg_edges_indices.size(), 2);
    else throw std::runtime_error("Unknown type of the edges");

    std::vector<int> elem_nums(dg_edges_indices.size());

    std::cout << "  corresp dg edges: ";
    for (int i = 0; i < (int)dg_edges_indices.size(); ++i) {
      std::cout << dg_edges_indices[i] << " ";
      elem_nums[i] = dg_fe_mesh->element_number(dg_edges_indices[i]);
    }
    std::cout << "\n";

    std::vector<const DGLagrangeFiniteElement*> finite_elements(elem_nums.size());

    std::cout << "  corresp cells: ";
    for (int i = 0; i < (int)elem_nums.size(); ++i) {
      std::cout << elem_nums[i] << " ";
      finite_elements[i] = dynamic_cast<const DGLagrangeFiniteElement*>(dg_fe_mesh->element(elem_nums[i]));
      EXPECT_TRUE(finite_elements[i] != NULL);
    }
    std::cout << "\n";

    std::cout << "  corresp finite elements:\n";
    for (int i = 0; i < (int)finite_elements.size(); ++i) {
      std::cout << "    " << elem_nums[i] << ", dofs: ";
      for (int d = 0; d < finite_elements[i]->n_dofs(); ++d)
        std::cout << finite_elements[i]->dof_number(d) << " ";
      std::cout << ", edges: ";
      for (int e = 0; e < finite_elements[i]->n_edges(); ++e)
        std::cout << finite_elements[i]->edge_number(e) << " ";
      std::cout << "\n";
    }
  } // loop over boundary or interior edges
}


#endif // FEMPLUS_TEST_DG_FE_MESH_AUX_HPP
