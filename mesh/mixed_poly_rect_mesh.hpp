#ifndef TEST_FEMPLUS_MIXED_POLY_RECT_MESH_HPP
#define TEST_FEMPLUS_MIXED_POLY_RECT_MESH_HPP

#include "config.hpp"
#include "femplus/function.hpp"
#include "femplus/mesh.hpp"
#include "femplus/mixed_poly_rect_mesh.hpp"
#include "femplus/math_functions.hpp"
#include "analytic_functions.hpp"
#include <gtest/gtest.h>

using namespace femplus;



// =======================================================
TEST(MixedPolyRectMesh, build1)
{
  const double x0 = 0;
  const double x1 = 1;
  const double zero_surface = 0;
  const double depth = 1;
  const int nx = 2;
  const int ny = 2;
  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, zero_surface));
  top_surface.push_back(Point(x1, zero_surface));

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, zero_surface, depth, nx, ny));
  mesh->build();

  EXPECT_EQ(mesh->n_vertices(), 9);
  EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_1.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build1_2)
{
  const double y0 = 0;
  const double depth = 1;
  const int nx = 2;
  const int ny = 2;
  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, y0));
  top_surface.push_back(Point(1, y0));
  top_surface.push_back(Point(0.5, y0)); // coincides with the mesh vertex

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  EXPECT_EQ(mesh->n_vertices(), 9);
  EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_1_2.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build1_3)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int nx = 2;
  const int ny = 2;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    // one point coincides with the mesh vertex, and there are many points which
    // are close to the vertex
    top_surface.push_back(Point((i+1)*1./N, 0));
  }

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_1_3.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build2)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int nx = 3;
  const int ny = 3;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_2.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build3)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_3.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build4)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, 0));
  top_surface.push_back(Point(x1, 0.1));

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_4.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build5)
{
  const double y0 = 0;
  const double depth = 1;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(0, 0));
  top_surface.push_back(Point(1, 0));
  top_surface.push_back(Point(0.25, 0.1));
  top_surface.push_back(Point(0.75, 0.1));
  top_surface.push_back(Point(0.5, 0));

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_5.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build6)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = (i + 1) * 1. / N;
    top_surface.push_back(Point(x, 0.1 * sin(x)));
  }

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

  mesh->write_geo("mixed_poly_rect_6.geo");
}

// =======================================================
TEST(MixedPolyRectMesh, build7)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = (i + 1) * 1. / N;
    top_surface.push_back(Point(x, 0.1 * sin(10*x)));
  }

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_geo("mixed_poly_rect_7.geo", 0.02);
}

// =======================================================
TEST(MixedPolyRectMesh, build8)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int nx = 10;
  const int ny = 10;

  std::vector<Point> top_surface;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = (i + 1) * 1. / N;
    top_surface.push_back(Point(x, 0.02 * sin(10*math::PI*x)));
  }

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_geo("mixed_poly_rect_8.geo", 0.02);
}

// =======================================================
TEST(MixedPolyRectMesh, build9)
{
  std::vector<Point> top_surface;
  const double x0 = -5;
  const double x1 = 5;
  const double y0 = 0;
  const double depth = 10;
  const int nx = 10;
  const int ny = 10;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const int N = 100;
  for (int i = 0; i < N - 1; ++i)
  {
    const double x = x0 + (i + 1) * (x1 - x0) / N;
    top_surface.push_back(Point(x, gauss(0, 0.4, x)));
  }

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, nx, ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  mesh->write_geo("mixed_poly_rect_9.geo", 0.02);
}

// =======================================================
TEST(MixedPolyRectMesh, build9_1)
{
  std::vector<Point> top_surface;
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double depth = 1;
  const int Nx = 10;
  const int Ny = 10;
  top_surface.push_back(Point(x0, y0));
  top_surface.push_back(Point(x1, y0));

  const double ampl = 0.008;
  const double mu = 0.5 * (x1 + x0);
  const double sigma = 0.04;
  const int N = 101;
  for (int i = 1; i < N - 1; ++i)
  {
    const double x = x0 + i * (x1 - x0) / (N - 1);
    top_surface.push_back(Point(x, ampl * gauss(mu, sigma, x)));
  }

  MeshPtr mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
  mesh->build();

  //EXPECT_EQ(mesh->n_vertices(), 9);
  //EXPECT_EQ(mesh->n_elements(), 4);
  //EXPECT_EQ(mesh->n_boundary_elements(), 4);

//  std::cout << "n_elements: " << mesh->n_elements() << std::endl;
//  std::cout << "n_vertices: " << mesh->n_vertices() << std::endl;

  const double cl = 0.02; // if we want to build a mesh
  mesh->write_geo("mixed_poly_rect_9_1.geo", cl);
}




#endif // TEST_FEMPLUS_MIXED_POLY_RECT_MESH_HPP
