#ifndef TEST_FEMPLUS_RECTANGULAR_DG_FE_MESH_HPP
#define TEST_FEMPLUS_RECTANGULAR_DG_FE_MESH_HPP

#include "config.hpp"
#include <gtest/gtest.h>
#include "femplus/mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/dg_lagrange_finite_element.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/dg_dof_handler.hpp"
#include "femplus/finite_element.hpp"
#include "femplus/line.hpp"

#include "compare_functions.hpp"
#include "dg_fe_mesh_aux.hpp"

using namespace femplus;

// =============================================================================
TEST(RectangularDGFEMesh, distribute_dofs_0)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  // should fail because the base mesh is not built (so it is empty)
  EXPECT_ANY_THROW(FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, 1)));

  mesh->build();

  const int order = 1;
  // now should be okay
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();

  // we check that all dofs are distributed as expected by the map between
  // dofs indices and mesh vertices indices
  std::vector<int> dofs_to_vertices;
  dof_handler.dofs_to_vertices(dofs_to_vertices);
  std::vector<int> expected;
  expected.push_back(0);
  expected.push_back(1);
  expected.push_back(2);
  expected.push_back(3);
  EXPECT_EQ((int)expected.size(), dof_handler.n_dofs());

  for (int d = 0; d < dof_handler.n_dofs(); ++d)
    EXPECT_EQ(dofs_to_vertices[d], expected[d]);
}

// =============================================================================
TEST(RectangularDGFEMesh, distribute_dofs_1)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 1));
  mesh->build();

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();

  // we check that all dofs are distributed as expected by the map between
  // dofs indices and mesh vertices indices
  std::vector<int> dofs_to_vertices;
  dof_handler.dofs_to_vertices(dofs_to_vertices);
  std::vector<int> expected(8);
  expected[0] = 0;
  expected[1] = 1;
  expected[2] = 3;
  expected[3] = 4;
  expected[4] = 1;
  expected[5] = 2;
  expected[6] = 4;
  expected[7] = 5;
  EXPECT_EQ((int)expected.size(), dof_handler.n_dofs());

  for (int d = 0; d < dof_handler.n_dofs(); ++d)
    EXPECT_EQ(dofs_to_vertices[d], expected[d]);
}

// =============================================================================
TEST(RectangularDGFEMesh, distribute_dofs_2)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 2));
  mesh->build();

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();

  // we check that all dofs are distributed as expected by the map between
  // dofs indices and mesh vertices indices
  std::vector<int> dofs_to_vertices;
  dof_handler.dofs_to_vertices(dofs_to_vertices);
  int exp_vertices[16] = { 0, 1, 3, 4, 1, 2, 4, 5, 3, 4, 6, 7, 4, 5, 7, 8 };
  std::vector<int> expected(16);
  for (unsigned i = 0; i < expected.size(); ++i)
    expected[i] = exp_vertices[i];

  EXPECT_EQ((int)expected.size(), dof_handler.n_dofs());

  for (int d = 0; d < dof_handler.n_dofs(); ++d)
    EXPECT_EQ(dofs_to_vertices[d], expected[d]);
}

// =============================================================================
TEST(RectangularDGFEMesh, numerate_edges_0)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  mesh->build();

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));
  const DGFiniteElementMesh* dg_fe_mesh = dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(dg_fe_mesh != NULL);

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 3));
  edges.push_back(new Line(2, 3));

  compare_edges(*dg_fe_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);

  // show all the data from the dg_finite_element_mesh
  EXPECT_EQ(boundary_edges.size(), 4); // all edges are boundary
  EXPECT_EQ(interior_edges.size(), 0); // no interior edges

  show_edge_info(boundary_edges, "boundary", dg_fe_mesh);
}

// =============================================================================
TEST(RectangularDGFEMesh, numerate_edges_1)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 1));
  mesh->build();

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 3));
  edges.push_back(new Line(2, 3));
  edges.push_back(new Line(4, 5));
  edges.push_back(new Line(4, 6));
  edges.push_back(new Line(5, 7));
  edges.push_back(new Line(6, 7));

  const DGFiniteElementMesh *const_dg_mesh =
      dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(const_dg_mesh != NULL);

  compare_edges(*const_dg_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);

  // show all the data from the dg_finite_element_mesh
  EXPECT_EQ(boundary_edges.size(), 6); // all edges are boundary
  EXPECT_EQ(interior_edges.size(), 1); // no interior edges

  show_edge_info(boundary_edges, "boundary", const_dg_mesh);
  show_edge_info(interior_edges, "interior", const_dg_mesh);
}

// =============================================================================
TEST(RectangularDGFEMesh, numerate_edges_1_switched)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 1));
  mesh->build();

  // now we switch numeration of the vertices in the first rectangle
  std::vector<int> ver_numbers(4);
  ver_numbers[0] = 0;
  ver_numbers[1] = 3;
  ver_numbers[2] = 1;
  ver_numbers[3] = 4;
  mesh->element(0)->set_vertices_numbers(ver_numbers);

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 3));
  edges.push_back(new Line(2, 3));
  edges.push_back(new Line(4, 5));
  edges.push_back(new Line(4, 6));
  edges.push_back(new Line(5, 7));
  edges.push_back(new Line(6, 7));

  const DGFiniteElementMesh *const_dg_mesh =
      dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(const_dg_mesh != NULL);

  compare_edges(*const_dg_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];

  std::cout << "this test surprisingly works!\n";

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);

  // show all the data from the dg_finite_element_mesh
  EXPECT_EQ(boundary_edges.size(), 6); // all edges are boundary
  EXPECT_EQ(interior_edges.size(), 1); // no interior edges

  show_edge_info(boundary_edges, "boundary", const_dg_mesh);
  show_edge_info(interior_edges, "interior", const_dg_mesh);
}

// =============================================================================
TEST(RectangularDGFEMesh, numerate_edges_2)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 2));
  mesh->build();

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 3));
  edges.push_back(new Line(2, 3));
  edges.push_back(new Line(4, 5));
  edges.push_back(new Line(4, 6));
  edges.push_back(new Line(5, 7));
  edges.push_back(new Line(6, 7));
  edges.push_back(new Line(8, 9));
  edges.push_back(new Line(8, 10));
  edges.push_back(new Line(9, 11));
  edges.push_back(new Line(10, 11));
  edges.push_back(new Line(12, 13));
  edges.push_back(new Line(12, 14));
  edges.push_back(new Line(13, 15));
  edges.push_back(new Line(14, 15));

  const DGFiniteElementMesh *const_dg_mesh =
      dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(const_dg_mesh != NULL);

  compare_edges(*const_dg_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);

  // show all the data from the dg_finite_element_mesh
  EXPECT_EQ(boundary_edges.size(), 8); // all edges are boundary
  EXPECT_EQ(interior_edges.size(), 4); // no interior edges

  show_edge_info(boundary_edges, "boundary", const_dg_mesh);
  show_edge_info(interior_edges, "interior", const_dg_mesh);
}

// =============================================================================
TEST(RectangularDGFEMesh, vector2_numerate_edges_0)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 1, 1));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 3));
  edges.push_back(new Line(0, 4));
  edges.push_back(new Line(1, 5));
  edges.push_back(new Line(2, 6));
  edges.push_back(new Line(4, 6));
  edges.push_back(new Line(3, 7));
  edges.push_back(new Line(5, 7));

  const DGFiniteElementMesh *const_dg_mesh =
      dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(const_dg_mesh != NULL);

  compare_edges(*const_dg_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

// =============================================================================
TEST(RectangularDGFEMesh, vector2_numerate_edges_1)
{
  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 2));
  mesh->build();

  const int order = 1;
  const int n_dofs_per_node = 2;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order, n_dofs_per_node));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 3));
  edges.push_back(new Line(0, 4));
  edges.push_back(new Line(1, 5));
  edges.push_back(new Line(2, 6));
  edges.push_back(new Line(4, 6));
  edges.push_back(new Line(3, 7));
  edges.push_back(new Line(5, 7));

  edges.push_back(new Line(8, 10));
  edges.push_back(new Line(9, 11));
  edges.push_back(new Line(8, 12));
  edges.push_back(new Line(9, 13));
  edges.push_back(new Line(10, 14));
  edges.push_back(new Line(12, 14));
  edges.push_back(new Line(11, 15));
  edges.push_back(new Line(13, 15));

  edges.push_back(new Line(16, 18));
  edges.push_back(new Line(17, 19));
  edges.push_back(new Line(16, 20));
  edges.push_back(new Line(17, 21));
  edges.push_back(new Line(18, 22));
  edges.push_back(new Line(20, 22));
  edges.push_back(new Line(19, 23));
  edges.push_back(new Line(21, 23));

  edges.push_back(new Line(24, 26));
  edges.push_back(new Line(25, 27));
  edges.push_back(new Line(24, 28));
  edges.push_back(new Line(25, 29));
  edges.push_back(new Line(26, 30));
  edges.push_back(new Line(28, 30));
  edges.push_back(new Line(27, 31));
  edges.push_back(new Line(29, 31));

  const DGFiniteElementMesh *const_dg_mesh =
      dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(const_dg_mesh != NULL);

  compare_edges(*const_dg_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];
}

#endif // TEST_FEMPLUS_RECTANGULAR_DG_FE_MESH_HPP
