#ifndef TEST_FEMPLUS_TRIANGULAR_DG_FE_MESH_HPP
#define TEST_FEMPLUS_TRIANGULAR_DG_FE_MESH_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/mesh.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/dg_dof_handler.hpp"
#include "femplus/dg_lagrange_finite_element.hpp"
#include "femplus/finite_element.hpp"
#include "femplus/line.hpp"

#include "compare_functions.hpp"
#include "dg_fe_mesh_aux.hpp"

using namespace femplus;

// =============================================================================
TEST(TriangularDGFEMesh, distribute_dofs_0)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_mesh_0.msh"));

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();

  // we check that all dofs are distributed as expected by the map between
  // dofs indices and mesh vertices indices
  std::vector<int> dofs_to_vertices;
  dof_handler.dofs_to_vertices(dofs_to_vertices);
  std::vector<int> expected;
  expected.push_back(0);
  expected.push_back(1);
  expected.push_back(2);

  EXPECT_EQ((int)expected.size(), dof_handler.n_dofs());

  for (int d = 0; d < dof_handler.n_dofs(); ++d)
    EXPECT_EQ(dofs_to_vertices[d], expected[d]);

  // test of the dofs themselves, not only the map to the vertices numbers
  std::vector<Point> expected_dofs;
  expected_dofs.push_back(Point(0, 0));
  expected_dofs.push_back(Point(1, 0));
  expected_dofs.push_back(Point(0, 1));

  compare_points(dof_handler.dofs(), expected_dofs);
}

// =============================================================================
TEST(TriangularDGFEMesh, distribute_dofs_1)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_mesh_1.msh"));

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();

  // we check that all dofs are distributed as expected by the map between
  // dofs indices and mesh vertices indices
  std::vector<int> dofs_to_vertices;
  dof_handler.dofs_to_vertices(dofs_to_vertices);
  int exp_vertices[12] = { 0, 1, 4, 0, 4, 3, 1, 2, 4, 2, 3, 4 };
  std::vector<int> expected(12);
  for (unsigned i = 0; i < expected.size(); ++i)
    expected[i] = exp_vertices[i];

  EXPECT_EQ((int)expected.size(), dof_handler.n_dofs());

  for (int d = 0; d < dof_handler.n_dofs(); ++d)
    EXPECT_EQ(dofs_to_vertices[d], expected[d]);

  // test of the dofs themselves, not only the map to the vertices numbers
  std::vector<Point> expected_dofs(12);
  expected_dofs[0] = Point(0, 0);
  expected_dofs[1] = Point(1, 0);
  expected_dofs[2] = Point(0.5, 0.5);
  expected_dofs[3] = Point(0, 0);
  expected_dofs[4] = Point(0.5, 0.5);
  expected_dofs[5] = Point(0, 1);
  expected_dofs[6] = Point(1, 0);
  expected_dofs[7] = Point(1, 1);
  expected_dofs[8] = Point(0.5, 0.5);
  expected_dofs[9] = Point(1, 1);
  expected_dofs[10]= Point(0, 1);
  expected_dofs[11]= Point(0.5, 0.5);

  compare_points(dof_handler.dofs(), expected_dofs);
}

// =============================================================================
TEST(TriangularDGFEMesh, numerate_edges_0)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_mesh_0.msh"));

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 2));

  const DGFiniteElementMesh *const_dg_mesh =
      dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(const_dg_mesh != NULL);

  compare_edges(*const_dg_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);

  // show all the data from the dg_finite_element_mesh
  EXPECT_EQ(boundary_edges.size(), 3); // all edges are boundary
  EXPECT_EQ(interior_edges.size(), 0); // no interior edges

  show_edge_info(boundary_edges, "boundary", const_dg_mesh);
}

// =============================================================================
TEST(TriangularDGFEMesh, numerate_edges_1)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_mesh_1.msh"));

  const int order = 1;
  FEMeshPtr dg_mesh(new DGFiniteElementMesh(mesh, order));

  const int gamma = 1;
  DGDoFHandler dof_handler(dg_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();

  std::vector<MeshElement*> edges;
  edges.push_back(new Line(0, 1));
  edges.push_back(new Line(0, 2));
  edges.push_back(new Line(1, 2));
  edges.push_back(new Line(3, 4));
  edges.push_back(new Line(3, 5));
  edges.push_back(new Line(5, 4)); // because corresponding vertices are 3->4
  edges.push_back(new Line(6, 7));
  edges.push_back(new Line(6, 8));
  edges.push_back(new Line(7, 8));
  edges.push_back(new Line(9, 10));
  edges.push_back(new Line(9, 11));
  edges.push_back(new Line(10, 11));

  const DGFiniteElementMesh *const_dg_mesh =
      dynamic_cast<const DGFiniteElementMesh*>(dg_mesh.get());
  EXPECT_TRUE(const_dg_mesh != NULL);

  compare_edges(*const_dg_mesh, edges);

  // clean the memory
  for (unsigned e = 0; e < edges.size(); ++e)
    delete edges[e];

  std::vector<int> boundary_edges;
  mesh->boundary_edges_numbers(boundary_edges);

  std::vector<int> interior_edges;
  mesh->interior_edges_numbers(interior_edges);

  // show all the data from the dg_finite_element_mesh
  EXPECT_EQ(boundary_edges.size(), 4); // all edges are boundary
  EXPECT_EQ(interior_edges.size(), 4); // no interior edges

  show_edge_info(boundary_edges, "boundary", const_dg_mesh);
  show_edge_info(interior_edges, "interior", const_dg_mesh);
}

#endif // TEST_FEMPLUS_TRIANGULAR_DG_FE_MESH_HPP
