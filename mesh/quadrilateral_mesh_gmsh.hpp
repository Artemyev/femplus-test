#ifndef TEST_FEMPLUS_QUADRILATERAL_MESH_GMSH_HPP
#define TEST_FEMPLUS_QUADRILATERAL_MESH_GMSH_HPP

#include "config.hpp"
#include "femplus/point.hpp"
#include <gtest/gtest.h>
#include "femplus/mesh.hpp"
#include <fstream>
#include "femplus/auxiliary_functions.hpp"
#if defined(USE_GMSH_LIB)
  #include "gmsh/Gmsh.h"
  #include "gmsh/GModel.h"
  #include "gmsh/GVertex.h"
  #include "gmsh/GEdge.h"
  #include "gmsh/GFace.h"
  #include "gmsh/MVertex.h"
  #include "gmsh/MElement.h"
#endif

using namespace femplus;



// =============================================================================
#if defined(USE_GMSH_LIB)
TEST(QuadrilateralMeshGmsh, using_temp_geo_file)
{
  std::vector<Point> _geometry_vertices;
  _geometry_vertices.push_back(Point(0, 0));
  _geometry_vertices.push_back(Point(1, 0));
  _geometry_vertices.push_back(Point(1, 1));
  _geometry_vertices.push_back(Point(0, 1));

  const double _characteristic_length = 0.25;

  // we create temporary .geo file, read it, mesh the model, save the data
  // to our structure
  const std::string temp_geo_file = "temporary.geo";
  std::ofstream out(temp_geo_file.c_str());
  require(out, "File " + temp_geo_file + " can't be opened");

  // points
  const int n_geo_vertices = _geometry_vertices.size();
  for (int i = 0; i < n_geo_vertices; ++i)
  {
    out << "Point(" << i + 1 << ") = {"
        << _geometry_vertices[i].coord(0) << ", "
        << _geometry_vertices[i].coord(1) << ", "
        << _geometry_vertices[i].coord(2) << ", "
        << _characteristic_length << "};\n";
  }

  // lines
  for (int i = 0; i < n_geo_vertices; ++i)
  {
    // pretend we have 4 vertices,
    // then the numbers of the vertices defining the lines should be
    // (0, 1), (1, 2), (2, 3), (3, 0) -> to make a line loop closed
    out << "Line(" << i + 1 << ") = {"
        << i + 1 << ", "
        << ((i + 1) % n_geo_vertices) + 1 << "};\n";
  }

  // line loop
  out << "Line Loop(" << n_geo_vertices + 2 << ") = {";
  for (int i = 0; i < n_geo_vertices; ++i)
    out << i + 1 << (i == n_geo_vertices - 1 ? "" : ", ");
  out << "};\n";

  // plane surface
  out << "Plane Surface(1) = {" << n_geo_vertices + 2 << "};\n";

  // recombination
  out << "Recombine Surface {1};\n";

  // physical entities
  out << "Physical Line(" << Mesh::BOUNDARY << ") = {";
  for (int i = 0; i < n_geo_vertices; ++i)
    out << i + 1 << (i == n_geo_vertices - 1 ? "" : ", ");
  out << "};\n";
  out << "Physical Surface(" << Mesh::MAIN_DOMAIN << ") = {1};\n";

  out.close();

  // now we read this file by Gmsh, build the mesh, delete the temporary file
  GmshInitialize(); // Gmsh API is used to build the mesh
  GmshSetOption("Mesh", "Algorithm", 8.);
  GModel *model = new GModel();
  model->setFactory("Gmsh"); // Gmsh or OCC

  model->readGEO(temp_geo_file); // read geo file
  model->mesh(2);                // build the mesh

  model->writeMSH(temp_geo_file + ".msh");

  delete model;
  GmshFinalize();


//  GmshInitialize(); // Gmsh API is used to build the mesh
//  GModel *model = new GModel();
//  model->setFactory("Gmsh"); // Gmsh or OCC

//  // add vertices
//  const int n_geo_vertices = _geometry_vertices.size();
//  std::vector<GVertex*> gvertices(n_geo_vertices);
//  for (int i = 0; i < n_geo_vertices; ++i)
//    gvertices[i] = model->addVertex(_geometry_vertices[i].coord(0),
//                                    _geometry_vertices[i].coord(1),
//                                    _geometry_vertices[i].coord(2),
//                                    _characteristic_length);

//  // add lines
//  std::vector<GEdge*> gedges(n_geo_vertices);
//  for (int i = 0; i < n_geo_vertices; ++i)
//  {
//    // pretend we have 4 vertices,
//    // then the numbers of the vertices defining the lines should be
//    // (0, 1), (1, 2), (2, 3), (3, 0) -> to make a line loop closed
//    gedges[i] = model->addLine(gvertices[i], gvertices[(i+1) % n_geo_vertices]);
//  }

//  // add line loop
//  std::vector<std::vector<GEdge*> > lineloop(1); // we always have 1 lineloop
//  for (int i = 0; i < n_geo_vertices; ++i)
//    lineloop[0].push_back(gedges[i]);

//  // add plane (flat) surface
//  GFace *gface = model->addPlanarFace(lineloop);

//  // add physical entity for the surface:
//  // name, dimesion, number ID
//  model->setPhysicalName("main_domain", 3, MAIN_DOMAIN);
//  // connect the physical entity to the model face
//  gface->addPhysicalEntity(MAIN_DOMAIN);
//  // make Gmsh generate quadrilaterals
////  gface->meshAttributes.recombine = 1;
////  gface->meshAttributes.recombineAngle = 45;
//  //  GmshSetOption("", "", 5);

//  // build a 2D mesh
//  int me = model->mesh(2);
//  std::cout << "model->mesh(2) returned " << me << std::endl;

//  // write to a mesh file
//  model->writeMSH("quad_mesh_gmsh.msh");

//  // show the mesh
////  system("gmsh quad_mesh_gmsh.msh");
}
#endif // USE_GMSH_LIB

// =============================================================================
#if defined(USE_GMSH_LIB)
TEST(QuadrilateralMeshGmsh, using_cpp_api)
{
  std::vector<Point> _geometry_vertices;
  _geometry_vertices.push_back(Point(0, 0));
  _geometry_vertices.push_back(Point(1, 0));
  _geometry_vertices.push_back(Point(1, 1));
  _geometry_vertices.push_back(Point(0, 1));

  const double _characteristic_length = 0.25;

  GmshInitialize(); // Gmsh API is used to build the mesh
  GModel *model = new GModel();
  model->setFactory("Gmsh"); // Gmsh or OCC

  // add vertices
  const int n_geo_vertices = _geometry_vertices.size();
  std::vector<GVertex*> gvertices(n_geo_vertices);
  for (int i = 0; i < n_geo_vertices; ++i)
    gvertices[i] = model->addVertex(_geometry_vertices[i].coord(0),
                                    _geometry_vertices[i].coord(1),
                                    _geometry_vertices[i].coord(2),
                                    _characteristic_length);

  // add lines
  std::vector<GEdge*> gedges(n_geo_vertices);
  for (int i = 0; i < n_geo_vertices; ++i)
  {
    // pretend we have 4 vertices,
    // then the numbers of the vertices defining the lines should be
    // (0, 1), (1, 2), (2, 3), (3, 0) -> to make a line loop closed
    gedges[i] = model->addLine(gvertices[i], gvertices[(i+1) % n_geo_vertices]);
  }

  // add line loop
  std::vector<std::vector<GEdge*> > lineloop(1); // we always have 1 lineloop
  for (int i = 0; i < n_geo_vertices; ++i)
    lineloop[0].push_back(gedges[i]);

  // add plane (flat) surface
  GFace *gface = model->addPlanarFace(lineloop);

  // add physical entity for the surface:
  // name, dimesion, number ID
  model->setPhysicalName("main_domain", 3, Mesh::MAIN_DOMAIN);
  // connect the physical entity to the model face
  gface->addPhysicalEntity(Mesh::MAIN_DOMAIN);
  // make Gmsh generate quadrilaterals
  gface->meshAttributes.recombine = 1;
//  gface->meshAttributes.recombineAngle = 45;
  //  GmshSetOption("", "", 5);

  // build a 2D mesh
  int me = model->mesh(2);
  std::cout << "model->mesh(2) returned " << me << std::endl;

  // write to a mesh file
  model->writeMSH("quad_mesh_gmsh_api.msh");

  // show the mesh
//  system("gmsh quad_mesh_gmsh_api.msh");

  delete model;
  GmshFinalize();
}
#endif // USE_GMSH_LIB


#endif // TEST_FEMPLUS_QUADRILATERAL_MESH_GMSH_HPP
