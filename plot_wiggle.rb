#!/usr/bin/ruby

# hbox - heigth of the plot
# wbox - width of the plot
# fill - fill the waveform or not, if not, =0
# x1end - the value of ending the plot in 1st dim
# linewidth - widht of the wiggle
# label1 - the name of the label in 1st dim
# label2 - ... 2nd dim
# labelsize - size of the label font
# d1num - interval of ticks in 1st dim
# d2num - ... 2nd dim
# d1 - interval of the sample in 1st dim
# d2 - ... 2nd dim
# f1 
# f2 - what is the value of the 1st sample in the 2nd dim

# | -continous output
# suaddhead - add header info to the raw data, and set the tracl and ns fields
# sushw - set one or more header words using trace number, mod and integer divide to compute the header word values
# -- options - key: header key word to set, a: value on first trace
# suwind - select from data from some window -key=tracl means select according to trace number
# -- option - j=6 means select every 6 traces
# supswigp - plot the su file with options
# psmerge - merge two or more plots

support = 10        # source support
Nx = 10             # number of coarse elements in x-direction
nx = 10             # number of fine elements in x-direction
n_boundary_bf = 15  # number of boundary basis functions
n_interior_bf = 15  # number of interior basis functions
file_in = "gms_R1_ss_#{support}_N_#{Nx}_n_#{nx}_nb_#{n_boundary_bf}_ni_#{n_interior_bf}"
file_out = "#{file_in}.ps"

nt = 999            # N time steps
dt = 1.5e-4         # time step
angle = 10          # angle increasing
n_receivers = 360/angle # number of tracks
track = 1           # show every *-th track
d1num = nt * dt / 5 # 5 is how many ticks on a y-axis
a0 = 0              # begin angle for circular receivers
a1 = 360            # end angle for circular receivers
d2num = (a1-a0) / 6 # 6 is how many ticks on a x-axis

options = "hbox=6 wbox=6 label1='Time (s)' label2='Degrees' labelsize=24"

wigopts = "#{options} fill=0 linewidth=1.0 perc=99 d1num=#{d1num} d2num=#{d2num} d1=#{dt} d2=#{track*angle} f1=0 f2=0"

system("a2b <#{file_in} n1=#{nt} >press.bin")
system("transp <press.bin n1=#{n_receivers} >press2.bin")

system("suaddhead < press2.bin n1=#{nt} | sushw key=dt a=100 | suwind key=tracl j=#{track} | supswigp >#{file_out} #{wigopts} tracecolor=blue")
system("evince #{file_out}")
