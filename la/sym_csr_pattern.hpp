#ifndef FEMPLUS_TEST_SYM_CSR_PATTERN_HPP
#define FEMPLUS_TEST_SYM_CSR_PATTERN_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/dense_pattern.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/sym_csr_pattern.hpp"

using namespace femplus;

// =============================================================================
TEST(SymCSRPattern, add_diag_0)
{
  const int r[] = { 0, 3, 5, 7, 10 };
  const int c[] = { 0, 1, 3, 0, 1, 2, 3, 0, 2, 3 };

  const std::vector<int> row(r, r+5);
  const std::vector<int> col(c, c+10);

  CSRPattern csr(row, col);

  // convert to symmetric CSR pattern
  std::vector<int> sym_row, sym_col;
  csr.to_sym_csr(sym_row, sym_col);

  SymCSRPattern symcsr(sym_row, sym_col);
  symcsr.add_diag();

  // symmetric CSR pattern based on the CSR pattern WITH diagonal
  const int sr[] = { 0, 1, 3, 4, 7 };
  const int sc[] = { 0, 0, 1, 2, 0, 2, 3 };

  // check the results
  std::vector<int> s_rows, s_cols;
  symcsr.get_rows(s_rows);
  symcsr.get_cols(s_cols);

  EXPECT_EQ(s_rows.size(), 5);
  for (int i = 0; i < 5; ++i)
    EXPECT_EQ(s_rows[i], sr[i]);

  EXPECT_EQ(s_cols.size(), 7);
  for (int i = 0; i < 7; ++i)
    EXPECT_EQ(s_cols[i], sc[i]);
}

// =============================================================================
TEST(SymCSRPattern, add_diag_1)
{
  const int r[] = { 0, 2, 5, 8, 11, 13, 16 };
  const int c[] = { 0, 2, 1, 3, 5, 0, 2, 4, 1, 3, 5, 2, 4, 1, 3, 5 };

  const std::vector<int> row(r, r+7);
  const std::vector<int> col(c, c+16);

  CSRPattern csr(row, col);

  // convert to symmetric CSR pattern
  std::vector<int> sym_row, sym_col;
  csr.to_sym_csr(sym_row, sym_col);

  SymCSRPattern symcsr(sym_row, sym_col);
  symcsr.add_diag();

  // symmetric CSR pattern based on the CSR pattern WITH diagonal
  const int sr[] = { 0, 1, 2, 4, 6, 8, 11 };
  const int sc[] = { 0, 1, 0, 2, 1, 3, 2, 4, 1, 3, 5 };

  // check the results
  std::vector<int> s_rows, s_cols;
  symcsr.get_rows(s_rows);
  symcsr.get_cols(s_cols);

  EXPECT_EQ(s_rows.size(), 7);
  for (int i = 0; i < 7; ++i)
    EXPECT_EQ(s_rows[i], sr[i]);

  EXPECT_EQ(s_cols.size(), 11);
  for (int i = 0; i < 11; ++i)
    EXPECT_EQ(s_cols[i], sc[i]);
}

// =============================================================================
TEST(SymCSRPattern, add_diag_2)
{
  DensePattern dense(5, 5);

  const int sr[] = { 0, 1, 3, 6, 10, 15 };
  const int sc[] = { 0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4 };

  std::vector<int> s_row, s_col;
  dense.to_sym_csr(s_row, s_col);

  SymCSRPattern symcsr(s_row, s_col);
  symcsr.add_diag();

  // check the results
  std::vector<int> s_rows, s_cols;
  symcsr.get_rows(s_rows);
  symcsr.get_cols(s_cols);

  EXPECT_EQ(s_rows.size(), 6);
  for (int i = 0; i < 6; ++i)
    EXPECT_EQ(s_rows[i], sr[i]);

  EXPECT_EQ(s_cols.size(), 15);
  for (int i = 0; i < 15; ++i)
    EXPECT_EQ(s_cols[i], sc[i]);
}

// =============================================================================
TEST(SymCSRPattern, get_rows_cols)
{
  DensePattern dense(5, 5);

  const int sr[] = { 0, 1, 3, 6, 10, 15 };
  const int sc[] = { 0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4 };

  std::vector<int> s_row, s_col;
  dense.to_sym_csr(s_row, s_col);

  SymCSRPattern symcsr(s_row, s_col);
  symcsr.add_diag();

  // check the results
  std::vector<int> s_rows, s_cols;
  symcsr.get_rows(s_rows, 1); // starting from 1 (for Fortran)
  symcsr.get_cols(s_cols, 1); // starting from 1 (for Fortran)

  EXPECT_EQ(s_rows.size(), 6);
  for (int i = 0; i < 6; ++i)
    EXPECT_EQ(s_rows[i], sr[i] + 1);

  EXPECT_EQ(s_cols.size(), 15);
  for (int i = 0; i < 15; ++i)
    EXPECT_EQ(s_cols[i], sc[i] + 1);
}

#endif // FEMPLUS_TEST_SYM_CSR_PATTERN_HPP
