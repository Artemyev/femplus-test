#ifndef TEST_FEMPLUS_CSR_PATTERN_HPP
#define TEST_FEMPLUS_CSR_PATTERN_HPP

#include "config.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/pattern.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/sym_csr_pattern.hpp"
#include "femplus/triangular_mesh.hpp"
#include "compare_functions.hpp"
#include <gtest/gtest.h>


using namespace femplus;

// =============================================================================
TEST(CSRPattern, rect_mesh)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int nx = 5;
  const int ny = 5;

  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);

  int row[] = { 0, 4, 10, 16, 22, 28, 32, 38, 47, 56, 65, 74, 80, 86, 95, 104,
                113, 122, 128, 134, 143, 152, 161, 170, 176, 182, 191, 200, 209,
                218, 224, 228, 234, 240, 246, 252, 256 };
  int col[] = { 0, 1, 6, 7, 0, 1, 2, 6, 7, 8, 1, 2, 3, 7, 8, 9, 2, 3, 4, 8, 9,
                10, 3, 4, 5, 9, 10, 11, 4, 5, 10, 11, 0, 1, 6, 7, 12, 13, 0, 1,
                2, 6, 7, 8, 12, 13, 14, 1, 2, 3, 7, 8, 9, 13, 14, 15, 2, 3, 4,
                8, 9, 10, 14, 15, 16, 3, 4, 5, 9, 10, 11, 15, 16, 17, 4, 5, 10,
                11, 16, 17, 6, 7, 12, 13, 18, 19, 6, 7, 8, 12, 13, 14, 18, 19,
                20, 7, 8, 9, 13, 14, 15, 19, 20, 21, 8, 9, 10, 14, 15, 16, 20,
                21, 22, 9, 10, 11, 15, 16, 17, 21, 22, 23, 10, 11, 16, 17, 22,
                23, 12, 13, 18, 19, 24, 25, 12, 13, 14, 18, 19, 20, 24, 25, 26,
                13, 14, 15, 19, 20, 21, 25, 26, 27, 14, 15, 16, 20, 21, 22, 26,
                27, 28, 15, 16, 17, 21, 22, 23, 27, 28, 29, 16, 17, 22, 23, 28,
                29, 18, 19, 24, 25, 30, 31, 18, 19, 20, 24, 25, 26, 30, 31, 32,
                19, 20, 21, 25, 26, 27, 31, 32, 33, 20, 21, 22, 26, 27, 28, 32,
                33, 34, 21, 22, 23, 27, 28, 29, 33, 34, 35, 22, 23, 28, 29, 34,
                35, 24, 25, 30, 31, 24, 25, 26, 30, 31, 32, 25, 26, 27, 31, 32,
                33, 26, 27, 28, 32, 33, 34, 27, 28, 29, 33, 34, 35, 28, 29, 34,
                35 };


  const int n_points = (nx+1) * (ny+1);
  compare_csr_pattern(pattern, n_points, &row[0], &col[0]);
}

// =============================================================================
TEST(CSRPattern, triangular_mesh)
{
  MeshPtr mesh(new TriangularMesh(TESTFILES_DIR + "/test_small_10_4.msh"));

  const int order = 1;
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler.dofs_connections(connections);

  CSRPattern pattern(connections);

  Point points[] = { Point(0, 0, 0),
                     Point(2, 0, 0),
                     Point(4, 0, 0),
                     Point(1, 1, 0),
                     Point(0, 2, 0),
                     Point(2, 2, 0),
                     Point(4, 2, 0),
                     Point(0, 4, 0),
                     Point(2, 4, 0),
                     Point(4, 4, 0)
                   };
  const int n_points = sizeof(points) / sizeof(Point);
  std::vector<Point> vec_points(&points[0],
                                &points[0] + n_points);

  compare_points(*mesh.get(), vec_points);

  int row[] = { 0, 4, 10, 13, 18, 23, 30, 36, 40, 45, 48 };
  int col[] = { 0, 1, 3, 4,
                0, 1, 2, 3, 5, 6,
                1, 2, 6,
                0, 1, 3, 4, 5,
                0, 3, 4, 5, 7,
                1, 3, 4, 5, 6, 7, 8,
                1, 2, 5, 6, 8, 9,
                4, 5, 7, 8,
                5, 6, 7, 8, 9,
                6, 8, 9
              };

  compare_csr_pattern(pattern, n_points, &row[0], &col[0]);
}

// =============================================================================
TEST(CSRPattern, to_sym_csr_0)
{
  const int r[] = { 0, 3, 5, 7, 10 };
  const int c[] = { 0, 1, 3, 0, 1, 2, 3, 0, 2, 3 };

  const std::vector<int> row(r, r+5);
  const std::vector<int> col(c, c+10);

  CSRPattern csr(row, col);
  EXPECT_EQ(csr.size(), 4);

  // symmetric CSR pattern based on the CSR pattern
  const int sr[] = { 0, 0, 1, 1, 3 };
  const int sc[] = { 0, 0, 2 };

  // convert to symmetric CSR pattern
  std::vector<int> sym_row, sym_col;
  csr.to_sym_csr(sym_row, sym_col);

  // check the conversion
  EXPECT_EQ(sym_row.size(), 5);
  for (int i = 0; i < 5; ++i)
    EXPECT_EQ(sym_row[i], sr[i]);

  EXPECT_EQ(sym_col.size(), 3);
  for (int i = 0; i < 3; ++i)
    EXPECT_EQ(sym_col[i], sc[i]);

  SymCSRPattern symcsr(sym_row, sym_col);

  // check the results
  std::vector<int> s_rows, s_cols;
  symcsr.get_rows(s_rows);
  symcsr.get_cols(s_cols);

  EXPECT_EQ(s_rows.size(), 5);
  for (int i = 0; i < 5; ++i)
    EXPECT_EQ(s_rows[i], sr[i]);

  EXPECT_EQ(s_cols.size(), 3);
  for (int i = 0; i < 3; ++i)
    EXPECT_EQ(s_cols[i], sc[i]);
}

// =============================================================================
TEST(CSRPattern, to_sym_csr_1)
{
  const int r[] = { 0, 2, 5, 8, 11, 13, 16 };
  const int c[] = { 0, 2, 1, 3, 5, 0, 2, 4, 1, 3, 5, 2, 4, 1, 3, 5 };

  const std::vector<int> row(r, r+7);
  const std::vector<int> col(c, c+16);

  CSRPattern csr(row, col);

  // symmetric CSR pattern based on the CSR pattern
  const int sr[] = { 0, 0, 0, 1, 2, 3, 5 };
  const int sc[] = { 0, 1, 2, 1, 3 };

  // convert to symmetric CSR pattern
  std::vector<int> sym_row, sym_col;
  csr.to_sym_csr(sym_row, sym_col);

  // check the conversion
  EXPECT_EQ(sym_row.size(), 7);
  for (int i = 0; i < 7; ++i)
    EXPECT_EQ(sym_row[i], sr[i]);

  EXPECT_EQ(sym_col.size(), 5);
  for (int i = 0; i < 5; ++i)
    EXPECT_EQ(sym_col[i], sc[i]);

  SymCSRPattern symcsr(sym_row, sym_col);

  // check the results
  std::vector<int> s_rows, s_cols;
  symcsr.get_rows(s_rows);
  symcsr.get_cols(s_cols);

  EXPECT_EQ(s_rows.size(), 7);
  for (int i = 0; i < 7; ++i)
    EXPECT_EQ(s_rows[i], sr[i]);

  EXPECT_EQ(s_cols.size(), 5);
  for (int i = 0; i < 5; ++i)
    EXPECT_EQ(s_cols[i], sc[i]);
}

#endif // TEST_FEMPLUS_CSR_PATTERN_HPP
