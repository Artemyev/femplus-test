#ifndef TEST_FEMPLUS_POLYGON_HPP
#define TEST_FEMPLUS_POLYGON_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/function.hpp"
#include "femplus/mesh.hpp"
#include "femplus/polygon.hpp"
#include "femplus/auxiliary_functions.hpp"

#include <fstream>


using namespace femplus;


// =======================================================
TEST(Polygon, build1)
{
  std::vector<int> v;
  v.push_back(0);
  v.push_back(1);
  v.push_back(2);

  EXPECT_NO_THROW(Polygon poly(v));
}

// =======================================================
TEST(Polygon, counterclockwise1)
{
  std::vector<int> v;
  v.push_back(0);
  v.push_back(1);
  v.push_back(2);

  Polygon poly(v);

  std::vector<Point> p;
  p.push_back(Point(0, 0));
  p.push_back(Point(1, 0));
  p.push_back(Point(0, 1));

  std::vector<int> v_sorted;
  poly.vertices_counterclockwise(p, v_sorted);

  const int v_true[] = { 2, 0, 1 };
  for (int i = 0; i < poly.n_vertices(); ++i)
    EXPECT_EQ(v_true[i], v_sorted[i]);
}

// =======================================================
TEST(Polygon, gmsh_geo)
{
  std::vector<int> v;
  v.push_back(1);
  v.push_back(2);
  v.push_back(3);
  v.push_back(4);
  v.push_back(8);
  v.push_back(9);
  v.push_back(10);
  v.push_back(11);
  v.push_back(12);

  Polygon poly(v);

  std::vector<Point> p;
  p.push_back(Point(0.48,-0.0996165));
  p.push_back(Point(0.49,-0.0982453));
  p.push_back(Point(0.5,-0.0958924));
  p.push_back(Point(0.47,-0.0999923));
  p.push_back(Point(0.46,-0.0993691));
  p.push_back(Point(0.45,-0.097753));
  p.push_back(Point(0.44,-0.0951602));
  p.push_back(Point(0.43,-0.0916166));
  p.push_back(Point(0.4,-0.0756802));
  p.push_back(Point(0.42,-0.0871576));
  p.push_back(Point(0.41,-0.0818277));
  p.push_back(Point(0.4,-0.15999));
  p.push_back(Point(0.5,-0.15999));

  // this should give bad results for this case of vertices.
  // that's why it has been decided not to use the counterclockwise
  // sorting for the vertices of polygons, but just
  // define the correct order a priori.
  std::vector<int> v_sorted;
  poly.vertices_counterclockwise(p, v_sorted);

  // so here we don't check anything, and just output the geometry, so that it
  // could be checked visually that it's wrong, when the order of the vertices
  // describing the polygon is wrong
  const std::string fname = TESTOUT_DIR + "poly_gmsh.geo";
  std::ofstream out(fname);
  require(out, "File '" + fname + "' cannot be opened");
  out << poly.gmsh_geo(p) << "\n";
  out.close();
}




#endif // TEST_FEMPLUS_POLYGON_HPP
