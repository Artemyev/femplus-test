#ifndef TEST_FEMPLUS_ELLIPTIC2D_HPP
#define TEST_FEMPLUS_ELLIPTIC2D_HPP

#include "config.hpp"
#include "elliptic2D_aux.hpp"
#include "analytic_functions.hpp"
#include <gtest/gtest.h>

using namespace femplus;

const bool show_results = false;
ConstantFunction coef_stif(1.);

#if 1
// =============================================================================
//
// test for rectangular and triangular meshes and constant solution
//
// =============================================================================
TEST(Elliptic2D, solution_rectangles_const_func)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 0.0000000000000000e+00,
                                0.0000000000000000e+00,
                                2.3498992183808827e-16,
                                1.0221474825847510e-13,
                                1.1975211260728726e-13,
                                7.8939349613667732e-13,
                                1.2704143637243938e-11,
                                1.7609137874771858e-11 };
  const double tol = 1e-14;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                       pow(2, i)*n_beg,// Ny
                                       an_solution_0(),  // analytic solution
                                       rhs_function_0(), // rhs function
                                       coef_stif,      // coefficient
                                       cur_error,      // get error
                                       cur_n_cells,    // get n of mesh cells
                                       prev_error,     // set error from
                                                       // previous solution
                                       prev_n_cells,   // set n of mesh cells
                                                       // from previous solution
                                       show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// -----------------------------------------------------------------------------
TEST(Elliptic2D, solution_triangles_const_func)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 2.2204460492503131e-16,
                                1.1521328816827454e-16,
                                6.8221032883399272e-16,
                                1.3424956440415675e-13,
                                3.1331227747661961e-12,
                                1.9850280642322081e-12,
                                5.1093426546292664e-11,
                                1.2477445553446954e-10 };
  const double tol = 1e-14;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                      pow(2, i)*n_beg,// Ny
                                      an_solution_0(),  // analytic solution
                                      rhs_function_0(), // rhs function
                                      coef_stif,      // coefficient
                                      cur_error,      // get error
                                      cur_n_cells,    // get n of mesh cells
                                      prev_error,     // set error from
                                                      // previous solution
                                      prev_n_cells,   // set n of mesh cells
                                                      // from previous solution
                                      show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// =============================================================================
//
// test for rectangular and triangular meshes and linear solution (x+y)
//
// =============================================================================
TEST(Elliptic2D, solution_rectangles_x_plus_y)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 1.1102230246251565e-16,
                                3.1969154255649200e-16,
                                2.0829630286482677e-16,
                                1.5716377316343681e-13,
                                1.5244498768195917e-13,
                                3.6620217447051044e-13,
                                2.9565252346613478e-12,
                                3.1772427780352570e-11 };
  const double tol = 1e-14;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                       pow(2, i)*n_beg,// Ny
                                       an_solution_1(),  // analytic solution
                                       rhs_function_1(), // rhs function
                                       coef_stif,      // coefficient
                                       cur_error,      // get error
                                       cur_n_cells,    // get n of mesh cells
                                       prev_error,     // set error from
                                                       // previous solution
                                       prev_n_cells,   // set n of mesh cells
                                                       // from previous solution
                                       show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// -----------------------------------------------------------------------------
TEST(Elliptic2D, solution_triangles_x_plus_y)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 2.2204460492503131e-16,
                                3.2911847257998117e-16,
                                2.0603555897889470e-16,
                                3.6824695784111107e-13,
                                6.6679075965988097e-12,
                                5.0059988823043679e-12,
                                1.9010500364357387e-11,
                                1.2912626254294931e-10 };
  const double tol = 1e-14;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                      pow(2, i)*n_beg,// Ny
                                      an_solution_1(),  // analytic solution
                                      rhs_function_1(), // rhs function
                                      coef_stif,      // coefficient
                                      cur_error,      // get error
                                      cur_n_cells,    // get n of mesh cells
                                      prev_error,     // set error from
                                                      // previous solution
                                      prev_n_cells,   // set n of mesh cells
                                                      // from previous solution
                                      show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// =============================================================================
//
// test for rectangular and triangular meshes and solution (x*y)
//
// =============================================================================
TEST(Elliptic2D, solution_rectangles_xy)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 0.0000000000000000e+00,
                                1.1102230246251565e-16,
                                1.6958936828758670e-16,
                                2.2567127266159511e-13,
                                1.9986571397569802e-13,
                                5.1698826077032526e-13,
                                4.0698481040297063e-12,
                                4.0065550361695987e-11 };
  const double tol = 1e-14;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                       pow(2, i)*n_beg,// Ny
                                       an_solution_2(),  // analytic solution
                                       rhs_function_2(), // rhs function
                                       coef_stif,      // coefficient
                                       cur_error,      // get error
                                       cur_n_cells,    // get n of mesh cells
                                       prev_error,     // set error from
                                                       // previous solution
                                       prev_n_cells,   // set n of mesh cells
                                                       // from previous solution
                                       show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// -----------------------------------------------------------------------------
TEST(Elliptic2D, solution_triangles_xy)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 0.0000000000000000e+00,
                                2.0374936843394800e-16,
                                3.0946501371164185e-03,
                                9.6498963856902326e-04,
                                2.3108540965059588e-04,
                                5.4271370034894621e-05,
                                1.3707650611537907e-05,
                                3.4916628860269418e-06 };
  const double tol = 5e-10;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                      pow(2, i)*n_beg,// Ny
                                      an_solution_2(),  // analytic solution
                                      rhs_function_2(), // rhs function
                                      coef_stif,      // coefficient
                                      cur_error,      // get error
                                      cur_n_cells,    // get n of mesh cells
                                      prev_error,     // set error from
                                                      // previous solution
                                      prev_n_cells,   // set n of mesh cells
                                                      // from previous solution
                                      show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// =============================================================================
//
// test for rectangular and triangular meshes and solution (x*x + y*y)
//
// =============================================================================
TEST(Elliptic2D, solution_rectangles_xx_yy)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 1.1102230246251565e-16,
                                3.4053084382900563e-16,
                                1.7242392730437759e-16,
                                1.8430963597345248e-13,
                                1.7859265058856282e-13,
                                4.7004432443799031e-13,
                                2.9414811436286191e-12,
                                2.0090247318020888e-11 };
  const double tol = 3e-10;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                       pow(2, i)*n_beg,// Ny
                                       an_solution_3(),  // analytic solution
                                       rhs_function_3(), // rhs function
                                       coef_stif,      // coefficient
                                       cur_error,      // get error
                                       cur_n_cells,    // get n of mesh cells
                                       prev_error,     // set error from
                                                       // previous solution
                                       prev_n_cells,   // set n of mesh cells
                                                       // from previous solution
                                       show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// -----------------------------------------------------------------------------
TEST(Elliptic2D, solution_triangles_xx_yy)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 6.6666666666666693e-02,
                                2.1709551768081384e-02,
                                6.7961759318988244e-03,
                                1.1460845555412329e-03,
                                3.2934513601447392e-04,
                                8.1832326515826867e-05,
                                2.2487695808012526e-05,
                                5.6105793959534093e-06 };
  const double tol = 5e-8;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                      pow(2, i)*n_beg,// Ny
                                      an_solution_3(),  // analytic solution
                                      rhs_function_3(), // rhs function
                                      coef_stif,      // coefficient
                                      cur_error,      // get error
                                      cur_n_cells,    // get n of mesh cells
                                      prev_error,     // set error from
                                                      // previous solution
                                      prev_n_cells,   // set n of mesh cells
                                                      // from previous solution
                                      show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// =============================================================================
//
// test for rectangular and triangular meshes and solution (sin(x) + sin(y))
//
// =============================================================================
TEST(Elliptic2D, solution_rectangles_sinx_siny)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 0.0000000000000000e+00,
                                5.9974817899172345e-04,
                                1.7024140614701723e-04,
                                4.6107992502384631e-05,
                                1.2132380696489930e-05,
                                3.1224061709374267e-06,
                                7.9273365195953908e-07,
                                1.9979625630343560e-07 };
  const double tol = 5e-10;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                       pow(2, i)*n_beg,// Ny
                                       an_solution_4(),  // analytic solution
                                       rhs_function_4(), // rhs function
                                       coef_stif,      // coefficient
                                       cur_error,      // get error
                                       cur_n_cells,    // get n of mesh cells
                                       prev_error,     // set error from
                                                       // previous solution
                                       prev_n_cells,   // set n of mesh cells
                                                       // from previous solution
                                       show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// -----------------------------------------------------------------------------
TEST(Elliptic2D, solution_triangles_sinx_siny)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 1.8636742517020584e-02,
                                5.6361776757975655e-03,
                                1.4740559745904877e-03,
                                2.5000835788981335e-04,
                                7.0165033936938495e-05,
                                1.7229696699070983e-05,
                                4.7390223929514050e-06,
                                1.1865479969678699e-06 };
  const double tol = 5e-8;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                      pow(2, i)*n_beg,// Ny
                                      an_solution_4(),  // analytic solution
                                      rhs_function_4(), // rhs function
                                      coef_stif,      // coefficient
                                      cur_error,      // get error
                                      cur_n_cells,    // get n of mesh cells
                                      prev_error,     // set error from
                                                      // previous solution
                                      prev_n_cells,   // set n of mesh cells
                                                      // from previous solution
                                      show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// =============================================================================
//
// test for rectangular and triangular meshes and solution (exp(x))
//
// =============================================================================
TEST(Elliptic2D, solution_rectangles_expx)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 1.7142283702456092e-16,
                                5.7661108342209149e-04,
                                1.6499079921229680e-04,
                                4.5087135362301002e-05,
                                1.1930462609196360e-05,
                                3.0800013779486078e-06,
                                7.8324601233186339e-07,
                                1.9757539205213148e-07 };
  const double tol = 3e-10;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                       pow(2, i)*n_beg,// Ny
                                       an_solution_5(),  // analytic solution
                                       rhs_function_5(), // rhs function
                                       coef_stif,      // coefficient
                                       cur_error,      // get error
                                       cur_n_cells,    // get n of mesh cells
                                       prev_error,     // set error from
                                                       // previous solution
                                       prev_n_cells,   // set n of mesh cells
                                                       // from previous solution
                                       show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
// -----------------------------------------------------------------------------
TEST(Elliptic2D, solution_triangles_expx)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  // expected relative errors
  const double rel_errors[] = { 1.4553080041638820e-02,
                                4.9819858028350922e-03,
                                1.5442470093605170e-03,
                                2.2411289531962288e-04,
                                7.1969279922727265e-05,
                                1.8232414790351637e-05,
                                4.8909166616482244e-06,
                                1.2328116153729596e-06 };
  const double tol = 2e-8;

  for (int i = 0; i < N_times; ++i)
  {
    check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                      pow(2, i)*n_beg,// Ny
                                      an_solution_5(),  // analytic solution
                                      rhs_function_5(), // rhs function
                                      coef_stif,      // coefficient
                                      cur_error,      // get error
                                      cur_n_cells,    // get n of mesh cells
                                      prev_error,     // set error from
                                                      // previous solution
                                      prev_n_cells,   // set n of mesh cells
                                                      // from previous solution
                                      show_results);

    EXPECT_NEAR(cur_error, rel_errors[i], tol);

    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
#endif

// =============================================================================
//
// Test for rectangular and triangular meshes and periodically oscillating
// solution
//
// =============================================================================
#if 1
TEST(Elliptic2D, solution_rect_and_tria_period_oscill_solution)
{
  class AnSolution: public Function {
  public:
    AnSolution(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return sin(k*pi*x/(x1-x0)) * sin(m*pi*y/(y1-y0));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  class RhsFunction: public Function {
  public:
    RhsFunction(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return (k*k*pi*pi*sin((k*pi*x)/(x1-x0))*sin((m*pi*y)/(y1-y0)))/((x1-x0)*(x1-x0)) +
             (m*m*pi*pi*sin((k*pi*x)/(x1-x0))*sin((m*pi*y)/(y1-y0)))/((y1-y0)*(y1-y0));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int k = 10;
  const int m = 10;
  AnSolution an_solution(x0, x1, y0, y1, k, m);
  RhsFunction rhs_function(x0, x1, y0, y1, k, m);

  { // -------------------- rectangles ---------------------------
    // expected relative errors
    const double rel_errors[] = { 0.0000000000000000e+00,
                                  3.9123351671205655e+00,
                                  1.9561675835602838e+01,
                                  9.4657208531601944e-01,
                                  2.4841032370348012e-01,
                                  7.6295158662571902e-02,
                                  1.9835510631471331e-02,
                                  5.0047842889953363e-03 };
    const double tol = 1e-14;

    const std::string fname = ""; //"rect_per_oscil";

    for (int i = 0; i < N_times; ++i)
    {
      check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution,  // analytic solution
                                         rhs_function, // rhs function
                                         coef_stif,      // coefficient = 1
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells,   // set n of mesh cells
                                                         // from previous solution
                                         show_results,
                                         fname,
                                         x0, x1, y0, y1);

      EXPECT_NEAR(cur_error, rel_errors[i], tol);

      prev_error = cur_error;
      prev_n_cells = cur_n_cells;
    }
  }

  { // -------------------- triangles ---------------------------
    // expected relative errors
    const double rel_errors[] = { 3.9895511204659165e+01,
                                  1.9561675835602827e+01,
                                  7.6108485169267723e+00,
                                  4.7152275681179159e+00,
                                  5.1466327744495421e-01,
                                  2.0119386822228505e-01,
                                  3.4956244927217099e-02,
                                  9.0892761511601004e-03 };
    const double tol = 1e-14;

    const std::string fname = ""; //"rect_per_oscil_tria";

    for (int i = 0; i < N_times; ++i)
    {
      check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution,  // analytic solution
                                         rhs_function, // rhs function
                                         coef_stif,      // coefficient = 1
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells,   // set n of mesh cells
                                                         // from previous solution
                                         show_results,
                                         fname,
                                         x0, x1, y0, y1);

      EXPECT_NEAR(cur_error, rel_errors[i], tol);

      prev_error = cur_error;
      prev_n_cells = cur_n_cells;
    }
  }
}
#endif

// =============================================================================
//
// Test for rectangular and triangular meshes and aperiodically oscillating
// solution
//
// =============================================================================
#if 1
TEST(Elliptic2D, solution_rect_and_tria_aperiod_oscill_solution)
{
  class AnSolution: public Function {
  public:
    AnSolution(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return sin(pi*x/(x1-x0)) * sin(pi*y/(y1-y0)) *
             sin(k*pi*x/(x1-x0)) * sin(m*pi*y/(y1-y0));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  class RhsFunction: public Function {
  public:
    RhsFunction(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return (pi*pi*(m*m*pow(x0-x1,2)+k*k*pow(y0-y1,2))*sin((k*pi*x)/(-x0+x1))*
              sin((m*pi*y)/(-y0+y1))*sin(pi*x/(-x0+x1))*sin(pi*y/(-y0+y1)))
             /(pow(x0-x1,2)*pow(y0-y1,2));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  const double x0 = 10;
  const double x1 = 1010;
  const double y0 = 10;
  const double y1 = 1010;
  const int k = 10;
  const int m = 10;
  AnSolution an_solution(x0, x1, y0, y1, k, m);
  RhsFunction rhs_function(x0, x1, y0, y1, k, m);

  { // ---------------------- rectangles -----------------------
    // expected relative errors
    const double rel_errors[] = { 7.1923149831760244e-17,
                                  8.1086539555732628e+01,
                                  2.0468688166036429e+01,
                                  1.1675232707313841e+00,
                                  2.6743857927129905e-01,
                                  1.5242339044671113e-01,
                                  1.3725958128657559e-01,
                                  1.3685985019120428e-01 };
    const double tol = 1e-14;

    const std::string fname = ""; //"rect_aper_oscil_";

    for (int i = 0; i < N_times; ++i)
    {
      check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution,  // analytic solution
                                         rhs_function, // rhs function
                                         coef_stif,      // coefficient = 1
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells,   // set n of mesh cells
                                                         // from previous solution
                                         show_results,
                                         fname,
                                         x0, x1, y0, y1);

      EXPECT_NEAR(cur_error, rel_errors[i], tol);

      prev_error = cur_error;
      prev_n_cells = cur_n_cells;
    }
  }

  { // ---------------------- triangles -----------------------
    // expected relative errors
    const double rel_errors[] = { 8.1246544851046949e+01,
                                  2.0931640191453056e+01,
                                  2.0308001686137167e+01,
                                  8.3167079420013081e+00,
                                  4.2367576744204616e-01,
                                  2.1102977613750379e-01,
                                  1.4426719565941382e-01,
                                  1.3720147957282150e-01 };
    const double tol = 1e-14;

    const std::string fname = ""; //"rect_aper_oscil_tria_";

    for (int i = 0; i < N_times; ++i)
    {
      check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution,  // analytic solution
                                         rhs_function, // rhs function
                                         coef_stif,      // coefficient = 1
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells,   // set n of mesh cells
                                                         // from previous solution
                                         show_results,
                                         fname,
                                         x0, x1, y0, y1);

      EXPECT_NEAR(cur_error, rel_errors[i], tol);

      prev_error = cur_error;
      prev_n_cells = cur_n_cells;
    }
  }
}
#endif

// =============================================================================
//
// Test for rectangular and triangular meshes and periodically oscillating
// solution and non-constant coefficient a
//
// =============================================================================
#if 1
TEST(Elliptic2D, solution_rect_and_tria_oscill_solution_coef_a)
{
  class AnSolution: public Function {
  public:
    AnSolution(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return sin(k*pi*x/(x1-x0)) * sin(m*pi*y/(y1-y0));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  class CoefficientA: public Function {
  public:
    CoefficientA(double x0_, double x1_, double y0_, double y1_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return sin(pi*x/(x1-x0)) * sin(pi*y/(y1-y0));
    }
  private:
    double x0, x1, y0, y1;
  };

  class RhsFunction: public Function {
  public:
    RhsFunction(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
      : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
    virtual double value(const Point &point) const {
      const double x = point.x(), y = point.y();
      const double pi = math::PI;
      return (pi*pi*(m*m*pow(x0-x1,2)+k*k*pow(y0-y1,2))*sin((k*pi*x)/(-x0+x1))*
              sin((m*pi*y)/(-y0+y1))*sin(pi*x/(-x0+x1))*sin(pi*y/(-y0+y1)))
             /(pow(x0-x1,2)*pow(y0-y1,2));
    }
  private:
    double x0, x1, y0, y1;
    int k, m;
  };

  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 8; // how many times to repeat the computation
  const int n_beg   = 1; // start number of cells in x- and y-directions

  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int k = 10;
  const int m = 10;
  AnSolution an_solution(x0, x1, y0, y1, k, m);
  CoefficientA coef_a(x0, x1, y0, y1);
  RhsFunction rhs_function(x0, x1, y0, y1, k, m);

  { // ------------------------ rectangles -----------------------
    // expected relative errors
    const double rel_errors[] = { 0.0000000000000000e+00,
                                  3.2698681336964519e+01,
                                  2.3089501678753997e+01,
                                  9.8772660494367992e-01,
                                  3.3505018505015044e-01,
                                  1.8814516646677745e-01,
                                  1.9583943383684449e-01,
                                  2.2267992101104472e-01 };
    const double tol = 6e-13;

    const std::string fname = ""; //"rect_per_oscil_coef_a";

    for (int i = 0; i < N_times; ++i)
    {
      check_elliptic_solution_rectangles(pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution,  // analytic solution
                                         rhs_function, // rhs function
                                         coef_a,       // non-const coefficient a
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells,   // set n of mesh cells
                                                         // from previous solution
                                         show_results,
                                         fname,
                                         x0, x1, y0, y1);

      EXPECT_NEAR(cur_error, rel_errors[i], tol);

      prev_error = cur_error;
      prev_n_cells = cur_n_cells;
    }
  }

  { // ------------------------ triangles -----------------------
    // expected relative errors
    const double rel_errors[] = { 3.9895511204659165e+01,
                                  2.2742578157349712e+01,
                                  7.2087417030959520e+00,
                                  6.1869056716754249e+00,
                                  7.1677099502806896e-01,
                                  7.3467931196967595e-01,
                                  2.0192395753161033e-01,
                                  2.2666670682176887e-01 };
    const double tol = 6e-13;

    const std::string fname = ""; //"rect_per_oscil_coef_a_tria_";

    for (int i = 0; i < N_times; ++i)
    {
      check_elliptic_solution_triangles(pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution,  // analytic solution
                                         rhs_function, // rhs function
                                         coef_a,       // non-const coefficient a
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells,   // set n of mesh cells
                                                         // from previous solution
                                         show_results,
                                         fname,
                                         x0, x1, y0, y1);

      EXPECT_NEAR(cur_error, rel_errors[i], tol);

      prev_error = cur_error;
      prev_n_cells = cur_n_cells;
    }
  }
}
#endif


#endif // TEST_FEMPLUS_ELLIPTIC2D_HPP
