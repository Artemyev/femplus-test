#ifndef FEMPLUS_TEST_GMSFEM_ELLIPTIC_ELASTIC_2D_HPP
#define FEMPLUS_TEST_GMSFEM_ELLIPTIC_ELASTIC_2D_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/gms_dof_handler.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/overlap_coarse_fe_mesh.hpp"
#include "femplus/vector.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/coarse_finite_element.hpp"
#include "femplus/overlap_coarse_finite_element.hpp"
#include "femplus/solver.hpp"

using namespace femplus;

// =============================================================================
//
//
//
// =============================================================================
#if 1
class CoefC: public Function
{
public:
  virtual Vector values(const Point &point) const {
    const double rho = 1000.;
    const double vp = 3000.;
    const double vs = 2000.;
    Vector coefvalues(6);
    coefvalues(0) = rho*vp*vp;
    coefvalues(1) = rho*(vp*vp - 2.*vs*vs);
    coefvalues(2) = 0;
    coefvalues(3) = rho*vp*vp;
    coefvalues(4) = 0;
    coefvalues(5) = rho*vs*vs;
    return coefvalues;
  }
};
class ConstRHS: public Function
{
public:
  virtual Vector values(const Point &point) const {
    return Vector(2, 1.);
  }
};

TEST(GMsFEM_Over_elliptic_elastic, rect_const_rhs)
{
  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int Nx = 5;
  const int Ny = 5;
  const int nx = 10;
  const int ny = 10;

  RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                FineMeshType::Rectangular));

  cmesh->build_coarse_mesh();
  cmesh->build_fine_mesh();

  const int coarse_order = 1;
  const int fine_order = 1;
  const int n_dofs_per_node = 2; // ELASTIC 2D
  const int n_overlap_elements = 1;
  double t0 = get_wall_time();
  CoarseFEMeshPtr fe_mesh(new OverlapCoarseFEMesh(cmesh,
                                                  coarse_order,
                                                  fine_order,
                                                  n_dofs_per_node,
                                                  n_overlap_elements));
  double t1 = get_wall_time();
  std::cout << "time of creation overlapping FE mesh: " << t1 - t0 << std::endl;

  int n_boundary_bf = 5;
  int n_interior_bf = 5;
  std::shared_ptr<Function> coef_rho(new ConstantFunction(1.));
  std::shared_ptr<Function> coef_C(new CoefC());

  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  const bool show_time = true;
  fe_mesh->compute_basis(coef_rho,
                         coef_C,
                         n_boundary_bf,
                         n_interior_bf,
                         LAPACK,
                         LAPACK,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         boundary_edge_mass_matrix,
                         show_time);

//  PetscMatrixPtr intBF;
//  for (int i = 0; i < fe_mesh->n_elements(); ++i)
//    fe_mesh->element(i)->compute_interior_basis(coef_C,
//                                                n_interior_bf,
//                                                JADAMILU,
//                                                intBF,
//                                                true);

//  for (int i = 0; i < 1; ++i)  //fe_mesh->n_elements(); ++i)
//    fe_mesh->element(i)->write_boundary_bf("over_boundary_" + d2s(i) + ".vtu", false);

//  t0 = get_wall_time();
//  const bool in_cells = true;
//  fe_mesh->write_interior_basis("rect_coarse_rect_fine_elastic_interior.vtu", in_cells);
//  fe_mesh->write_boundary_basis("rect_coarse_rect_fine_elastic_boundary.vtu", in_cells);
//  t1 = get_wall_time();
//  std::cout << "time of writing basis functions: " << t1 - t0 << std::endl;


#if defined(SHOW_MAT)
  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    std::cout << "\n\nR matrix for " << el << " elm\n";
    fe_mesh->element(el)->R()->full_view(std::cout);
  }
#endif

  const int gamma = 100;
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();

  mesh->numerate_edges();

  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  PatternPtr mass_pattern(new CSRPattern(connections));

  connections.clear();
  dof_handler.dg_dofs_connections(connections);
  PatternPtr dg_pattern(new CSRPattern(connections));

  Vector system_rhs; // right hand side vector
  Vector solution; // numerical solution

  PetscMatrix system_mat(*dg_pattern.get()); // global DG matrix
  PetscMatrix mass_mat(*mass_pattern.get()); // global mass matrix for RHS

  dof_handler.build_mass_matrix(*coef_rho.get(), mass_mat);
  mass_mat.final_assembly();

  dof_handler.build_stiffness_matrix(*coef_C.get(), system_mat);
  dof_handler.add_interior_edges_matrices(*coef_C.get(), system_mat);
  system_mat.final_assembly();

#if defined(SHOW_MAT)
  std::cout << "DG MAT\n";
  system_mat.full_view(std::cout);
  std::cout << "\n\ncoarse MASS MAT\n";
  mass_mat.full_view(std::cout);
#endif

  ConstRHS rhs_function;
  //RHSFunction1 rhs_function;
  dof_handler.build_vector(rhs_function, system_rhs);

  const bool in_cells = true;
  dof_handler.write_solution_vtp("./",
                                 "rhs_rect_elast_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx) + ".vtp",
                                 system_rhs,
                                 "system_rhs",
                                 in_cells);

#if defined(SHOW_MAT)
  std::cout << "\n\nsystem_rhs before dirichlet\n";
  std::cout << system_rhs << "\n";
#endif

//  ConstantFunction analytic_solution(1);
//  dof_handler.dirichlet(coef_laplace, analytic_solution, system_rhs);

#if defined(SHOW_MAT)
  std::cout << "\n\nsystem_rhs after dirichlet\n";
  std::cout << system_rhs << "\n";
#endif

  Solver solver(system_mat);
  solver.solve(system_rhs, solution);

#if defined(SHOW_MAT)
  std::cout << "\n\nsolution\n";
  std::cout << solution << "\n";
#endif

//  Vector fine_scale_solution = dof_handler.to_fine_scale(solution);

#if defined(SHOW_MAT)
  std::cout << "\n\nfine scale solution\n";
  std::cout << fine_scale_solution << "\n";
#endif

  dof_handler.write_solution_vtp("./",
                                 "rect_const_rhs_rect_elast_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx) + ".vtp",
                                 solution,
                                 "coarse_solution",
                                 in_cells);
}
#endif

#endif // FEMPLUS_TEST_GMSFEM_ELLIPTIC_ELASTIC_2D_HPP
