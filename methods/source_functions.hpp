#ifndef SOURCE_FUNCTIONS_HPP
#define SOURCE_FUNCTIONS_HPP

#include "femplus/function.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/math_functions.hpp"

#include <fstream>
#include <map>


//==============================================================================
class GaussianScalarThis: public femplus::Function
{
public:
  GaussianScalarThis(double support, const femplus::Point &center)
    : _support(support), _center(center) { }
  double value(const femplus::Point &p) const {
    const double x = p.x();
    const double y = p.y();
    const double xc = _center.x();
    const double yc = _center.y();
    const double h = _support;
    const double G = exp(-((x-xc)*(x-xc)+(y-yc)*(y-yc))/(h*h));
    return G;
  }
private:
  double _support; // source support
  femplus::Point _center; // source center
};
//==============================================================================
class Ricker: public femplus::Function
{
public:
  Ricker(double f_) : f(f_) { }
  double value(const femplus::Point &p) const {
    const double t = _time;
    const double t0 = 1./f;
    const double pi = femplus::math::PI;
    const double part = pi*pi*f*f*(t-t0)*(t-t0);
    const double R = (1. - 2.*part) * exp(-part);
    return R * 1e+6;
  }
private:
  double f; // source frequency
};
//==============================================================================
class CoefRhoLayers: public femplus::Function {
public:
  CoefRhoLayers(const std::string &filename) {
    std::ifstream in(filename.c_str());
    require(in, "File " + filename + " can't be opened");
    int layer;
    double rho, vp;
    std::string tmp;
    while (in >> layer) {
      in >> rho >> vp; getline(in, tmp);
      _map[layer] = rho;
    }
    in.close();
  }
  virtual double value(const femplus::Point &p) const {
    if (_map.find(_material_id) == _map.end())
      require(false, "Unknown material");
    return _map.find(_material_id)->second;
  }
private:
  std::map<int, double> _map;
};
//==============================================================================
class CoefKappaLayers: public femplus::Function {
public:
  CoefKappaLayers(const std::string &filename) {
    std::ifstream in(filename.c_str());
    require(in, "File " + filename + " can't be opened");
    int layer;
    double rho, vp;
    std::string tmp;
    while (in >> layer) {
      in >> rho >> vp; getline(in, tmp);
      _map[layer] = rho * vp * vp;
    }
    in.close();
  }
  virtual double value(const femplus::Point &p) const {
    if (_map.find(_material_id) == _map.end())
      require(false, "Unknown material");
    return _map.find(_material_id)->second;
  }
private:
  std::map<int, double> _map;
};
//==============================================================================
class CoefVpLayers: public femplus::Function {
public:
  CoefVpLayers(const std::string &filename) {
    std::ifstream in(filename.c_str());
    require(in, "File " + filename + " can't be opened");
    int layer;
    double rho, vp;
    std::string tmp;
    while (in >> layer) {
      in >> rho >> vp; getline(in, tmp);
      _map[layer] = vp;
    }
    in.close();
  }
  virtual double value(const femplus::Point &p) const {
    if (_map.find(_material_id) == _map.end())
      require(false, "Unknown material");
    return _map.find(_material_id)->second;
  }
private:
  std::map<int, double> _map;
};

#endif // SOURCE_FUNCTIONS_HPP
