#ifndef TEST_FEMPLUS_DG_ELLIPTIC2D_AUX_HPP
#define TEST_FEMPLUS_DG_ELLIPTIC2D_AUX_HPP

#include "config.hpp"
#include "femplus/function.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/dg_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/vector.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/solver.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/rectangle.hpp"
#include "femplus/mesh_element.hpp"
#if defined(USE_BOOST)
  #include "boost/format.hpp"
#endif

using namespace femplus;

// =============================================================================
//
// solution of an elliptic 2D problem with Dirichlet boundary condition
// by DG method
//
// =============================================================================
void check_dg_elliptic_solution(MeshPtr mesh,
                                double gamma,
                                const Function &an_solution,
                                const Function &rhs_function,
                                double &current_rel_error,
                                int &current_n_elements,
                                double prev_rel_error,
                                int prev_n_elements)
{
  const int order = 1;
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order));

  DGDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();
  dof_handler.distribute_boundary_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  PatternPtr mass_pattern(new CSRPattern(connections));

  connections.clear();
  dof_handler.dg_dofs_connections(connections);
  PatternPtr dg_pattern(new CSRPattern(connections));

  Vector system_rhs; // right hand side vector
  Vector solution; // numerical solution
  Vector exact_solution; // analytic solution

  PetscMatrix system_mat(*dg_pattern.get()); // global stiffness matrix
  PetscMatrixPtr mass_mat(new PetscMatrix(*mass_pattern.get())); // global mass matrix for RHS

  dof_handler.build_stiffness_matrix(ConstantFunction(1.), system_mat);
  dof_handler.add_boundary_edges_matrices(ConstantFunction(1.), system_mat);
  dof_handler.add_interior_edges_matrices(ConstantFunction(1.), system_mat);
  system_mat.final_assembly();

  dof_handler.build_mass_matrix(ConstantFunction(1.), *mass_mat.get());
  mass_mat->final_assembly();

  dof_handler.build_vector(mass_mat, rhs_function, system_rhs);

//  dof_handler.dirichlet(system_mat, an_solution, &system_rhs);

  Solver solver(system_mat);
  solver.solve(system_rhs, solution);

  dof_handler.init_vector(an_solution, exact_solution);

  if (math::L2_norm(exact_solution) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE)
  {
    // if the exact solution is 0, that means that we just want to print the
    // solution
    const std::string fname = TESTOUT_DIR + "dg_elliptic_N_" +
                              d2s<int>(mesh->n_elements()) + ".vtu";
    dof_handler.write_solution(fname, std::vector<Vector>(1, solution),
                               std::vector<std::string>(1, "sol"));

    current_rel_error = -1.;
  }
  else
  {
    // comparison between the exact and numerical solutions
    current_rel_error = math::rel_error(exact_solution, solution);

    const double errors_ratio = prev_rel_error / current_rel_error;
    const double n_cells_ratio = 1. * current_n_elements / prev_n_elements;

#if defined(USE_BOOST)
    using boost::format;
    std::cout << format("ncells = %5d rel_error = %2.8e "
                        "error_reduction = %2.8e mesh_reduction = %5g\n")
                 % mesh->n_elements()
                 % current_rel_error
                 % (fabs(prev_rel_error + 1) < 1e-8 ? 0 : errors_ratio)
                 % (prev_n_elements == -1 ? 0 : n_cells_ratio);
#else
    std::cout << " ncells = " << mesh->n_elements()
              << " rel_error = " << current_rel_error
              << " error_reduction = " << (fabs(prev_rel_error + 1) < 1e-8 ? 0 : errors_ratio)
              << " mesh_reduction = " << (prev_n_elements == -1 ? 0 : n_cells_ratio)
              << "\n";
#endif
  }
}

// =============================================================================
//
// solution of an elliptic 2D problem with zero Dirichlet boundary condition
// and heterogeneous coefficient by DG method
//
// =============================================================================
void check_dg_elliptic_solution_hetero_coef(MeshPtr mesh,
                                double gamma,
                                const Function &an_solution,
                                const Function &rhs_function,
                                double &current_rel_error,
                                int &current_n_elements,
                                double prev_rel_error,
                                int prev_n_elements,
                                const Function &coefficient,
                                const std::string &suffix)
{
  const int order = 1;
  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order));

  DGDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  dof_handler.numerate_edges();
  dof_handler.distribute_boundary_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  PatternPtr mass_pattern(new CSRPattern(connections));

  connections.clear();
  dof_handler.dg_dofs_connections(connections);
  PatternPtr dg_pattern(new CSRPattern(connections));

  Vector system_rhs; // right hand side vector
  Vector solution; // numerical solution
  Vector exact_solution; // analytic solution

  PetscMatrix system_mat(*dg_pattern.get()); // global stiffness matrix
  PetscMatrixPtr L2_mat(new PetscMatrix(*mass_pattern.get())); // global mass matrix for RHS

  dof_handler.build_stiffness_matrix(coefficient, system_mat);
  dof_handler.add_boundary_edges_matrices(coefficient, system_mat);
  dof_handler.add_interior_edges_matrices(coefficient, system_mat);
  system_mat.final_assembly();

  dof_handler.build_L2_matrix(*L2_mat.get());
  L2_mat->final_assembly();

  dof_handler.build_vector(L2_mat, rhs_function, system_rhs);

//  dof_handler.dirichlet(system_mat, an_solution, &system_rhs);

  Solver solver(system_mat);
  solver.solve(system_rhs, solution);

  dof_handler.init_vector(an_solution, exact_solution);

  if (math::L2_norm(exact_solution) < math::FLOAT_NUMBERS_EQUALITY_TOLERANCE)
  {
    // if the exact solution is 0, that means that we just want to print the
    // solution
    const std::string fname = TESTOUT_DIR + "dg_elliptic_" + suffix + "_N_" +
                              d2s<int>(mesh->n_elements()) + "_hetero_coef.vtu";
    dof_handler.write_solution(fname, std::vector<Vector>(1, solution),
                               std::vector<std::string>(1, "sol"));

    current_rel_error = -1.;
  }
  else
  {
    // comparison between the exact and numerical solutions
    current_rel_error = math::rel_error(exact_solution, solution);

    const double errors_ratio = prev_rel_error / current_rel_error;
    const double n_cells_ratio = 1. * current_n_elements / prev_n_elements;

#if defined(USE_BOOST)
    using boost::format;
    std::cout << format("ncells = %5d rel_error = %2.8e "
                        "error_reduction = %2.8e mesh_reduction = %5g\n")
                 % mesh->n_elements()
                 % current_rel_error
                 % (fabs(prev_rel_error + 1) < 1e-8 ? 0 : errors_ratio)
                 % (prev_n_elements == -1 ? 0 : n_cells_ratio);
#else
    std::cout << " ncells = " << mesh->n_elements()
              << " rel_error = " << current_rel_error
              << " error_reduction = " << (fabs(prev_rel_error + 1) < 1e-8 ? 0 : errors_ratio)
              << " mesh_reduction = " << (prev_n_elements == -1 ? 0 : n_cells_ratio)
              << "\n";
#endif
  }
}

// -----------------------------------------------------------------------------
void check_dg_elliptic_solution_rectangles(double gamma,
                                           unsigned int N_FINE_X,
                                           unsigned int N_FINE_Y,
                                           const Function &an_solution,
                                           const Function &rhs_function,
                                           double &current_rel_error,
                                           int &current_n_elements,
                                           double prev_rel_error = 0,
                                           int prev_n_elements = -1,
                                           const double X_BEG = 0,
                                           const double X_END = 1,
                                           const double Y_BEG = 0,
                                           const double Y_END = 1)
{
  MeshPtr mesh(new RectangularMesh(X_BEG, X_END,
                                   Y_BEG, Y_END,
                                   N_FINE_X, N_FINE_Y));
  mesh->build();
  current_n_elements = mesh->n_elements();

  check_dg_elliptic_solution(mesh,
                             gamma,
                             an_solution,
                             rhs_function,
                             current_rel_error,
                             current_n_elements,
                             prev_rel_error,
                             prev_n_elements);
}

// -----------------------------------------------------------------------------
class HeteroCoef: public Function {
public:
  HeteroCoef(double Y_BEG_, double Y_END_): Y_BEG(Y_BEG_), Y_END(Y_END_) {}
  double value(const Point &point) const {
    if (point.y() < Y_BEG + 0.2*(Y_END - Y_BEG))
      return 0.1;
    else return 1.;
  }
private:
  double Y_BEG, Y_END;
};

void check_dg_elliptic_solution_rectangles_hetero_coef(double gamma,
                                           unsigned int N_FINE_X,
                                           unsigned int N_FINE_Y,
                                           const Function &an_solution,
                                           const Function &rhs_function,
                                           double &current_rel_error,
                                           int &current_n_elements,
                                           double prev_rel_error = 0,
                                           int prev_n_elements = -1,
                                           const double X_BEG = 0,
                                           const double X_END = 1,
                                           const double Y_BEG = 0,
                                           const double Y_END = 1)
{
  MeshPtr mesh(new RectangularMesh(X_BEG, X_END,
                                   Y_BEG, Y_END,
                                   N_FINE_X, N_FINE_Y));
  mesh->build();
  current_n_elements = mesh->n_elements();

  HeteroCoef coefficient(Y_BEG, Y_END);

  check_dg_elliptic_solution_hetero_coef(mesh,
                             gamma,
                             an_solution,
                             rhs_function,
                             current_rel_error,
                             current_n_elements,
                             prev_rel_error,
                             prev_n_elements,
                             coefficient,
                             "rect");
}

// -----------------------------------------------------------------------------
void check_dg_elliptic_solution_triangles(double gamma,
                                          unsigned int N_FINE_X,
                                          unsigned int N_FINE_Y,
                                          const Function &an_solution,
                                          const Function &rhs_function,
                                          double &current_rel_error,
                                          int &current_n_elements,
                                          double prev_rel_error = 0,
                                          int prev_n_elements = -1,
                                          const double X_BEG = 0,
                                          const double X_END = 1,
                                          const double Y_BEG = 0,
                                          const double Y_END = 1)
{
  std::vector<Point> points;
  points.push_back(Point(X_BEG, Y_BEG));
  points.push_back(Point(X_END, Y_BEG));
  points.push_back(Point(X_BEG, Y_END));
  points.push_back(Point(X_END, Y_END));
  MeshElementPtr rectangle(new Rectangle(0, 1, 2, 3));
  MeshPtr mesh(new TriangularMesh(points, rectangle, N_FINE_X));
  mesh->build();
  current_n_elements = mesh->n_elements();

  check_dg_elliptic_solution(mesh,
                             gamma,
                             an_solution,
                             rhs_function,
                             current_rel_error,
                             current_n_elements,
                             prev_rel_error,
                             prev_n_elements);
}

// -----------------------------------------------------------------------------
void check_dg_elliptic_solution_triangles_hetero_coef(double gamma,
                                          unsigned int N_FINE_X,
                                          unsigned int N_FINE_Y,
                                          const Function &an_solution,
                                          const Function &rhs_function,
                                          double &current_rel_error,
                                          int &current_n_elements,
                                          double prev_rel_error = 0,
                                          int prev_n_elements = -1,
                                          const double X_BEG = 0,
                                          const double X_END = 1,
                                          const double Y_BEG = 0,
                                          const double Y_END = 1)
{
  std::vector<Point> points;
  points.push_back(Point(X_BEG, Y_BEG));
  points.push_back(Point(X_END, Y_BEG));
  points.push_back(Point(X_BEG, Y_END));
  points.push_back(Point(X_END, Y_END));
  MeshElementPtr rectangle(new Rectangle(0, 1, 2, 3));
  MeshPtr mesh(new TriangularMesh(points, rectangle, N_FINE_X));
  mesh->build();
  current_n_elements = mesh->n_elements();

  HeteroCoef coefficient(Y_BEG, Y_END);

  check_dg_elliptic_solution_hetero_coef(mesh,
                             gamma,
                             an_solution,
                             rhs_function,
                             current_rel_error,
                             current_n_elements,
                             prev_rel_error,
                             prev_n_elements,
                             coefficient,
                             "tria");
}



#endif // TEST_FEMPLUS_DG_ELLIPTIC2D_AUX_HPP
