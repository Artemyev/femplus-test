#ifndef TEST_FEMPLUS_GMSFEM_ELLIPTIC2D_HPP
#define TEST_FEMPLUS_GMSFEM_ELLIPTIC2D_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "analytic_functions.hpp"
#include "elliptic2D_aux.hpp"

#include "femplus/coarse_mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/vector.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/solver.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/gms_dof_handler.hpp"
#include "femplus/mixed_poly_rect_mesh.hpp"
#include "femplus/coarse_mixed_poly_rect_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/coarse_finite_element.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/coarse_rectangle.hpp"
#include "femplus/triangular_mesh.hpp"

#if defined(USE_BOOST)
  #include "boost/format.hpp"
#endif

using namespace femplus;

//#define SHOW_MAT

//******************************************************************************
namespace gmsfem_elliptic_2D_an_solution {

class AnSolution: public Function
{
public:
  virtual double value(const Point &point) const {
    const double x = point.x();
    const double y = point.y();
    const double pi = math::PI;
    double sum = 0.;
    for (int k = 1; k < 30; k += 2)
      sum += sin(k*pi*0.5*(1+x))/(k*k*k*sinh(k*pi)) * (sinh(k*pi*0.5*(1+y)) + sinh(k*pi*0.5*(1-y)));
    return (0.5*(1-x*x) - 16*sum/(pi*pi*pi));
  }
};

#if 0 // just to watch the analytic solution
TEST(watch_function, watch)
{
  MeshPtr mesh(new RectangularMesh(-1, 1, -1, 1, 100, 100));
  mesh->build();
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, 1));
  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  Vector vec;
  dof_handler.init_vector(AnSolution(), vec);
  using namespace std;
  dof_handler.write_solution_vtu("delme.vtu", vector<Vector>(1, vec), vector<string>(1,"bla"));
}
#endif

// PARAMETERS
const double x0 = -1;
const double x1 =  1;
const double y0 = -1;
const double y1 =  1;
const int Nx = 10;
const int Ny = 10;
const int nx = 10;
const int ny = 10;
const int coarse_order = 1;
const int fine_order   = 1;

// --- R matrix ---
const int n_boundary_bf[] = { 7, 9, 11, 13, 15, -1 };
const int n_interior_bf[] = { 5, -1 };
Eigensolver bound_eigensolver     = KRYLOV;
Eigensolver inter_eigensolver     = KRYLOV;
const bool bound_regularization   = true;
const bool inter_regularization   = true;
const bool bound_take_first_eigen = true;
const bool inter_take_first_eigen = true;
const bool bound_edge_mass        = true;
const bool show_time_eig          = false;

const double gamma  = 100.;
const double coef_a = 1.;
const bool add_boundary_edges = true;
const bool add_interior_edges = true;
ConstantFunction rhs_function(1.);
FunctionPtr coef_stif(new ConstantFunction(coef_a));

const bool ref_numerical = false; //true; //false;



// =============================================================================
//
// Model with rectangular domain [-1,1]^2, where the analytical solution is
// known. Source function is 1, boundary condition is zero Dirichlet.
// The values of the analytical solution are computed at the same points where
// the GMsFEM solution is. Fine mesh is rectangular.
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_rect_coarse, solution_rectangles)
{
  std::string fname = "R_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
  fname = ""; // no output

  MeshPtr an_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));
  if (ref_numerical) an_mesh->build();

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  for (int ni = 0; ni < n_times_interior; ++ni)
  {
    const int n_in = n_interior_bf[ni];
    std::map<int, double> reference_error; // key is the number of boundary basis functions
    if (n_in == 5) {
      reference_error[7] = 4.0502285708777397e-02;
      reference_error[9] = 3.8066184163276888e-02;
      reference_error[11]= 1.8970216730410203e-02;
      reference_error[13]= 1.7414494765352815e-02;
      reference_error[15]= 6.1762968404902533e-03;
      reference_error[-1]= 2.8474788334182304e-04;
    }
    else if (n_in == -1) {
      reference_error[7] = 4.0244975668626032e-02;
      reference_error[9] = 3.7816927268680800e-02;
      reference_error[11]= 1.8755528332357906e-02;
      reference_error[13]= 1.7223732640290333e-02;
      reference_error[15]= 6.1048410431840056e-03;
      reference_error[-1]= 9.0314592054470896e-05;
    }

    for (int nb = 0; nb < n_times_boundary; ++nb)
    {
      const int n_bo = n_boundary_bf[nb];

      double refer_error = -1;
      if (reference_error.find(n_bo) != reference_error.end())
        refer_error = reference_error.find(n_bo)->second;

      RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
      CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                    FineMeshType::Rectangular));

      cmesh->build_coarse_mesh();
      cmesh->build_fine_mesh();
      mesh->numerate_edges();

      Vector solution;
      double rel_error =
      gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                      n_bo, n_in, bound_eigensolver,
                      inter_eigensolver, bound_regularization,
                      inter_regularization, bound_take_first_eigen,
                      inter_take_first_eigen, bound_edge_mass,
                      show_time_eig, gamma, add_boundary_edges,
                      add_interior_edges, rhs_function, fname,
                      solution, an_mesh, AnSolution(), ref_numerical);

      if (refer_error > 0) { // if reference error is known for this case
        const double tol = 1e-10;
        EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
      }
      else {
        std::cout.precision(16);
        std::cout.setf(std::ios::scientific);
        std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                  << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                  << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                  << std::endl;
      }
    }
  }
}
#endif

// =============================================================================
//
// Model with rectangular domain [-1,1]^2, where the analytical solution is
// known. Source function is 1, boundary condition is zero Dirichlet.
// The values of the analytical solution are computed at the same points where
// the GMsFEM solution is. Fine mesh is structured triangular, therefore the
// results should be the same as for the case of rectangular fine grid.
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_rect_coarse, solution_stuctured_triangles)
{
  std::string fname = "ST_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
  fname = ""; // no output

  MeshPtr an_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  for (int ni = 0; ni < n_times_interior; ++ni)
  {
    const int n_in = n_interior_bf[ni];
    std::map<int, double> reference_error; // key is the number of boundary basis functions
    if (n_in == 5) {
      reference_error[7] = 2.3123232700996255e-01;
      reference_error[9] = 3.5921379682761927e-02;
      reference_error[11]= 2.2956370877903913e-02;
      reference_error[13]= 1.2393394076063794e-02;
      reference_error[15]= 5.1852807682460150e-03;
      reference_error[-1]= 9.6695295974355902e-04;
    }
    else if (n_in == -1) {
      reference_error[7] = 2.2839720799467550e-01;
      reference_error[9] = 3.5440749884828239e-02;
      reference_error[11]= 2.2524523471234729e-02;
      reference_error[13]= 1.1988041417496117e-02;
      reference_error[15]= 4.7886269712665349e-03;
      reference_error[-1]= 9.1843993840530510e-05;
    }

    for (int nb = 0; nb < n_times_boundary; ++nb)
    {
      const int n_bo = n_boundary_bf[nb];

      double refer_error = -1;
      if (reference_error.find(n_bo) != reference_error.end())
        refer_error = reference_error.find(n_bo)->second;

      RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
      CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                    FineMeshType::StructuredTriangular));

      cmesh->build_coarse_mesh();
      cmesh->build_fine_mesh();
      mesh->numerate_edges();

      cmesh->write_msh("gms_struct_tria.msh");

      Vector solution;
      double rel_error =
      gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                      n_bo, n_in, bound_eigensolver,
                      inter_eigensolver, bound_regularization,
                      inter_regularization, bound_take_first_eigen,
                      inter_take_first_eigen, bound_edge_mass,
                      show_time_eig, gamma, add_boundary_edges,
                      add_interior_edges, rhs_function, fname,
                      solution, an_mesh, AnSolution());

      if (refer_error > 0) { // if reference error is known for this case
        const double tol = 1e-10;
        EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
      }
      else {
        std::cout.precision(16);
        std::cout.setf(std::ios::scientific);
        std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                  << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                  << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                  << std::endl;
      }
    }
  }
}
#endif



// =============================================================================
//
// Model with rectangular domain [-1,1]^2, where the analytical solution is
// known. Source function is 1, boundary condition is zero Dirichlet.
// The values of the analytical solution are computed at the vertices of a
// virtual rectangular grid, and the values computed with GMsFEM are recomputed
// at the same points for comparison. Fine mesh is unstructured triangular
// everywhere.
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_rect_coarse, solution_triangles)
{
  std::string fname = "UT_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
  fname = ""; // no output

  MeshPtr an_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  for (int ni = 0; ni < n_times_interior; ++ni)
  {
    const int n_in = n_interior_bf[ni];
    std::map<int, double> reference_error; // key is the number of boundary basis functions
    if (n_in == 5) {
      reference_error[7] = 4.2999337744834880e-02;
      reference_error[9] = 7.6765245993034664e-02;
      reference_error[11]= 1.9425556285378853e-02;
      reference_error[13]= 1.4221998467424555e-02;
      reference_error[15]= 5.9405999272802113e-03;
      reference_error[-1]= 9.3122901607505665e-04;
    }
    else if (n_in == -1) {
      reference_error[7] = 4.2416454159494008e-02;
      reference_error[9] = 7.6193505751240523e-02;
      reference_error[11]= 1.8935830082731201e-02;
      reference_error[13]= 1.3793048151588525e-02;
      reference_error[15]= 5.5870392579944108e-03;
      reference_error[-1]= 1.5683645535537565e-04;
    }

    for (int nb = 0; nb < n_times_boundary; ++nb)
    {
      const int n_bo = n_boundary_bf[nb];

      double refer_error = -1;
      if (reference_error.find(n_bo) != reference_error.end())
        refer_error = reference_error.find(n_bo)->second;

      RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
      CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                    FineMeshType::Triangular));

      cmesh->build_coarse_mesh();
      cmesh->build_fine_mesh();
      mesh->numerate_edges();

      Vector solution;
      double rel_error =
      gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                      n_bo, n_in, bound_eigensolver,
                      inter_eigensolver, bound_regularization,
                      inter_regularization, bound_take_first_eigen,
                      inter_take_first_eigen, bound_edge_mass,
                      show_time_eig, gamma, add_boundary_edges,
                      add_interior_edges, rhs_function, fname,
                      solution, an_mesh, AnSolution());

      if (refer_error > 0) { // if reference error is known for this case
        const double tol = 1e-10;
        EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
      }
      else {
        std::cout.precision(16);
        std::cout.setf(std::ios::scientific);
        std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                  << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                  << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                  << std::endl;
      }
    }
  }
}
#endif

// =============================================================================
//
// Model with rectangular domain [-1,1]^2, where the analytical solution is
// known. Source function is 1, boundary condition is zero Dirichlet.
// The values of the analytical solution are computed at the vertices of a
// virtual rectangular grid, and the values computed with GMsFEM are recomputed
// at the same points for comparison. Fine mesh is randomly chosen between
// rectangular and unstructured triangular.
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_rect_coarse, solution_mixed_rectangles_triangles)
{
  std::string fname = "RUT_q_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
  fname = ""; // no output

  MeshPtr an_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  for (int ni = 0; ni < n_times_interior; ++ni)
  {
    const int n_in = n_interior_bf[ni];
    std::map<int, double> reference_error; // key is the number of boundary basis functions
    if (n_in == 5) {
      reference_error[7] = 4.2965794671239850e-02;
      reference_error[9] = 3.8153952144017296e-02;
      reference_error[11]= 1.8757128182641396e-02;
      reference_error[13]= 1.6215826738560649e-02;
      reference_error[15]= 6.1707315291163536e-03;
      reference_error[-1]= 6.6393403270236797e-04;
    }
    else if (n_in == -1) {
      reference_error[7] = 4.2542069440613582e-02;
      reference_error[9] = 7.6396687505042318e-02;
      reference_error[11]= 1.7732270603105615e-02;
      reference_error[13]= 1.5582066083663016e-02;
      reference_error[15]= 5.8929599849108704e-03;
      reference_error[-1]= 9.6481085341750106e-05;
    }

    for (int nb = 0; nb < n_times_boundary; ++nb)
    {
      const int n_bo = n_boundary_bf[nb];

      double refer_error = -1;
      if (reference_error.find(n_bo) != reference_error.end())
        refer_error = reference_error.find(n_bo)->second;

      RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
      mesh->build();

      CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny));
      for (int el = 0; el < Nx*Ny; ++el)
      {
        // current element
        Rectangle *elem = dynamic_cast<Rectangle*>(mesh->element(el));
        EXPECT_TRUE(elem != NULL);

        MeshPtr fmesh;
        switch (rand()%2) {
          case 0: {
            fmesh = MeshPtr(new RectangularMesh(mesh->vertex(elem->vertex_number(0)),
                                                mesh->vertex(elem->vertex_number(3)),
                                                nx, ny));
            break;
          }
          case 1: {
            fmesh = MeshPtr(new TriangularMesh(mesh->vertices(),
                                               MeshElementPtr(elem),
                                               nx));
            break;
          }
          default:
            throw std::runtime_error("Unexpected type of a fine mesh");
        }
        CoarseElement *c_elem = new CoarseRectangle(el,     // serial number
                                                    fmesh,  // fine mesh inside
                                                    *elem); // rectangle as a base
        cmesh->add_element(c_elem);
      }

      cmesh->build_fine_mesh();
      mesh->numerate_edges();

      //  for (int i =0; i < mesh->n_edges(); ++i)
      //    std::cout << "edge " << i << ": " << mesh->edge(i)->vertex_number(0)
      //              << " " << mesh->edge(i)->vertex_number(1) << std::endl;

      //  cmesh->write_msh("gms_diff_fine_mesh_" + d2s<int>(Nx) + "_" + d2s<int>(nx) +
      //                   ".msh");

      Vector solution;
      double rel_error =
      gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                      n_bo, n_in, bound_eigensolver,
                      inter_eigensolver, bound_regularization,
                      inter_regularization, bound_take_first_eigen,
                      inter_take_first_eigen, bound_edge_mass,
                      show_time_eig, gamma, add_boundary_edges,
                      add_interior_edges, rhs_function, fname,
                      solution, an_mesh, AnSolution());

      if (refer_error > 0) { // if reference error is known for this case
        const double tol = 1e-10;
        EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
      }
      else {
        std::cout.precision(16);
        std::cout.setf(std::ios::scientific);
        std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                  << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                  << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                  << std::endl;
      }
    }
  }
}
#endif



// =============================================================================
//
// Different number of fine scale rectangles in the coarse elements. This test
// doesn't work very well. Investigate it, when free time is available. But do
// not include in production version of the test suite, because this test
// considers not very common situations.
//
// =============================================================================
#if 0 // read the comment above before uncommenting
TEST(GMsFEMElliptic2D_rect_coarse, solution_random_rectangles)
{
  const double x0 = -1;
  const double x1 = 1;
  const double y0 = -1;
  const double y1 = 1;
  const int Nx = 3;
  const int Ny = 3;
  const int nx = 3;
  const int ny = 3;
  const int coarse_order = 1;
  const int fine_order = 1;

  // --- R matrix ---
  int n_boundary_bf                 = -1;
  int n_interior_bf                 = -1;
  Eigensolver bound_eigensolver     = KRYLOV;
  Eigensolver inter_eigensolver     = KRYLOV;
  const bool bound_regularization   = true;
  const bool inter_regularization   = true;
  const bool bound_take_first_eigen = true;
  const bool inter_take_first_eigen = true;
  const bool bound_edge_mass        = true;
  const bool show_time_eig          = false;

  const int gamma = 100;
  const double coef_a = 1;
  const bool add_boundary_edges = true;
  const bool add_interior_edges = true;
  ConstantFunction rhs_function(1);
  std::string fname = "RR_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
  //fname = ""; // no output

  FunctionPtr coef_stif(new ConstantFunction(coef_a));

  RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  mesh->build();

  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny));
  for (int el = 0; el < Nx*Ny; ++el)
  {
    // current element
    Rectangle *elem = dynamic_cast<Rectangle*>(mesh->element(el));
    EXPECT_TRUE(elem != NULL);

    const int rx = rand()%2; // 0 or 1
    const int ry = rand()%2; // 0 or 1
    MeshPtr fmesh(new RectangularMesh(mesh->vertex(elem->vertex_number(0)),
                                      mesh->vertex(elem->vertex_number(3)),
                                      nx-rx, ny-ry));

    CoarseElement *c_elem = new CoarseRectangle(el,     // serial number
                                                fmesh,  // fine mesh inside
                                                *elem); // rectangle as a base
    cmesh->add_element(c_elem);
  }

  cmesh->build_fine_mesh();
  mesh->numerate_edges();

  cmesh->write_msh("gms_random_rect_fine_mesh_0.msh");

  double rel_error =
  gmsfem_elliptic_rectangles(cmesh, coef_stif, coarse_order, fine_order,
                             n_boundary_bf, n_interior_bf, bound_eigensolver,
                             inter_eigensolver, bound_regularization,
                             inter_regularization, bound_take_first_eigen,
                             inter_take_first_eigen, bound_edge_mass,
                             show_time_eig, gamma, add_boundary_edges,
                             add_interior_edges, rhs_function, fname,
                             AnSolution());

  std::cout << "rel_error = " << rel_error << std::endl;
}
#endif

} // namespace gmsfem_elliptic_2D_an_solution



//******************************************************************************
namespace gmsfem_elliptic_2D_topography {

// PARAMETERS
const double x0    = 0;
const double x1    = 1000;
const double y0    = 0;
const double depth = 1000;
const int n_top_points = 100;
const double amplitude = 20;
const int period = 7;
const int Nx = 5;
const int Ny = 5;
const int nx = 10;
const int ny = 10;
const int coarse_order = 1;
const int fine_order = 1;

// --- R matrix ---
const int n_boundary_bf[] = { 7, 9, 11, 13, 15, -1 };
const int n_interior_bf[] = { 5, -1 };
Eigensolver bound_eigensolver     = KRYLOV;
Eigensolver inter_eigensolver     = KRYLOV;
const bool bound_regularization   = true;
const bool inter_regularization   = true;
const bool bound_take_first_eigen = true;
const bool inter_take_first_eigen = true;
const bool bound_edge_mass        = true;
const bool show_time_eig          = false;

const int gamma = 100;
const double coef_a = 1;
const bool add_boundary_edges = true;
const bool add_interior_edges = true;
ConstantFunction rhs_function(1.);
ConstantFunction bound_value(0.);
const bool ref_numerical = true;

FunctionPtr coef_stif(new ConstantFunction(coef_a));


// =============================================================================
//
// Model with top surface topography and triangular fine mesh everywhere
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_topography, solution_tria_tria_small)
{
  std::string fname = "topo_UT_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
  fname = ""; // no output

  std::vector<Point> top_surface;
  for (int i = 0; i < n_top_points + 1; ++i)
  {
    const double x = x0 + i * (x1 - x0) / n_top_points;
    top_surface.push_back(Point(x, amplitude * sin(period*math::PI*x/(x1-x0))));
  }

  std::vector<Point> domain_geo_points(top_surface);
  domain_geo_points.push_back(Point(x1, y0 - depth));
  domain_geo_points.push_back(Point(x0, y0 - depth));
  MeshPtr ref_mesh(new TriangularMesh(domain_geo_points, Nx*nx));

  ref_mesh->build();
  if (fname != "")
    ref_mesh->write("gms_topography_ref.msh");

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  for (int ni = 0; ni < n_times_interior; ++ni)
  {
    const int n_in = n_interior_bf[ni];
    std::map<int, double> reference_error; // key is the number of boundary basis functions
    if (n_in == 5) {
//      reference_error[7] = 4.0502285708777397e-02;
//      reference_error[9] = 3.8066184163276888e-02;
//      reference_error[11]= 1.8970216730410203e-02;
//      reference_error[13]= 1.7414494765352815e-02;
//      reference_error[15]= 6.1762968404902533e-03;
//      reference_error[-1]= 1.8886777103157976e-01;
    }
    else if (n_in == -1) {
//      reference_error[7] = 7.1644397314258076e-02;
//      reference_error[9] = 6.4053081065830741e-02;
//      reference_error[11]= 6.4902357395767746e-02;
//      reference_error[13]= 1.2191761890103681e-02;
//      reference_error[15]= 1.1326080478335259e-02;
//      reference_error[-1]= 2.9807436651194908e-03;
    }

    for (int nb = 0; nb < n_times_boundary; ++nb)
    {
      const int n_bo = n_boundary_bf[nb];

      double refer_error = -1;
      if (reference_error.find(n_bo) != reference_error.end())
        refer_error = reference_error.find(n_bo)->second;

      MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
      CoarseMeshPtr cmesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny,
                                                      FineMeshType::Triangular,
                                                      FineMeshType::Triangular));

      cmesh->build_coarse_mesh();
      cmesh->build_fine_mesh();
      mpr_mesh->numerate_edges();

      if (fname != "")
        cmesh->write_msh("gms_" + fname + ".msh");

      Vector solution;
      const double rel_error =
      gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                      n_bo, n_in, bound_eigensolver,
                      inter_eigensolver, bound_regularization,
                      inter_regularization, bound_take_first_eigen,
                      inter_take_first_eigen, bound_edge_mass,
                      show_time_eig, gamma, add_boundary_edges,
                      add_interior_edges, rhs_function, fname,
                      solution, ref_mesh, bound_value, ref_numerical);

      if (refer_error > 0) { // if reference error is known for this case
        const double tol = 1e-10;
        EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
      }
      else {
        std::cout.precision(16);
        std::cout.setf(std::ios::scientific);
        std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                  << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                  << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                  << std::endl;
      }
    }
  }
}
#endif

// =============================================================================
//
// Model with top surface topography with triangular fine mesh in the near top
// surface layer and rectangular fine mesh elsewhere
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_topography, solution_rect_tria_small)
{
  std::string fname = "topo_UTR_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
  fname = ""; // no output

  std::vector<Point> top_surface;
  for (int i = 0; i < n_top_points + 1; ++i)
  {
    const double x = x0 + i * (x1 - x0) / n_top_points;
    top_surface.push_back(Point(x, amplitude * sin(period*math::PI*x/(x1-x0))));
  }

  std::vector<Point> domain_geo_points(top_surface);
  domain_geo_points.push_back(Point(x1, y0 - depth));
  domain_geo_points.push_back(Point(x0, y0 - depth));
  MeshPtr ref_mesh(new TriangularMesh(domain_geo_points, Nx*nx));

  ref_mesh->build();
  if (fname != "")
    ref_mesh->write("gms_topography_ref.msh");

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  for (int ni = 0; ni < n_times_interior; ++ni)
  {
    const int n_in = n_interior_bf[ni];
    std::map<int, double> reference_error; // key is the number of boundary basis functions
    if (n_in == 5) {
//      reference_error[7] = 4.0502285708777397e-02;
//      reference_error[9] = 3.8066184163276888e-02;
//      reference_error[11]= 1.8970216730410203e-02;
//      reference_error[13]= 1.7414494765352815e-02;
//      reference_error[15]= 6.1762968404902533e-03;
//      reference_error[-1]= 1.8886777103157976e-01;
    }
    else if (n_in == -1) {
//      reference_error[7] = 7.1644397314258076e-02;
//      reference_error[9] = 6.4053081065830741e-02;
//      reference_error[11]= 6.4902357395767746e-02;
//      reference_error[13]= 1.2191761890103681e-02;
//      reference_error[15]= 1.1326080478335259e-02;
//      reference_error[-1]= 2.9807436651194908e-03;
    }

    for (int nb = 0; nb < n_times_boundary; ++nb)
    {
      const int n_bo = n_boundary_bf[nb];

      double refer_error = -1;
      if (reference_error.find(n_bo) != reference_error.end())
        refer_error = reference_error.find(n_bo)->second;

      MixedPolyRectMeshPtr mpr_mesh(new MixedPolyRectMesh(top_surface, y0, depth, Nx, Ny));
      CoarseMeshPtr cmesh(new CoarseMixedPolyRectMesh(mpr_mesh, nx, ny,
                                                      FineMeshType::Rectangular,
                                                      FineMeshType::Triangular));

      cmesh->build_coarse_mesh();
      cmesh->build_fine_mesh();
      mpr_mesh->numerate_edges();

      if (fname != "")
        cmesh->write_msh("gms_" + fname + ".msh");

      Vector solution;
      const double rel_error =
      gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                      n_bo, n_in, bound_eigensolver,
                      inter_eigensolver, bound_regularization,
                      inter_regularization, bound_take_first_eigen,
                      inter_take_first_eigen, bound_edge_mass,
                      show_time_eig, gamma, add_boundary_edges,
                      add_interior_edges, rhs_function, fname,
                      solution, ref_mesh, bound_value, ref_numerical);

      if (refer_error > 0) { // if reference error is known for this case
        const double tol = 1e-10;
        EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
      }
      else {
        std::cout.precision(16);
        std::cout.setf(std::ios::scientific);
        std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                  << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                  << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                  << std::endl;
      }
    }
  }
}
#endif

} // namespace gmsfem_elliptic_2D_topography



//******************************************************************************
namespace gmsfem_elliptic_2D_coef_a_and_rhs {

class AnSolutionPerOscil: public Function {
public:
  AnSolutionPerOscil(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
    : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
  virtual double value(const Point &point) const {
    const double x = point.x(), y = point.y();
    const double pi = math::PI;
    return sin(k*pi*x/(x1-x0)) * sin(m*pi*y/(y1-y0));
  }
private:
  double x0, x1, y0, y1;
  int k, m;
};

class RhsFunctionPerOscil: public Function {
public:
  RhsFunctionPerOscil(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
    : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
  virtual double value(const Point &point) const {
    const double x = point.x(), y = point.y();
    const double pi = math::PI;
    return (k*k*pi*pi*sin((k*pi*x)/(x1-x0))*sin((m*pi*y)/(y1-y0)))/((x1-x0)*(x1-x0)) +
           (m*m*pi*pi*sin((k*pi*x)/(x1-x0))*sin((m*pi*y)/(y1-y0)))/((y1-y0)*(y1-y0));
  }
private:
  double x0, x1, y0, y1;
  int k, m;
};

class AnSolutionAperOscil: public Function {
public:
  AnSolutionAperOscil(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
    : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
  virtual double value(const Point &point) const {
    const double x = point.x(), y = point.y();
    const double pi = math::PI;
    return sin(pi*x/(x1-x0)) * sin(pi*y/(y1-y0)) *
           sin(k*pi*x/(x1-x0)) * sin(m*pi*y/(y1-y0));
  }
private:
  double x0, x1, y0, y1;
  int k, m;
};

class RhsFunctionAperOscil: public Function {
public:
  RhsFunctionAperOscil(double x0_, double x1_, double y0_, double y1_, int k_, int m_)
    : x0(x0_), x1(x1_), y0(y0_), y1(y1_), k(k_), m(m_) { }
  virtual double value(const Point &point) const {
    const double x = point.x(), y = point.y();
    const double pi = math::PI;
    return (pi*pi*(m*m*pow(x0-x1,2)+k*k*pow(y0-y1,2))*sin((k*pi*x)/(-x0+x1))*
            sin((m*pi*y)/(-y0+y1))*sin(pi*x/(-x0+x1))*sin(pi*y/(-y0+y1)))
           /(pow(x0-x1,2)*pow(y0-y1,2));
  }
private:
  double x0, x1, y0, y1;
  int k, m;
};

class CoefficientA: public Function {
public:
  CoefficientA(double x0_, double x1_, double y0_, double y1_)
    : x0(x0_), x1(x1_), y0(y0_), y1(y1_) { }
  virtual double value(const Point &point) const {
    const double x = point.x(), y = point.y();
    const double pi = math::PI;
    return sin(pi*x/(x1-x0)) * sin(pi*y/(y1-y0));
  }
private:
  double x0, x1, y0, y1;
};

// PARAMETERS
const double x0 = 0;
const double x1 = 1000;
const double y0 = 0;
const double y1 = 1000;
const int Nx = 5;
const int Ny = 5;
const int nx = 10;
const int ny = 10;
const int coarse_order = 1;
const int fine_order   = 1;

// --- R matrix ---
const int n_boundary_bf[] = { 7, 9, 11, 13, 15, -1 };
const int n_interior_bf[] = { 5, -1 };
Eigensolver bound_eigensolver     = KRYLOV;
Eigensolver inter_eigensolver     = KRYLOV;
const bool bound_regularization   = true;
const bool inter_regularization   = true;
const bool bound_take_first_eigen = true;
const bool inter_take_first_eigen = true;
const bool bound_edge_mass        = true;
const bool show_time_eig          = false;

const double gamma  = 100.;
const bool add_boundary_edges = true;
const bool add_interior_edges = true;

#if 0 // just to watch the analytic solution
TEST(an_solution, watch)
{
  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, 100, 100));
  mesh->build();
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, 1));
  CGDoFHandler dof_handler(fe_mesh);
  dof_handler.distribute_dofs();
  Vector vec;
  dof_handler.init_vector(AnSolution(x0, x1, y0, y1, 15, 15), vec);
  using namespace std;
  dof_handler.write_solution_vtu("delme.vtu", vector<Vector>(1, vec), vector<string>(1,"bla"));
}
#endif

// =============================================================================
//
// Model with rectangular domain [x0,x1]x[y0,y1], where the analytical solution
// is known. Source function is computed analytically with respect to the
// solution, boundary condition is zero Dirichlet. For this test we compare the
// GMsFEM solution with not only analytical one, but with numerical as well,
// because the numerical solution obtained with standard FEM may give big error,
// and it's interesting to see what method is better. The first test is for
// periodic solution with constant coefficient a.
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_rect, solution_rectangles_per_oscil)
{
  const int k = 10;
  const int m = 10;
  AnSolutionPerOscil an_solution(x0, x1, y0, y1, k, m);
  FunctionPtr coef_stif(new ConstantFunction(1.));
  RhsFunctionPerOscil rhs_function(x0, x1, y0, y1, k, m);

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  { // --------------------- compare with analytical solution ------------------
    std::string fname = "per_oscil_an_R_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
    fname = ""; // no output

    bool ref_numerical = false;
    MeshPtr ref_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));

    for (int ni = 0; ni < n_times_interior; ++ni)
    {
      const int n_in = n_interior_bf[ni];
      std::map<int, double> reference_error; // key is the number of boundary basis functions
      if (n_in == 5) {
        reference_error[7] = 3.2328325279243425e-02;
        reference_error[9] = 3.2331148020830779e-02;
        reference_error[11] = 3.2333142572145701e-02;
        reference_error[13] = 3.2410016952394961e-02;
        reference_error[15] = 3.2410014390605320e-02;
        reference_error[-1] = 3.2412421803159022e-02;
      }
      else if (n_in == -1) {
        reference_error[7] = 3.2325137218663529e-02;
        reference_error[9] = 3.2328013999073354e-02;
        reference_error[11] = 3.2329874283360420e-02;
        reference_error[13] = 3.2404279955303990e-02;
        reference_error[15] = 3.2404283969609857e-02;
        reference_error[-1] = 3.2406656958055578e-02;
      }

      for (int nb = 0; nb < n_times_boundary; ++nb)
      {
        const int n_bo = n_boundary_bf[nb];

        double refer_error = -1;
        if (reference_error.find(n_bo) != reference_error.end())
          refer_error = reference_error.find(n_bo)->second;

        RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
        CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                      FineMeshType::Rectangular));

        cmesh->build_coarse_mesh();
        cmesh->build_fine_mesh();
        mesh->numerate_edges();

        Vector solution;
        double rel_error =
        gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                        n_bo, n_in, bound_eigensolver,
                        inter_eigensolver, bound_regularization,
                        inter_regularization, bound_take_first_eigen,
                        inter_take_first_eigen, bound_edge_mass,
                        show_time_eig, gamma, add_boundary_edges,
                        add_interior_edges, rhs_function, fname,
                        solution, ref_mesh, an_solution, ref_numerical);

        if (refer_error > 0) { // if reference error is known for this case
          const double tol = 1e-10;
          EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
        }
        else {
          std::cout.precision(16);
          std::cout.setf(std::ios::scientific);
          std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                    << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                    << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                    << std::endl;
        }
      }
    }
  }

  { // --------------------- compare with FEM solution ------------------
    std::string fname = "per_oscil_fem_R_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
    fname = ""; // no output

    bool ref_numerical = true;
    MeshPtr ref_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));
    ref_mesh->build();

    for (int ni = 0; ni < n_times_interior; ++ni)
    {
      const int n_in = n_interior_bf[ni];
      std::map<int, double> reference_error; // key is the number of boundary basis functions
      if (n_in == 5) {
        reference_error[7] = 1.4944316490233581e-03;
        reference_error[9] = 1.5442535041902891e-03;
        reference_error[11] = 1.5789859872607690e-03;
        reference_error[13] = 2.2300281542422698e-03;
        reference_error[15] = 2.2299584740829428e-03;
        reference_error[-1] = 2.2498662713869000e-03;
      }
      else if (n_in == -1) {
        reference_error[7] = 1.3793698107452089e-03;
        reference_error[9] = 1.4330076933313481e-03;
        reference_error[11] = 1.4660342674125229e-03;
        reference_error[13] = 2.0972556424927930e-03;
        reference_error[15] = 2.0972839494012596e-03;
        reference_error[-1] = 2.1155914465397522e-03;
      }

      for (int nb = 0; nb < n_times_boundary; ++nb)
      {
        const int n_bo = n_boundary_bf[nb];

        double refer_error = -1;
        if (reference_error.find(n_bo) != reference_error.end())
          refer_error = reference_error.find(n_bo)->second;

        RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
        CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                      FineMeshType::Rectangular));

        cmesh->build_coarse_mesh();
        cmesh->build_fine_mesh();
        mesh->numerate_edges();

        Vector solution;
        double rel_error =
        gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                        n_bo, n_in, bound_eigensolver,
                        inter_eigensolver, bound_regularization,
                        inter_regularization, bound_take_first_eigen,
                        inter_take_first_eigen, bound_edge_mass,
                        show_time_eig, gamma, add_boundary_edges,
                        add_interior_edges, rhs_function, fname,
                        solution, ref_mesh, an_solution, ref_numerical);

        if (refer_error > 0) { // if reference error is known for this case
          const double tol = 1e-10;
          EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
        }
        else {
          std::cout.precision(16);
          std::cout.setf(std::ios::scientific);
          std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                    << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                    << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                    << std::endl;
        }
      }
    }
  }
}
#endif

// =============================================================================
//
// Model with rectangular domain [x0,x1]x[y0,y1], where the analytical solution
// is known. Source function is computed analytically with respect to the
// solution, boundary condition is zero Dirichlet. For this test we compare the
// GMsFEM solution with not only analytical one, but with numerical as well,
// because the numerical solution obtained with standard FEM may give big error,
// and it's interesting to see what method is better. The second test is for
// aperiodic solution with constant coefficient a.
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_rect, solution_rectangles_aper_oscil)
{
  const int k = 10;
  const int m = 10;
  AnSolutionAperOscil an_solution(x0, x1, y0, y1, k, m);
  FunctionPtr coef_stif(new ConstantFunction(1.));
  RhsFunctionAperOscil rhs_function(x0, x1, y0, y1, k, m);

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  { // --------------------- compare with analytical solution ------------------
    std::string fname = "aper_oscil_an_R_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
    fname = ""; // no output

    bool ref_numerical = false;
    MeshPtr ref_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));

    for (int ni = 0; ni < n_times_interior; ++ni)
    {
      const int n_in = n_interior_bf[ni];
      std::map<int, double> reference_error; // key is the number of boundary basis functions
      if (n_in == 5) {
        reference_error[7] = 1.9046776464989346e-01;
        reference_error[9] = 1.8932894872569916e-01;
        reference_error[11] = 1.9250642012280045e-01;
        reference_error[13] = 2.0987426463701436e-01;
        reference_error[15] = 2.0957506713699503e-01;
        reference_error[-1] = 2.1079102344841499e-01;
      }
      else if (n_in == -1) {
        reference_error[7] = 1.2612677354596252e-01;
        reference_error[9] = 1.2488850074077144e-01;
        reference_error[11] = 1.2546565970676649e-01;
        reference_error[13] = 1.3678632091340207e-01;
        reference_error[15] = 1.3678557074388570e-01;
        reference_error[-1] = 1.3789669368327251e-01;
      }

      for (int nb = 0; nb < n_times_boundary; ++nb)
      {
        const int n_bo = n_boundary_bf[nb];

        double refer_error = -1;
        if (reference_error.find(n_bo) != reference_error.end())
          refer_error = reference_error.find(n_bo)->second;

        RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
        CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                      FineMeshType::Rectangular));

        cmesh->build_coarse_mesh();
        cmesh->build_fine_mesh();
        mesh->numerate_edges();

        Vector solution;
        double rel_error =
        gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                        n_bo, n_in, bound_eigensolver,
                        inter_eigensolver, bound_regularization,
                        inter_regularization, bound_take_first_eigen,
                        inter_take_first_eigen, bound_edge_mass,
                        show_time_eig, gamma, add_boundary_edges,
                        add_interior_edges, rhs_function, fname,
                        solution, ref_mesh, an_solution, ref_numerical);

        if (refer_error > 0) { // if reference error is known for this case
          const double tol = 1e-10;
          EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
        }
        else {
          std::cout.precision(16);
          std::cout.setf(std::ios::scientific);
          std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                    << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                    << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                    << std::endl;
        }
      }
    }
  }

  { // --------------------- compare with FEM solution ------------------
    std::string fname = "aper_oscil_fem_R_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
    fname = ""; // no output

    bool ref_numerical = true;
    MeshPtr ref_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));
    ref_mesh->build();

    for (int ni = 0; ni < n_times_interior; ++ni)
    {
      const int n_in = n_interior_bf[ni];
      std::map<int, double> reference_error; // key is the number of boundary basis functions
      if (n_in == 5) {
        reference_error[7] = 1.8489754100785780e-01;
        reference_error[9] = 1.8091649620791242e-01;
        reference_error[11] = 1.8013130096585039e-01;
        reference_error[13] = 1.8529835066486616e-01;
        reference_error[15] = 1.8496310536290494e-01;
        reference_error[-1] = 1.8532803387552912e-01;
      }
      else if (n_in == -1) {
        reference_error[7] = 7.1018771205864542e-02;
        reference_error[9] = 6.1572433214597481e-02;
        reference_error[11] = 4.6538315692778227e-02;
        reference_error[13] = 9.7605540357612406e-03;
        reference_error[15] = 9.0803635055591823e-03;
        reference_error[-1] = 1.3164916308032574e-03;
      }

      for (int nb = 0; nb < n_times_boundary; ++nb)
      {
        const int n_bo = n_boundary_bf[nb];

        double refer_error = -1;
        if (reference_error.find(n_bo) != reference_error.end())
          refer_error = reference_error.find(n_bo)->second;

        RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
        CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                      FineMeshType::Rectangular));

        cmesh->build_coarse_mesh();
        cmesh->build_fine_mesh();
        mesh->numerate_edges();

        Vector solution;
        double rel_error =
        gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                        n_bo, n_in, bound_eigensolver,
                        inter_eigensolver, bound_regularization,
                        inter_regularization, bound_take_first_eigen,
                        inter_take_first_eigen, bound_edge_mass,
                        show_time_eig, gamma, add_boundary_edges,
                        add_interior_edges, rhs_function, fname,
                        solution, ref_mesh, an_solution, ref_numerical);

        if (refer_error > 0) { // if reference error is known for this case
          const double tol = 1e-10;
          EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
        }
        else {
          std::cout.precision(16);
          std::cout.setf(std::ios::scientific);
          std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                    << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                    << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                    << std::endl;
        }
      }
    }
  }
}
#endif

// =============================================================================
//
// Model with rectangular domain [x0,x1]x[y0,y1], where the analytical solution
// is known. Source function is computed analytically with respect to the
// solution, boundary condition is zero Dirichlet. For this test we compare the
// GMsFEM solution with not only analytical one, but with numerical as well,
// because the numerical solution obtained with standard FEM may give big error,
// and it's interesting to see what method is better. The third test is for
// periodic solution (like for the first test) with non-constant coefficient a.
// The source function for this test coincides with the one for the second test.
//
// =============================================================================
#if 0
TEST(GMsFEMElliptic2D_rect, solution_rectangles_per_oscil_coef_a)
{
  const int k = 10;
  const int m = 10;
  AnSolutionPerOscil an_solution(x0, x1, y0, y1, k, m);
  FunctionPtr coef_stif(new CoefficientA(x0, x1, y0, y1));
  RhsFunctionAperOscil rhs_function(x0, x1, y0, y1, k, m);

  // this test faced significant difficulties for eigensolvers, therefore all
  // eigensolvers where replaced to LAPACK
  bound_eigensolver = LAPACK;
  inter_eigensolver = LAPACK;

  const int n_times_interior = sizeof(n_interior_bf)/sizeof(int);
  const int n_times_boundary = sizeof(n_boundary_bf)/sizeof(int);

  { // --------------------- compare with analytical solution ------------------
    std::string fname = "aper_oscil_an_R_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
    fname = ""; // no output

    bool ref_numerical = false;
    MeshPtr ref_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));

    for (int ni = 0; ni < n_times_interior; ++ni)
    {
      const int n_in = n_interior_bf[ni];
      std::map<int, double> reference_error; // key is the number of boundary basis functions
      if (n_in == 5) {
        reference_error[7] = 6.6994171528479318e-01;
        reference_error[9] = 6.7126148846041334e-01;
        reference_error[11] = 6.7234243143349992e-01;
        reference_error[13] = 6.7437511976616249e-01;
        reference_error[15] = 6.7977336170174474e-01;
        reference_error[-1] = 6.8088567106472175e-01;
      }
      else if (n_in == -1) {
        reference_error[7] = 1.8845424512182479e-01;
        reference_error[9] = 1.8878967161307580e-01;
        reference_error[11] = 1.8777813314935629e-01;
        reference_error[13] = 1.8828692476895256e-01;
        reference_error[15] = 1.8753628923478435e-01;
        reference_error[-1] = 1.8804446946128961e-01;
      }

      for (int nb = 0; nb < n_times_boundary; ++nb)
      {
        const int n_bo = n_boundary_bf[nb];

        double refer_error = -1;
        if (reference_error.find(n_bo) != reference_error.end())
          refer_error = reference_error.find(n_bo)->second;

        RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
        CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                      FineMeshType::Rectangular));

        cmesh->build_coarse_mesh();
        cmesh->build_fine_mesh();
        mesh->numerate_edges();

        Vector solution;
        double rel_error =
        gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                        n_bo, n_in, bound_eigensolver,
                        inter_eigensolver, bound_regularization,
                        inter_regularization, bound_take_first_eigen,
                        inter_take_first_eigen, bound_edge_mass,
                        show_time_eig, gamma, add_boundary_edges,
                        add_interior_edges, rhs_function, fname,
                        solution, ref_mesh, an_solution, ref_numerical);

        if (refer_error > 0) { // if reference error is known for this case
          const double tol = 1e-10;
          EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
        }
        else {
          std::cout.precision(16);
          std::cout.setf(std::ios::scientific);
          std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                    << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                    << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                    << std::endl;
        }
      }
    }
  }

  { // --------------------- compare with FEM solution ------------------
    std::string fname = "aper_oscil_fem_R_N_" + d2s<int>(Nx) + "_n_" + d2s<int>(nx);
    fname = ""; // no output

    bool ref_numerical = true;
    MeshPtr ref_mesh(new RectangularMesh(x0, x1, y0, y1, Nx*nx, Ny*ny));
    ref_mesh->build();

    for (int ni = 0; ni < n_times_interior; ++ni)
    {
      const int n_in = n_interior_bf[ni];
      std::map<int, double> reference_error; // key is the number of boundary basis functions
      if (n_in == 5) {
        reference_error[7] = 5.9918726620921714e-01;
        reference_error[9] = 6.0025160521015108e-01;
        reference_error[11] = 6.0082144549108352e-01;
        reference_error[13] = 6.0276343482138484e-01;
        reference_error[15] = 6.0777209728183135e-01;
        reference_error[-1] = 6.0883739687160832e-01;
      }
      else if (n_in == -1) {
        reference_error[7] = 5.1520352566965563e-02;
        reference_error[9] = 5.1092530486864005e-02;
        reference_error[11] = 4.3812609317531385e-02;
        reference_error[13] = 3.7612669343111305e-02;
        reference_error[15] = 1.4181704290101691e-02;
        reference_error[-1] = 4.2885604805672200e-03;
      }

      for (int nb = 0; nb < n_times_boundary; ++nb)
      {
        const int n_bo = n_boundary_bf[nb];

        double refer_error = -1;
        if (reference_error.find(n_bo) != reference_error.end())
          refer_error = reference_error.find(n_bo)->second;

        RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
        CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                      FineMeshType::Rectangular));

        cmesh->build_coarse_mesh();
        cmesh->build_fine_mesh();
        mesh->numerate_edges();

        Vector solution;
        double rel_error =
        gmsfem_elliptic(cmesh, coef_stif, coarse_order, fine_order,
                        n_bo, n_in, bound_eigensolver,
                        inter_eigensolver, bound_regularization,
                        inter_regularization, bound_take_first_eigen,
                        inter_take_first_eigen, bound_edge_mass,
                        show_time_eig, gamma, add_boundary_edges,
                        add_interior_edges, rhs_function, fname,
                        solution, ref_mesh, an_solution, ref_numerical);

        if (refer_error > 0) { // if reference error is known for this case
          const double tol = 1e-10;
          EXPECT_NEAR(fabs(rel_error - refer_error) / refer_error, 0., tol);
        }
        else {
          std::cout.precision(16);
          std::cout.setf(std::ios::scientific);
          std::cout << "Nx = " << Nx << " Ny = " << Ny << " nx = " << nx
                    << " ny = " << ny << " n_bo = " << n_bo << " n_in = " << n_in
                    << " rel_error = " << rel_error << " UNEXPECTED PARAMETERS"
                    << std::endl;
        }
      }
    }
  }
}
#endif

} // namespace gmsfem_elliptic_2D_coef_a_and_rhs

#endif // TEST_FEMPLUS_GMSFEM_ELLIPTIC2D_HPP
