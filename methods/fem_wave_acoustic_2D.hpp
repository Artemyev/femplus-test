#ifndef FEMPLUS_TEST_FEM_WAVE_ACOUSTIC_2D_HPP
#define FEMPLUS_TEST_FEM_WAVE_ACOUSTIC_2D_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/rectangular_mesh.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"

#include "fem_acoustic_aux.hpp"
#include "source_functions.hpp"

using namespace femplus;

//==============================================================================
// Standard conventional FEM for solution of acoustic wave equation with
// Dirichlet boundary condition in 2D recrangular domain
//==============================================================================
#if 1
TEST(FEM_acoustic_2D, rectangular_mesh)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int    nx = 100;
  const int    ny = 100;
  const int order = 1;
  const double rho = 1e+3;
  const double kappa = 5e+9;

  const double tend = 0.5;
  const int      nt = 500;
  const double   dt = tend / nt;

  // source parameters
  const double xc = 0.5 * (x1 - x0);
  const double yc = 0.5 * (y1 - y0);
  const double source_support = 20.;
  const double source_frequency = 20.;
  const Point source_center = Point(xc, yc);

  const int step_snapshot = 10; // every *-th time step is outputed
  const int step_seismo = 1;

  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  ConstantFunction coef_mass(rho);
  ConstantFunction coef_stif(kappa);
  GaussianScalarThis rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  const std::string fname_base = "fem_acou_R_nx_" + d2s<int>(nx);
  std::shared_ptr<CGDoFHandler> dof_handler;

  fem_acoustic(fe_mesh, coef_mass, coef_stif, rhs_space, rhs_time,
               nt, dt, step_snapshot, step_seismo, fname_base, dof_handler);
}
#endif
//==============================================================================
#if 0
TEST(FEM_acoustic_2D, struct_triangular_mesh)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int    nx = 100;
  const int    ny = 100;
  const int order = 1;
  const double rho = 1e+3;
  const double kappa = 5e+9;

  const double hx = (x1 - x0) / nx;
  const double xc = 0.5 * (x1 - x0);
  const double yc = 0.5 * (y1 - y0);
  const double source_support = 2.*hx;
  const Point source_center = Point(xc, yc);
  const double source_frequency = 20;

  const double tend = 0.5;
  const int      nt = 500;
  const double   dt = tend / nt;

  const int step_to_show = 10; // every *-th time step is outputed

  RectangularMesh rmesh(x0, x1, y0, y1, nx, ny);
  rmesh.build();
  MeshPtr tria_mesh = rmesh.build_triangular_mesh();

  FEMeshPtr fe_mesh(new LagrangeMesh(tria_mesh, order));

  ConstantFunction coef_mass(rho);
  ConstantFunction coef_stif(kappa);
  SourceGaussAcousticRectangle rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  const std::string fname_base = "fem_acou_ST_nx_" + d2s<int>(nx);
  fem_acoustic(fe_mesh, coef_mass, coef_stif, rhs_space, rhs_time,
               nt, dt, step_to_show, fname_base);
}
#endif
//==============================================================================
#if 0
TEST(FEM_acoustic_2D, unstruct_triangular_mesh)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int    nx = 100;
  const int order = 1;
  const double rho = 1e+3;
  const double kappa = 5e+9;

  const double hx = (x1 - x0) / nx;
  const double xc = 0.5 * (x1 - x0);
  const double yc = 0.5 * (y1 - y0);
  const double source_support = 2.*hx;
  const Point source_center = Point(xc, yc);
  const double source_frequency = 20;

  const double tend = 0.5;
  const int      nt = 500;
  const double   dt = tend / nt;

  const int step_to_show = 10; // every *-th time step is outputed

  std::vector<Point> geo_vert;
  geo_vert.push_back(Point(x0, y0));
  geo_vert.push_back(Point(x1, y0));
  geo_vert.push_back(Point(x1, y1));
  geo_vert.push_back(Point(x0, y1));
  MeshPtr mesh(new TriangularMesh(geo_vert, nx));
  mesh->build();

  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  ConstantFunction coef_mass(rho);
  ConstantFunction coef_stif(kappa);
  SourceGaussAcousticRectangle rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  const std::string fname_base = "fem_acou_UT_nx_" + d2s<int>(nx);
  fem_acoustic(fe_mesh, coef_mass, coef_stif, rhs_space, rhs_time,
               nt, dt, step_to_show, fname_base);
}
#endif
//==============================================================================
#if 0
TEST(FEM_acoustic_2D, comparison)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int    nx = 100;
  const int    ny = 100;
  const int order = 1;
  const double rho = 1e+3;
  const double kappa = 5e+9;

  const double hx = (x1 - x0) / nx;
  const double xc = 0.5 * (x1 - x0);
  const double yc = 0.5 * (y1 - y0);
  const double source_support = 2.*hx;
  const Point source_center = Point(xc, yc);
  const double source_frequency = 20;

  const double tend = 0.5;
  const int      nt = 500;
  const double   dt = tend / nt;

  const int step_to_show = nt+1; // no steps to print

  ConstantFunction coef_mass(rho);
  ConstantFunction coef_stif(kappa);
  SourceGaussAcousticRectangle rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  MeshPtr rmesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  rmesh->build();
  FEMeshPtr fe_rmesh(new LagrangeMesh(rmesh, order));
  Vector solution_R = fem_acoustic(fe_rmesh, coef_mass, coef_stif,
                                   rhs_space, rhs_time,
                                   nt, dt, step_to_show, "");

  MeshPtr stmesh = dynamic_cast<RectangularMesh*>(rmesh.get())->build_triangular_mesh();
  FEMeshPtr fe_stmesh(new LagrangeMesh(stmesh, order));
  Vector solution_ST = fem_acoustic(fe_stmesh, coef_mass, coef_stif,
                                    rhs_space, rhs_time,
                                    nt, dt, step_to_show, "");

  std::vector<Point> geo_vert;
  geo_vert.push_back(Point(x0, y0));
  geo_vert.push_back(Point(x1, y0));
  geo_vert.push_back(Point(x1, y1));
  geo_vert.push_back(Point(x0, y1));
  MeshPtr utmesh(new TriangularMesh(geo_vert, nx));
  utmesh->build();
  FEMeshPtr fe_utmesh(new LagrangeMesh(utmesh, order));
  Vector solution_UT = fem_acoustic(fe_utmesh, coef_mass, coef_stif,
                                    rhs_space, rhs_time,
                                    nt, dt, step_to_show, "");

  using namespace math;
  std::cout << "rel err: R vs ST = " << rel_error(solution_R, solution_ST) << std::endl;

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // NOTE! The comparison between R and UT is not possible since the dofs are
  // associated with different points, and even the number of them is different
  //
  //std::cout << "rel err: R vs UT = " << rel_error(solution_R, solution_UT) << std::endl;
}
#endif

//==============================================================================
//
// Comparison between two solutions obtained on different meshes
//
//==============================================================================
#if 0
TEST(FEM_acoustic_2D, salt_dome_flat_T_R)
{
  const std::string tria_file = TESTFILES_DIR + "/salt_dome_flat_F_UT.msh";
  const std::string rect_file = TESTFILES_DIR + "/salt_dome_flat_F_R.msh";
  const std::string file_layers = TESTFILES_DIR + "/salt_dome_properties.dat";

  MeshPtr tria_mesh(new TriangularMesh(tria_file));

  const double x0 = tria_mesh->min_coord().x();
  const double x1 = tria_mesh->max_coord().x();
  const double y0 = tria_mesh->min_coord().y();
  const double y1 = tria_mesh->max_coord().y();
  const int nx = 150;
  const int ny = 150;
  const int order = 1;

  const double hx = (x1 - x0) / nx;
  const double hy = (y1 - y0) / ny;
  const double xc = 500.;
  const double yc = 950.;
  const double source_frequency = 20.;
  const double source_support = 20.; // in meters
  const Point source_center = Point(xc, yc);

  const double tend = 0.3;
  const int      nt = 2500;
  const double   dt = tend / nt;
  const int step_to_show = 10; // steps to print

  CoefRhoLayers coef_mass(file_layers);
  CoefKappaLayers coef_stif(file_layers);
  SourceGaussAcousticRectangle rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  const double vmax = 4550;
  const double vmin = 2300;
  print_info_about_wavelenghts(vmax, vmin, source_frequency, hx, hy, tend, dt);

  double t0, t1;

  t0 = get_wall_time();
  MeshPtr rect_mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
//  MeshPtr rect_mesh(new TriangularMesh(rect_file));
  rect_mesh->read(rect_file);
  FEMeshPtr fe_rmesh(new LagrangeMesh(rect_mesh, order));
  std::shared_ptr<CGDoFHandler> dof_handler_R;
  Vector solution_R = fem_acoustic(fe_rmesh, coef_mass, coef_stif,
                                   rhs_space, rhs_time,
                                   nt, dt, step_to_show, "",
                                   dof_handler_R);
  t1 = get_wall_time();
  std::cout << "time of rect computation is " << t1 - t0 << std::endl;

  t0 = get_wall_time();
  FEMeshPtr fe_tmesh(new LagrangeMesh(tria_mesh, order));
  std::shared_ptr<CGDoFHandler> dof_handler_T;
  Vector solution_T;
  try {
  solution_T = fem_acoustic(fe_tmesh, coef_mass, coef_stif,
                            rhs_space, rhs_time,
                            nt, dt, step_to_show, "flat_F_T_movie",
                            dof_handler_T);
  }
  catch(...) {
    std::cout << "n bound dofs = " << dof_handler_T->n_boundary_dofs() << std::endl;
    throw;
  }
  t1 = get_wall_time();
  std::cout << "time of tria computation is " << t1 - t0 << std::endl;


//  // output properties of the model
//  CoefVpLayers velocity(file_layers);
//  Vector rho_values, vp_values;
//  dof_handler_T->init_vector_at_centers(coef_mass, rho_values);
//  dof_handler_T->init_vector_at_centers(velocity, vp_values);
//  std::vector<Vector> properties(2);
//  properties[0] = rho_values;
//  properties[1] = vp_values;
//  std::vector<std::string> prop_names(2);
//  prop_names[0] = "Density, kg/m^3";
//  prop_names[1] = "Velocity, m/s";
//  dof_handler_T->write_solution_vtu_in_cells("salt_dome_properties_color.vtu",
//                                             properties,
//                                             prop_names);


//  // comparison
//  t0 = get_wall_time();
//  const Vector sol_R_at_T_dofs = dof_handler_R->compute_solution_at_points(dof_handler_T->dofs(),
//                                                                           solution_R);
//  const Vector F_TR_diff = solution_T - sol_R_at_T_dofs;
//  dof_handler_T->write_solution("F_TR_diff.vtu",
//                                std::vector<Vector>(1, F_TR_diff),
//                                std::vector<std::string>(1, "F_RT_diff"));
//  std::cout << "rel err: T vs R = " << math::rel_error(solution_T, sol_R_at_T_dofs) << std::endl;
//  std::cout << "time of 2D comparison = " << get_wall_time() - t0;

//  // comparison in a subdomain
//  const double subx0 = 300;
//  const double subx1 = 700;
//  const double suby0 = 600;
//  const double suby1 = 1000;
//  const int subnx = 50;
//  const int subny = 50;
//  MeshPtr sub_mesh(new RectangularMesh(subx0, subx1, suby0, suby1, subnx, subny));
//  sub_mesh->build();
//  FEMeshPtr sub_fe_mesh(new LagrangeMesh(sub_mesh, order));
//  CGDoFHandler sub_dh(sub_fe_mesh);
//  sub_dh.distribute_dofs();

//  const Vector solR_sub = dof_handler_R->compute_solution_at_points(sub_dh.dofs(),
//                                                                    solution_R);
//  const Vector solT_sub = dof_handler_T->compute_solution_at_points(sub_dh.dofs(),
//                                                                    solution_T);
//  const Vector solTR_sub_diff = solT_sub - solR_sub;
//  std::cout << "rel err: T vs R in subdomain = " << math::rel_error(solT_sub, solR_sub) << std::endl;
//  std::vector<Vector> sol_sub(3);
//  sol_sub[0] = solT_sub;
//  sol_sub[1] = solR_sub;
//  sol_sub[2] = solTR_sub_diff;
//  std::vector<std::string> sub_names(3);
//  sub_names[0] = "F_T solution";
//  sub_names[1] = "F_R solution";
//  sub_names[2] = "difference";
//  sub_dh.write_solution("flat_subdomain_F_RT.vtu", sol_sub, sub_names);

//  // comparison along lines
//  t0 = get_wall_time();
//  const double x_ver   = 500.; // vertical line
//  const double y_ver_0 = 10.;
//  const double y_ver_1 = 990.;
//  const double x_hor_0 = 10.;  // horizontal line
//  const double x_hor_1 = 990.;
//  const double y_hor   = 500.;
//  const double dx = 5.;
//  const double dy = 5.;
//  std::vector<Vector> solutions_along_lines(2);
//  std::vector<std::string> names(2);

//  std::vector<Point> points;
//  Vector sol_R_along_ver = dof_handler_R->compute_solution_along_line(x_ver, x_ver, y_ver_0, y_ver_1,
//                                                                      dx, dy, solution_R, points);
//  Vector sol_T_along_ver = dof_handler_T->compute_solution_along_line(x_ver, x_ver, y_ver_0, y_ver_1,
//                                                                      dx, dy, solution_T, points);
//  solutions_along_lines[0] = sol_T_along_ver; names[0] = "pressure (F-T), Pa";
//  solutions_along_lines[1] = sol_R_along_ver; names[1] = "pressure (F-R), Pa";
//  std::string csv_fname = "compare_along_ver.csv";
//  print_csv(csv_fname, points, solutions_along_lines, names);

//  Vector sol_R_along_hor = dof_handler_R->compute_solution_along_line(x_hor_0, x_hor_1, y_hor, y_hor,
//                                                                      dx, dy, solution_R, points);
//  Vector sol_T_along_hor = dof_handler_T->compute_solution_along_line(x_hor_0, x_hor_1, y_hor, y_hor,
//                                                                      dx, dy, solution_T, points);
//  solutions_along_lines[0] = sol_T_along_hor; names[0] = "pressure (F-T), Pa";
//  solutions_along_lines[1] = sol_R_along_hor; names[1] = "pressure (F-R), Pa";
//  csv_fname = "compare_along_hor.csv";
//  print_csv(csv_fname, points, solutions_along_lines, names);
//  std::cout << "time of 1D comparison = " << get_wall_time() - t0;
}
#endif

//==============================================================================
//
// Solution with fine unstructured triangular grid in the domain with topography
//
//==============================================================================
#if 0
TEST(FEM_acoustic_2D, salt_dome_topo_T)
{
  const std::string tria_file = "salt_dome_wide_F_UT.msh";
  const std::string file_layers = TESTFILES_DIR + "salt_dome_properties.dat";

  MeshPtr tria_mesh(new TriangularMesh(tria_file));

  const double xc = 1000.;
  const double yc = 980.;
  const double source_frequency = 20.;
  const double source_support = 10.;
  const Point source_center = Point(xc, yc);

  const double tend = 0.6;
  const int      nt = 2500;
  const double   dt = tend / nt;
  const int step_snapshot = nt+1; //25; // steps to print
  const int step_seismo   = 5;
  const int order = 1;

  const double rec_beg_x = 500;
  const double rec_end_x = 1500;
  const double rec_y     = 900;
  const int n_rec = 51;

  const std::string snapshot_file = "wide_F_T_movie";
  const std::string seismo_file   = "wide_F_T_seis_new";

  CoefRhoLayers coef_mass(file_layers);
  CoefKappaLayers coef_stif(file_layers);
  GaussianScalarThis rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  std::vector<Point> receivers;
  std::vector<Vector> values_receivers; // 1 dim - time, 2 dim - receiver points
//  std::vector<Vector> right_values; // 1 dim - receiver points, 2 dim - time

  points_along_line(Point(rec_beg_x, rec_y), Point(rec_end_x, rec_y), n_rec, receivers);

  double t0 = get_wall_time();
  FEMeshPtr fe_tmesh(new LagrangeMesh(tria_mesh, order));
  std::shared_ptr<CGDoFHandler> dof_handler_T;
  Vector solution_T = fem_acoustic(fe_tmesh, coef_mass, coef_stif,
                                   rhs_space, rhs_time,
                                   nt, dt, step_snapshot, step_seismo,
                                   snapshot_file,
                                   dof_handler_T,
                                   NULL,
                                   NULL,
                                   &seismo_file,
                                   &receivers,
                                   &values_receivers);

  double t1 = get_wall_time();
  std::cout << "time of tria computation is " << t1 - t0 << std::endl;
}
#endif

#endif // FEMPLUS_TEST_FEM_WAVE_ACOUSTIC_2D_HPP
