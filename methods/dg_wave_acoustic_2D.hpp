#ifndef FEMPLUS_TEST_DG_WAVE_ACOUSTIC_2D_HPP
#define FEMPLUS_TEST_DG_WAVE_ACOUSTIC_2D_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/rectangular_mesh.hpp"
#include "femplus/finite_element_mesh.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/dg_finite_element_mesh.hpp"

#include "source_functions.hpp"
#include "dg_acoustic_aux.hpp"

using namespace femplus;

//==============================================================================
//
// DG for acoustic wave equation on rectangular grid
//
//==============================================================================
TEST(DG_acoustic_2D, rectangular_mesh)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int    nx = 100;
  const int    ny = 100;
  const int order = 1;
  const double rho = 1e+3;
  const double kappa = 5e+9;
  const double gamma = 1.;

  const double tend = 0.5;
  const int      nt = 500;
  const double   dt = tend / nt;

  // source parameters
  const double xc = 0.5 * (x1 - x0);
  const double yc = 0.5 * (y1 - y0);
  const double source_support = 20.;
  const double source_frequency = 20.;
  const Point source_center = Point(xc, yc);

  const int step_snaphot = 10; // every *-th time step is outputed
  const int step_seismo  = 1;

  MeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, nx, ny));
  mesh->build();

  FEMeshPtr fe_mesh(new DGFiniteElementMesh(mesh, order));

  ConstantFunction coef_mass(rho);
  ConstantFunction coef_stif(kappa);
  GaussianScalarThis rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  const std::string fname_base = "dg_acou_R_nx_gam1_" + d2s<int>(nx);
  std::shared_ptr<DGDoFHandler> dof_handler;

  dg_acoustic(fe_mesh, gamma, coef_mass, coef_stif, rhs_space, rhs_time,
              nt, dt, step_snaphot, step_seismo, fname_base, dof_handler);
}

#endif // FEMPLUS_TEST_DG_WAVE_ACOUSTIC_2D_HPP
