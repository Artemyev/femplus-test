#ifndef FEMPLUS_TEST_TEST_SOURCE_APPROXIMATION_HPP
#define FEMPLUS_TEST_TEST_SOURCE_APPROXIMATION_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "fem_acoustic_aux.hpp"
#include "source_functions.hpp"
#include "acoustic2D_aux.hpp"

#include "femplus/auxiliary_functions.hpp"
#include "femplus/polygon.hpp"
#include "femplus/gaussian_scalar.hpp"

using namespace femplus;

//==============================================================================
TEST(TriRect, source_approximation)
{
  const double x0 = 0;
  const double x1 = 1000;
  const double y0 = 0;
  const double y1 = 1000;
  const int    nx = 200;
  const int    ny = 200;
  const int order = 1;
  const bool build_new_tria_mesh = true;
  const double rho = 1e+3;
  const double vp  = 3e+3;
  const double kappa = rho * vp * vp;

  const double xc = 0.5 * (x1 - x0);
  const double yc = 0.5 * (y1 - y0);
  const Point source_center = Point(xc, yc);
  const double source_support = 1;
  const double source_frequency = 30.;

  const double tend = 0.15;
  const int      nt = 1000;
  const double   dt = tend / nt;

  const int step_to_show = 1; // every *-th time step is outputed

  const double angle_dis = 5.; // in degrees
  const double seis_radius = 250.; // radius of circular line around a source
                                   // center, where receivers are located

  ConstantFunction coef_mass(rho);
  ConstantFunction coef_stif(kappa);
  GaussianScalarThis rhs_space_this(source_support, source_center);
  GaussianScalar rhs_space(source_support, source_center);
  Ricker rhs_time(source_frequency);

  std::string filemesh, fname, seismofile, fname_base;
  std::vector<Vector> sols;
  std::vector<std::string> nams;
  double x_of_max, x_of_min;
  std::vector<Point> circ_max, circ_min;
  std::vector<Point> points, receivers;
  std::vector<Vector> values_receivers; // 1 dim - time, 2 dim - receiver points
  std::vector<Vector> right_values; // 1 dim - receiver points, 2 dim - time
  std::vector<Point> time;

  circular_points(source_center, seis_radius, angle_dis, receivers);
  write_circle(TESTOUT_DIR + "testmesh.geo", source_center, receivers);

  // --------------------------------- R1 --------------------------------------
  fname_base = "fem_R1_ss_" + d2s<int>(source_support) + "_nx_" + d2s<int>(nx);

  MeshPtr mesh_R1(new RectangularMesh(x0, x1, y0, y1, nx-1, ny-1));
  mesh_R1->build();
  filemesh = TESTOUT_DIR + "mesh_R1.msh"; mesh_R1->write(filemesh); write_geo_support(filemesh, source_center, source_support);
  FEMeshPtr fe_mesh_R1(new LagrangeMesh(mesh_R1, order));
  fname = ""; fname_base; //fname = "";
  seismofile = TESTOUT_DIR + "seis_" + fname_base;
  std::shared_ptr<CGDoFHandler> dof_handler_R1;
  const Vector sol_R1 =
  fem_acoustic(fe_mesh_R1, coef_mass, coef_stif, rhs_space, rhs_time,
               nt, dt, step_to_show, fname, dof_handler_R1, NULL, NULL,
               &seismofile, &receivers, &values_receivers);

  right_values.resize(receivers.size());
  nams.resize(receivers.size());
  for (int i = 0; i < (int)receivers.size(); ++i) {
    nams[i] = "receiver " + d2s<int>(i*angle_dis);
    std::vector<double> val(values_receivers.size()); // values for ONE receiver over time
    for (int j = 0; j < (int)values_receivers.size(); ++j)
      val[j] = values_receivers[j](i);
    right_values[i] = Vector(val);
  }
  time.resize(values_receivers.size());
  for (int i = 0; i < (int)values_receivers.size(); ++i)
    time[i] = Point((i+1)*dt, 0, 0);
  fname = TESTOUT_DIR + fname_base + "_values_receivers.csv";
  print_csv(fname, time, right_values, nams);

  const Point end_point = dof_handler_R1->finite_element_mesh()->base_mesh()->max_coord();
  sup_wavefield(*dof_handler_R1.get(), source_center, end_point, sol_R1, x_of_min, x_of_max);
  circular_points(source_center, x_of_max - source_center.x(), angle_dis, circ_max);
  circular_points(source_center, x_of_min - source_center.x(), angle_dis, circ_min);
  const Vector sol_R1_max_iso = dof_handler_R1->compute_solution_at_points(circ_max, sol_R1);
  const Vector sol_R1_min_iso = dof_handler_R1->compute_solution_at_points(circ_min, sol_R1);
  fname = TESTOUT_DIR + fname_base + "_iso.csv";
  sols.clear();
  sols.push_back(sol_R1_max_iso);
  sols.push_back(sol_R1_min_iso);
  nams.clear();
  nams.push_back("sol_R1_max_iso");
  nams.push_back("sol_R1_min_iso");
  print_csv(fname, circ_max, sols, nams);

//  Vector rhs_values;
//  dof_handler_R1->init_vector(rhs_space, rhs_values);
//  fname = TESTOUT_DIR + fname_base + "_rhs_values.vtu";
//  dof_handler_R1->write_solution(fname, std::vector<Vector>(1, rhs_values), std::vector<std::string>(1, "rhs values"));

//  std::vector<double> sol_R1_max_iso_ = sol_R1_max_iso.std_vector();
//  std::vector<double> sol_R1_min_iso_ = sol_R1_min_iso.std_vector();
//  double sol_R1_max_iso_max = *std::max_element(sol_R1_max_iso_.begin(), sol_R1_max_iso_.end());
//  double sol_R1_max_iso_min = *std::min_element(sol_R1_max_iso_.begin(), sol_R1_max_iso_.end());
//  double sol_R1_min_iso_max = *std::max_element(sol_R1_min_iso_.begin(), sol_R1_min_iso_.end());
//  double sol_R1_min_iso_min = *std::min_element(sol_R1_min_iso_.begin(), sol_R1_min_iso_.end());

//  std::cout << "sol_R1_max_iso_max = " << sol_R1_max_iso_max << std::endl;
//  std::cout << "sol_R1_max_iso_min = " << sol_R1_max_iso_min << std::endl;
//  std::cout << "sol_R1_min_iso_max = " << sol_R1_min_iso_max << std::endl;
//  std::cout << "sol_R1_min_iso_min = " << sol_R1_min_iso_min << std::endl;

//  const Vector sol_R1_line = dof_handler_R1->compute_solution_along_line(0, 1000, 500, 500, 5, 5, sol_R1, points);
//  std::cout << "x of min = " << x_of_min << " x of max = " << x_of_max << std::endl;
//  fname = "sol_R1_line_ss_" + d2s<int>(source_support) + ".csv";
//  print_csv(fname, points, std::vector<Vector>(1, sol_R1_line), std::vector<std::string>(1, "solution R1 along line"));


  // --------------------------------- R2 --------------------------------------
//  fname_base = "fem_R2_ss_" + d2s<int>(source_support) + "_nx_" + d2s<int>(nx);

//  MeshPtr mesh_R2(new RectangularMesh(x0, x1, y0, y1, nx-1, ny-1));
//  mesh_R2->build();
//  filemesh = TESTOUT_DIR + "mesh_R2.msh"; mesh_R2->write(filemesh); write_geo(filemesh, source_center, source_support);
//  FEMeshPtr fe_mesh_R2(new LagrangeMesh(mesh_R2, order));
//  fname = "";
//  seismofile = TESTOUT_DIR + "seis_" + fname_base;
//  std::shared_ptr<CGDoFHandler> dof_handler_R2;
//  const Vector sol_R2 =
//  fem_acoustic(fe_mesh_R2, coef_mass, coef_stif, rhs_space_this, rhs_time,
//               nt, dt, step_to_show, fname, dof_handler_R2, NULL, NULL,
//               &seismofile, &receivers, &values_receivers);

//  right_values.resize(receivers.size());
//  nams.resize(receivers.size());
//  for (int i = 0; i < (int)receivers.size(); ++i) {
//    nams[i] = "receiver " + d2s<int>(i*angle_dis);
//    std::vector<double> val(values_receivers.size()); // values for ONE receiver over time
//    for (int j = 0; j < (int)values_receivers.size(); ++j)
//      val[j] = values_receivers[j](i);
//    right_values[i] = Vector(val);
//  }
//  time.resize(values_receivers.size());
//  for (int i = 0; i < (int)values_receivers.size(); ++i)
//    time[i] = Point((i+1)*dt, 0, 0);
//  fname = TESTOUT_DIR + fname_base + "_values_receivers.csv";
//  print_csv(fname, time, right_values, nams);

//  sup_wavefield(*dof_handler_R2.get(), source_center, sol_R2, x_of_min, x_of_max);
//  circular_points(source_center, x_of_max - source_center.x(), angle_dis, circ_max);
//  circular_points(source_center, x_of_min - source_center.x(), angle_dis, circ_min);
//  const Vector sol_R2_max_iso = dof_handler_R2->compute_solution_at_points(circ_max, sol_R2);
//  const Vector sol_R2_min_iso = dof_handler_R2->compute_solution_at_points(circ_min, sol_R2);
//  fname = TESTOUT_DIR + fname_base + "_iso.csv";
//  sols.clear();
//  sols.push_back(sol_R2_max_iso);
//  sols.push_back(sol_R2_min_iso);
//  nams.clear();
//  nams.push_back("sol_R2_max_iso");
//  nams.push_back("sol_R2_min_iso");
//  print_csv(fname, circ_max, sols, nams);

//  std::vector<double> sol_R2_max_iso_ = sol_R2_max_iso.std_vector();
//  std::vector<double> sol_R2_min_iso_ = sol_R2_min_iso.std_vector();
//  double sol_R2_max_iso_max = *std::max_element(sol_R2_max_iso_.begin(), sol_R2_max_iso_.end());
//  double sol_R2_max_iso_min = *std::min_element(sol_R2_max_iso_.begin(), sol_R2_max_iso_.end());
//  double sol_R2_min_iso_max = *std::max_element(sol_R2_min_iso_.begin(), sol_R2_min_iso_.end());
//  double sol_R2_min_iso_min = *std::min_element(sol_R2_min_iso_.begin(), sol_R2_min_iso_.end());

//  std::cout << "sol_R2_max_iso_max = " << sol_R2_max_iso_max << std::endl;
//  std::cout << "sol_R2_max_iso_min = " << sol_R2_max_iso_min << std::endl;
//  std::cout << "sol_R2_min_iso_max = " << sol_R2_min_iso_max << std::endl;
//  std::cout << "sol_R2_min_iso_min = " << sol_R2_min_iso_min << std::endl;

//  const Vector sol_R2_line = dof_handler_R2->compute_solution_along_line(0, 1000, 500, 500, 5, 5, sol_R2, points);
//  std::cout << "x of min = " << x_of_min << " x of max = " << x_of_max << std::endl;
//  fname = "sol_R2_line_ss_" + d2s<int>(source_support) + ".csv";
//  print_csv(fname, points, std::vector<Vector>(1, sol_R2_line), std::vector<std::string>(1, "solution R2 along line"));


//  // ----------------------- COMPARISON between R1 and R2-----------------------
//  const Vector sol_R21 = dof_handler_R2->compute_solution_at_points(dof_handler_R1->dofs(), sol_R2);
//  fname = TESTOUT_DIR + "sols_RR.vtu";
//  sols.clear();
//  sols.push_back(sol_R1);
//  sols.push_back(sol_R21);
//  sols.push_back(sol_R1-sol_R21);
//  nams.clear();
//  nams.push_back("1_solution_R1");
//  nams.push_back("2_solution_R2");
//  nams.push_back("3_difference");
//  dof_handler_R1->write_solution(fname, sols, nams);
//  std::cout << "rel error RR = " << math::rel_error(sol_R1, sol_R21) << std::endl;

//  std::vector<Vector> r1r2;
//  r1r2.push_back(


//  // --------------------------------- T ---------------------------------------
//  filemesh = TESTOUT_DIR + "mesh_T.msh";
//  MeshPtr mesh_T;
//  if (build_new_tria_mesh) {
//    std::vector<Point> geo_vertices;
//    geo_vertices.push_back(Point(x0, y0));
//    geo_vertices.push_back(Point(x1, y0));
//    geo_vertices.push_back(Point(x1, y1));
//    geo_vertices.push_back(Point(x0, y1));
//    mesh_T = MeshPtr(new TriangularMesh(geo_vertices, nx));
//    mesh_T->build();
//  }
//  else {
//   mesh_T = MeshPtr(new TriangularMesh(filemesh));
//  }
//  mesh_T->write(filemesh); write_geo(filemesh, source_center, source_support);
//  FEMeshPtr fe_mesh_T(new LagrangeMesh(mesh_T, order));
//  fname = ""; //"fem_acou_T_ss_" + d2s<int>(source_support) + "_nx_" + d2s<int>(nx);
//  seismofile = TESTOUT_DIR + "mesh_T_ss_" + d2s<int>(source_support) + "_nx_" + d2s<int>(nx);
//  std::shared_ptr<CGDoFHandler> dof_handler_T;
//  const Vector solution_T =
//  fem_acoustic(fe_mesh_T, coef_mass, coef_stif, rhs_space, rhs_time,
//               nt, dt, step_to_show, fname, dof_handler_T, NULL, NULL,
//               &seismofile, &receivers, &values_receivers);

//  right_values.resize(receivers.size());
//  nams.resize(receivers.size());
//  for (int i = 0; i < (int)receivers.size(); ++i) {
//    nams[i] = "receiver " + d2s<int>(i*angle_dis);
//    std::vector<double> val(values_receivers.size()); // values for ONE receiver over time
//    for (int j = 0; j < (int)values_receivers.size(); ++j)
//      val[j] = values_receivers[j](i);
//    right_values[i] = Vector(val);
//  }
//  time.resize(values_receivers.size());
//  for (int i = 0; i < (int)values_receivers.size(); ++i)
//    time[i] = Point((i+1)*dt, 0, 0);
//  fname = TESTOUT_DIR + "mesh_T_values_receivers.csv";
//  print_csv(fname, time, right_values, nams);

//  sup_wavefield(*dof_handler_T.get(), source_center, solution_T, x_of_min, x_of_max);
//  circular_points(source_center, x_of_max - source_center.x(), angle_dis, circ_max);
//  circular_points(source_center, x_of_min - source_center.x(), angle_dis, circ_min);
//  const Vector sol_T_max_iso = dof_handler_T->compute_solution_at_points(circ_max, solution_T);
//  const Vector sol_T_min_iso = dof_handler_T->compute_solution_at_points(circ_min, solution_T);
//  fname = "sol_T_iso_ss_" + d2s<int>(source_support) + ".csv";
//  sols.clear();
//  sols.push_back(sol_T_max_iso);
//  sols.push_back(sol_T_min_iso);
//  nams.clear();
//  nams.push_back("sol_T_max_iso");
//  nams.push_back("sol_T_min_iso");
//  print_csv(fname, circ_max, sols, nams);

//  std::vector<double> sol_T_max_iso_ = sol_T_max_iso.std_vector();
//  std::vector<double> sol_T_min_iso_ = sol_T_min_iso.std_vector();
//  double sol_T_max_iso_max = *std::max_element(sol_T_max_iso_.begin(), sol_T_max_iso_.end());
//  double sol_T_max_iso_min = *std::min_element(sol_T_max_iso_.begin(), sol_T_max_iso_.end());
//  double sol_T_min_iso_max = *std::max_element(sol_T_min_iso_.begin(), sol_T_min_iso_.end());
//  double sol_T_min_iso_min = *std::min_element(sol_T_min_iso_.begin(), sol_T_min_iso_.end());

//  std::cout << "sol_T_max_iso_max = " << sol_T_max_iso_max << std::endl;
//  std::cout << "sol_T_max_iso_min = " << sol_T_max_iso_min << std::endl;
//  std::cout << "sol_T_min_iso_max = " << sol_T_min_iso_max << std::endl;
//  std::cout << "sol_T_min_iso_min = " << sol_T_min_iso_min << std::endl;

//  const Vector sol_T_line = dof_handler_T->compute_solution_along_line(0, 1000, 500, 500, 5, 5, solution_T, points);
//  std::cout << "x of min = " << x_of_min << " x of max = " << x_of_max << std::endl;
//  fname = "sol_T_line_ss_" + d2s<int>(source_support) + ".csv";
//  print_csv(fname, points, std::vector<Vector>(1, sol_T_line), std::vector<std::string>(1, "solution T along line"));


//  // --------------------- COMPARISON between R1 and T -------------------------
//  const Vector solution_R = dof_handler_R1->compute_solution_at_points(dof_handler_T->dofs(), sol_R1);
//  fname = "sols_RT.vtu";
//  sols.clear();
//  sols.push_back(solution_T);
//  sols.push_back(solution_R);
//  sols.push_back(solution_T-solution_R);
//  nams.clear();
//  nams.push_back("1_solution_T");
//  nams.push_back("2_solution_R");
//  nams.push_back("3_difference");
//  dof_handler_T->write_solution(fname, sols, nams);
//  std::cout << "rel error RT = " << math::rel_error(solution_T, solution_R) << std::endl;
}

#endif // FEMPLUS_TEST_TEST_SOURCE_APPROXIMATION_HPP
