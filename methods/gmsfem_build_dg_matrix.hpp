#ifndef TEST_FEMPLUS_GMSFEM_BUILD_DG_MATRIX_HPP
#define TEST_FEMPLUS_GMSFEM_BUILD_DG_MATRIX_HPP

#include "config.hpp"
#include "analytic_functions.hpp"
#include "femplus/coarse_mesh.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/dg_finite_element_mesh.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/vector.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/solver.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/gms_dof_handler.hpp"
#include "femplus/mixed_poly_rect_mesh.hpp"
#include "femplus/coarse_mixed_poly_rect_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/coarse_finite_element.hpp"
#include "femplus/dense_pattern.hpp"
#include <gtest/gtest.h>
#if defined(USE_BOOST)
  #include "boost/format.hpp"
#endif

using namespace femplus;

//#define SHOW_MAT



// =============================================================================
//TEST(GMsFEMBuildDGMatrix, rectangles)
//{
//  const double x0 = 0;
//  const double x1 = 1;
//  const double y0 = 0;
//  const double y1 = 1;
//  const int Nx = 2;
//  const int Ny = 2;
//  const int nx = 3;
//  const int ny = 3;

//  RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
//  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
//                                                FineMeshType::Rectangular));

//  cmesh->build_coarse_mesh();
//  cmesh->build_fine_mesh();

//  const int coarse_order = 1;
//  const int fine_order = 1;
//  CoarseFEMeshPtr fe_mesh(new CoarseFiniteElementMesh(cmesh,
//                                                      coarse_order,
//                                                      fine_order));

//  const int n_boundary_bf = 2;
//  const int n_interior_bf = 2;
//  ConstantFunction coef_laplace(1);
//  ConstantFunction coef_mass(1);

//  fe_mesh->compute_basis(coef_mass,
//                         coef_laplace,
//                         n_boundary_bf,
//                         n_interior_bf,
//                         EPSLAPACK,
//                         EPSLAPACK);

//  fe_mesh->write_interior_basis("RECT_COARSE_RECT_FINE_INTERIOR.vtu");
//  fe_mesh->write_boundary_basis("RECT_COARSE_RECT_FINE_BOUNDARY.vtu");

//  const int gamma = 1;
//  GMsDoFHandler dof_handler(fe_mesh, gamma);
//  dof_handler.distribute_dofs();

//  mesh->numerate_edges();

//  std::vector<std::set<int> > connections;
//  dof_handler.dg_dofs_connections(connections);
//  PatternPtr dg_pattern(new CSRPattern(connections));

//  SparsePetscMatrix system_mat(dg_pattern); // global DG matrix

//  // ------------- standard contribution of laplace matrix first ---------------
//  // for each coarse element
//  for (int el = 0; el < fe_mesh->n_elements(); ++el)
//  {
//    // current coarse finite element
//    const CoarseFiniteElement *cf_element = fe_mesh->element(el);

//    // local laplace matrix for a coarse scale
//    PatternPtr dense_pattern(new DensePattern(cf_element->n_dofs(),
//                                              cf_element->n_dofs()));
//    SparsePetscMatrix local_coarse_lapl_matrix(dense_pattern);

//    // projecting the local mass matrix from the fine scale to the coarse one,
//    // multiplying by R and R^T
//    local_coarse_lapl_matrix.RARt(cf_element->laplace_matrix(),
//                                  cf_element->R());

//#if defined(SHOW_MAT)
//  std::cout << "\n\n laplace matrix for " << el << " elm \n";
//  cf_element->laplace_matrix()->matlab_view(std::cout);
//  std::cout << "\n\n R for " << el << " elm \n";
//  cf_element->R()->matlab_view(std::cout);
//  std::cout << "\n";
//#endif

//    // assemble the global DG matrix for a coarse scale from the local ones
//    for (int di = 0; di < cf_element->n_dofs(); ++di)
//    {
//      const int dof_i = cf_element->dof_number(di);
//      for (int dj = 0; dj < cf_element->n_dofs(); ++dj)
//      {
//        const int dof_j = cf_element->dof_number(dj);
//        const double value = local_coarse_lapl_matrix(di, dj);
//        system_mat.add_value(dof_i,
//                             dof_j,
//                             value);
//      }
//    }
//  }
//  system_mat.final_assembly();
//#if defined(SHOW_MAT)
//  std::cout << "\n\n DG MAT \n";
//  system_mat.full_view(std::cout);
//  std::cout << "\n";
//#endif

//  ConstantFunction coefficient(1);
//  const double _gamma = 1.;

//  // ------------- contribution of integrals over mesh edges ---------------
//  const Mesh *base_mesh = fe_mesh->coarse_mesh()->coarse_mesh();
//  // the edges of the base mesh should be initialized
//  expect(base_mesh->n_edges() > 0,
//         "The edges of the base mesh are not initialized");

//  // a vector of sets keeping an info: to which elements a mesh edge belongs
//  std::vector<std::set<int> > edge_to_cell(base_mesh->n_edges());

//  // generate this map between mesh edges indices and numbers of mesh elements:
//  // pass though all mesh elements
//  for (int el = 0; el < base_mesh->n_elements(); ++el)
//  {
//    // the current element of a base mesh
//    const MeshElement *mesh_element = base_mesh->element(el);
//    // pass though all edges (whose numbers are in a structure of a mesh element
//    // object)
//    for (int e = 0; e < mesh_element->n_edges(); ++e)
//    {
//      // get a global index of an edge belonging to the mesh element
//      const int edge_number = mesh_element->edge_number(e);
//      // and insert the number of the mesh element to a corresponding place of
//      // a resulting map
//      edge_to_cell[edge_number].insert(el);
//    }
//  }

//  // now we assemble the global matrix:
//  // pass through all edges of the base mesh
//  for (int e = 0; e < base_mesh->n_edges(); ++e)
//  {
//    // if an edge belongs to 1 element, it's a boundary edge
//    if (edge_to_cell[e].size() == 1)
//    {
//      std::set<int>::const_iterator cell_number = edge_to_cell[e].begin();
//      const CoarseFiniteElement *cf_element = fe_mesh->element(*cell_number);

//      // a center (with coordinates) of the mesh element corresponding to
//      // the coarse finite element
//      const Point coarse_element_center =
//                base_mesh->element(*cell_number)->center(base_mesh->vertices());

//      // local matrix of integrals over a boundary coarse edge
//      PatternPtr dense_pattern(new DensePattern(cf_element->n_dofs(),
//                                                cf_element->n_dofs()));
//      PetscMatrixPtr edge_matrix(new SparsePetscMatrix(dense_pattern));

//      PetscMatrixPtr glob_fine_scale_bound_edges_DG_mat;

//      // compute the matrix
//      cf_element->boundary_edge_matrix(*base_mesh->edge(e),
//                                       base_mesh->vertices(),
//                                       coefficient,
//                                       _gamma,
//                                       coarse_element_center,
//                                       edge_matrix,
//                                       glob_fine_scale_bound_edges_DG_mat);
//#if defined(SHOW_MAT)
//      std::cout << "\n\nboundary edge " << e << ": "
//                << base_mesh->edge(e)->vertex_number(0) << " -> "
//                << base_mesh->edge(e)->vertex_number(1) << ", el = "
//                << *cell_number << "\n";
//      std::cout << "edge coarse matrix\n";
//      edge_matrix->full_view(std::cout);
//      std::cout << "edge fine matrix\n";
//      glob_fine_scale_bound_edges_DG_mat->full_view(std::cout);
//#endif

//      // assemble the global DG matrix for a coarse scale from the local ones
//      for (int di = 0; di < cf_element->n_dofs(); ++di)
//      {
//        const int dof_i = cf_element->dof_number(di);
//        for (int dj = 0; dj < cf_element->n_dofs(); ++dj)
//        {
//          const int dof_j = cf_element->dof_number(dj);
//          const double value = (*edge_matrix)(di, dj);
//          system_mat.add_value(dof_i,
//                               dof_j,
//                               value);
//        }
//      }

//#if defined(SHOW_MAT)
//      system_mat.final_assembly();
//      std::cout << "DG MAT after bound edge\n";
//      system_mat.full_view(std::cout);
//      std::cout << "\n";
//#endif

//    }
//    // if an edge belongs to 2 elements, it's an interior edge
//    else if (edge_to_cell[e].size() == 2)
//    {
//      std::set<int>::const_iterator cell_number = edge_to_cell[e].begin();
//      const CoarseFiniteElement *cf_element_0 = fe_mesh->element(*cell_number);

//      ++cell_number;
//      const CoarseFiniteElement *cf_element_1 = fe_mesh->element(*cell_number);

//      // local matrices of integrals over an interior coarse edge
//      const int n_dofs_coarse_plus = cf_element_0->n_dofs();
//      const int n_dofs_coarse_minu = cf_element_1->n_dofs();
//      PatternPtr coarse_plus_plus(new DensePattern(n_dofs_coarse_plus, n_dofs_coarse_plus));
//      PatternPtr coarse_plus_minu(new DensePattern(n_dofs_coarse_plus, n_dofs_coarse_minu));
//      PatternPtr coarse_minu_plus(new DensePattern(n_dofs_coarse_minu, n_dofs_coarse_plus));
//      PatternPtr coarse_minu_minu(new DensePattern(n_dofs_coarse_minu, n_dofs_coarse_minu));
//      PetscMatrixPtr loc_mat_plus_plus(new SparsePetscMatrix(coarse_plus_plus));
//      PetscMatrixPtr loc_mat_plus_minu(new SparsePetscMatrix(coarse_plus_minu));
//      PetscMatrixPtr loc_mat_minu_plus(new SparsePetscMatrix(coarse_minu_plus));
//      PetscMatrixPtr loc_mat_minu_minu(new SparsePetscMatrix(coarse_minu_minu));

//      // compute the matrix
//      cf_element_0->interior_edge_matrix(*base_mesh->edge(e),
//                                         base_mesh->vertices(),
//                                         coefficient,
//                                         _gamma,
//                                         *cf_element_1,
//                                         loc_mat_plus_plus,
//                                         loc_mat_plus_minu,
//                                         loc_mat_minu_plus,
//                                         loc_mat_minu_minu);

//#if defined(SHOW_MAT)
//      std::cout << "\n\ninterior edge " << e << ": "
//                << base_mesh->edge(e)->vertex_number(0) << " -> "
//                << base_mesh->edge(e)->vertex_number(1) << " between el = "
//                << *(edge_to_cell[e].begin()) << " and " << *cell_number << "\n";
//      std::cout << "loc_mat_plus_plus\n";
//      loc_mat_plus_plus->full_view(std::cout);
//      std::cout << "loc_mat_plus_minu\n";
//      loc_mat_plus_minu->full_view(std::cout);
//      std::cout << "loc_mat_minu_plus\n";
//      loc_mat_minu_plus->full_view(std::cout);
//      std::cout << "loc_mat_minu_minu\n";
//      loc_mat_minu_minu->full_view(std::cout);
//#endif

//      // assemble the global DG matrix for a coarse scale from the local ones

//      // interaction between boundary functions of edges of the cf_element_0
//      for (int di = 0; di < cf_element_0->n_dofs(); ++di)
//      {
//        const int dof_i = cf_element_0->dof_number(di);
//        for (int dj = 0; dj < cf_element_0->n_dofs(); ++dj)
//        {
//          const int dof_j = cf_element_0->dof_number(dj);
//          const double value = (*loc_mat_plus_plus)(di, dj);
//          system_mat.add_value(dof_i,
//                               dof_j,
//                               value);
//        }
//      }


//      // interaction between boundary functions of edges of the cf_element_0 and
//      // cf_element_1
//      for (int di = 0; di < cf_element_0->n_dofs(); ++di)
//      {
//        const int dof_i = cf_element_0->dof_number(di);
//        for (int dj = 0; dj < cf_element_1->n_dofs(); ++dj)
//        {
//          const int dof_j = cf_element_1->dof_number(dj);
//          const double value = (*loc_mat_plus_minu)(di, dj);
//          system_mat.add_value(dof_i,
//                               dof_j,
//                               value);
//        }
//      }

//      // interaction between boundary functions of edges of the cf_element_1 and
//      // cf_element_0
//      for (int di = 0; di < cf_element_1->n_dofs(); ++di)
//      {
//        const int dof_i = cf_element_1->dof_number(di);
//        for (int dj = 0; dj < cf_element_0->n_dofs(); ++dj)
//        {
//          const int dof_j = cf_element_0->dof_number(dj);
//          const double value = (*loc_mat_minu_plus)(di, dj);
//          system_mat.add_value(dof_i,
//                               dof_j,
//                               value);
//        }
//      }

//      // interaction between boundary functions of edges of the cf_element_1
//      for (int di = 0; di < cf_element_1->n_dofs(); ++di)
//      {
//        const int dof_i = cf_element_1->dof_number(di);
//        for (int dj = 0; dj < cf_element_1->n_dofs(); ++dj)
//        {
//          const int dof_j = cf_element_1->dof_number(dj);
//          const double value = (*loc_mat_minu_minu)(di, dj);
//          system_mat.add_value(dof_i,
//                               dof_j,
//                               value);
//        }
//      }
//#if defined(SHOW_MAT)
//      system_mat.final_assembly();
//      std::cout << "DG MAT after inter edge\n";
//      system_mat.full_view(std::cout);
//      std::cout << "\n";
//#endif

//    }
//    // otherwise there is an error (because in 2D an edge can belong to either
//    // 1 or 2 elements only)
//    else
//    {
//      require(false,
//              "The " + d2s<int>(e) + "-th edge belongs to " +
//              d2s<unsigned>(edge_to_cell[e].size()) +
//              " number of mesh elements (should be 1 or 2 only)");
//    }
//  } // loop over edges of the coarse mesh

//  //system_mat.final_assembly();
//#if defined(SHOW_MAT)
//  std::cout << "\n\n DG MAT after all edges \n";
//  system_mat.full_view(std::cout);
//  std::cout << "\n";
//#endif
//}





// =============================================================================
//
//
//
//
//
// =============================================================================
TEST(GMsFEMBuildDGMatrix, triangles)
{
#if defined(SHOW_MAT)
  std::cout << "\n\n\n";
  std::cout << "=========================================================\n";
  std::cout << "                     TRIANGLES                           \n";
  std::cout << "=========================================================\n";
#endif

  const double x0 = 0;
  const double x1 = 1;
  const double y0 = 0;
  const double y1 = 1;
  const int Nx = 2;
  const int Ny = 2;
  const int nx = 3;
  const int ny = 3;

  RectMeshPtr mesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  CoarseMeshPtr cmesh(new CoarseRectangularMesh(mesh, nx, ny,
                                                FineMeshType::Triangular));

  cmesh->build_coarse_mesh();
  cmesh->read_fine_mesh(TESTFILES_DIR + "/tria_master_order.msh");

  const int coarse_order = 1;
  const int fine_order = 1;
  CoarseFEMeshPtr fe_mesh(new CoarseFiniteElementMesh(cmesh,
                                                      coarse_order,
                                                      fine_order));

  const int n_boundary_bf = 2;
  const int n_interior_bf = 2;
  FunctionPtr coef_mass(new ConstantFunction(1));
  FunctionPtr coef_stif(new ConstantFunction(1));

  const bool boundary_regularization = true;
  const bool interior_regularization = true;
  const bool boundary_take_first_eigen = true;
  const bool interior_take_first_eigen = true;
  const bool boundary_edge_mass_matrix = true;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         n_boundary_bf,
                         n_interior_bf,
                         LAPACK,
                         LAPACK,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         boundary_edge_mass_matrix);

  fe_mesh->write_interior_basis("RECT_COARSE_TRIA_FINE_INTERIOR.vtu");
  fe_mesh->write_boundary_basis("RECT_COARSE_TRIA_FINE_BOUNDARY.vtu");

  const int gamma = 1;
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();

  mesh->numerate_edges();

  std::vector<std::set<int> > connections;
  dof_handler.dg_dofs_connections(connections);
  PatternPtr dg_pattern(new CSRPattern(connections));

  PetscMatrix system_mat(*dg_pattern.get()); // global DG matrix

  // ------------- standard contribution of laplace matrix first ---------------
  // for each coarse element
  for (int el = 0; el < fe_mesh->n_elements(); ++el)
  {
    // current coarse finite element
    const CoarseFiniteElement *cf_element = fe_mesh->element(el);

    // local stiffness matrix for a coarse scale
    PatternPtr dense_pattern(new DensePattern(cf_element->n_dofs(),
                                              cf_element->n_dofs()));
    PetscMatrix local_coarse_stif_matrix(*dense_pattern.get());

    // projecting the local stiffness matrix from the fine scale to the coarse 
    // one, multiplying by R and R^T
    local_coarse_stif_matrix.RARt(cf_element->S_fine(),
                                  cf_element->R());

#if defined(SHOW_MAT)
  std::cout << "\n\n laplace matrix for " << el << " elm \n";
  cf_element->S_fine()->matlab_view(std::cout);
  std::cout << "\n\n R for " << el << " elm \n";
  cf_element->R()->matlab_view(std::cout);
  std::cout << "\n";
#endif

    // assemble the global DG matrix for a coarse scale from the local ones
    for (int di = 0; di < cf_element->n_dofs(); ++di)
    {
      const int dof_i = cf_element->dof_number(di);
      for (int dj = 0; dj < cf_element->n_dofs(); ++dj)
      {
        const int dof_j = cf_element->dof_number(dj);
        const double value = local_coarse_stif_matrix(di, dj);
        system_mat.add_value(dof_i,
                             dof_j,
                             value);
      }
    }
  }
  system_mat.final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n DG MAT \n";
  system_mat.full_view(std::cout);
  std::cout << "\n";
#endif

  ConstantFunction coefficient(1);
  const double _gamma = 1.;

  // ------------- contribution of integrals over mesh edges ---------------
  const Mesh *base_mesh = fe_mesh->coarse_mesh()->coarse_mesh();
  // the edges of the base mesh should be initialized
  expect(base_mesh->n_edges() > 0,
         "The edges of the base mesh are not initialized");

  // a vector of sets keeping an info: to which elements a mesh edge belongs
  std::vector<std::set<int> > edge_to_cell(base_mesh->n_edges());

  // generate this map between mesh edges indices and numbers of mesh elements:
  // pass though all mesh elements
  for (int el = 0; el < base_mesh->n_elements(); ++el)
  {
    // the current element of a base mesh
    const MeshElement *mesh_element = base_mesh->element(el);
    // pass though all edges (whose numbers are in a structure of a mesh element
    // object)
    for (int e = 0; e < mesh_element->n_edges(); ++e)
    {
      // get a global index of an edge belonging to the mesh element
      const int edge_number = mesh_element->edge_number(e);
      // and insert the number of the mesh element to a corresponding place of
      // a resulting map
      edge_to_cell[edge_number].insert(el);
    }
  }

  // now we assemble the global matrix:
  // pass through all edges of the base mesh
  for (int e = 0; e < base_mesh->n_edges(); ++e)
  {
    // if an edge belongs to 1 element, it's a boundary edge
    if (edge_to_cell[e].size() == 1)
    {
      std::set<int>::const_iterator cell_number = edge_to_cell[e].begin();
      const CoarseFiniteElement *cf_element = fe_mesh->element(*cell_number);

      // local matrix of integrals over a boundary coarse edge
      PatternPtr dense_pattern(new DensePattern(cf_element->n_dofs(),
                                                cf_element->n_dofs()));
      PetscMatrixPtr edge_matrix(new PetscMatrix(*dense_pattern.get()));

      PetscMatrixPtr glob_fine_scale_bound_edges_DG_mat;

      // rewrite this !!!!!!!!!!!!!!!!!!!!!

//      // outward normal for the boundary edge
//      const MeshElement *base_cell = base_mesh->element(*cell_number);
//      const MeshElement *coarse_edge = base_mesh->edge(*b_edge);
//      const Point edge_normal = base_cell->edge_normal(*coarse_edge,
//                                                       base_mesh->vertices());

//      // compute the matrix
//      cf_element->boundary_edge_matrix(*base_mesh->edge(e),
//                                       base_mesh->vertices(),
//                                       coefficient,
//                                       _gamma,
//                                       edge_normal,
//                                       edge_matrix,
//                                       glob_fine_scale_bound_edges_DG_mat);
#if defined(SHOW_MAT)
      std::cout << "\n\nboundary edge " << e << ": "
                << base_mesh->edge(e)->vertex_number(0) << " -> "
                << base_mesh->edge(e)->vertex_number(1) << ", el = "
                << *cell_number << "\n";
      std::cout << "edge coarse matrix\n";
      edge_matrix->full_view(std::cout);
      std::cout << "edge fine matrix\n";
      glob_fine_scale_bound_edges_DG_mat->full_view(std::cout);
#endif

      // assemble the global DG matrix for a coarse scale from the local ones
      for (int di = 0; di < cf_element->n_dofs(); ++di)
      {
        const int dof_i = cf_element->dof_number(di);
        for (int dj = 0; dj < cf_element->n_dofs(); ++dj)
        {
          const int dof_j = cf_element->dof_number(dj);
          const double value = (*edge_matrix)(di, dj);
          system_mat.add_value(dof_i,
                               dof_j,
                               value);
        }
      }

#if defined(SHOW_MAT)
      system_mat.final_assembly();
      std::cout << "DG MAT after bound edge\n";
      system_mat.full_view(std::cout);
      std::cout << "\n";
#endif

    }
    // if an edge belongs to 2 elements, it's an interior edge
    else if (edge_to_cell[e].size() == 2)
    {
      std::set<int>::const_iterator cell_number = edge_to_cell[e].begin();
      const CoarseFiniteElement *cf_element_0 = fe_mesh->element(*cell_number);

      ++cell_number;
      const CoarseFiniteElement *cf_element_1 = fe_mesh->element(*cell_number);

      // local matrices of integrals over an interior coarse edge
      const int n_dofs_coarse_plus = cf_element_0->n_dofs();
      const int n_dofs_coarse_minu = cf_element_1->n_dofs();
      PatternPtr coarse_plus_plus(new DensePattern(n_dofs_coarse_plus, n_dofs_coarse_plus));
      PatternPtr coarse_plus_minu(new DensePattern(n_dofs_coarse_plus, n_dofs_coarse_minu));
      PatternPtr coarse_minu_plus(new DensePattern(n_dofs_coarse_minu, n_dofs_coarse_plus));
      PatternPtr coarse_minu_minu(new DensePattern(n_dofs_coarse_minu, n_dofs_coarse_minu));
      PetscMatrixPtr loc_mat_plus_plus(new PetscMatrix(*coarse_plus_plus.get()));
      PetscMatrixPtr loc_mat_plus_minu(new PetscMatrix(*coarse_plus_minu.get()));
      PetscMatrixPtr loc_mat_minu_plus(new PetscMatrix(*coarse_minu_plus.get()));
      PetscMatrixPtr loc_mat_minu_minu(new PetscMatrix(*coarse_minu_minu.get()));

      // rewrite this !!!!!!!!!!!!!!!!!!!!!!!!!

//      // compute the normal to the current interior edge oriented from 'plus'
//      // element to the 'minus' one
//      const MeshElement *cell_0 = base_mesh->element(cell_numbers[0]);
//      const MeshElement *coarse_edge = base_mesh->edge(*i_edge);
//      const Point inter_edge_normal = cell_0->edge_normal(*coarse_edge,
//                                                          base_mesh->vertices());

//      // compute the matrix
//      cf_element_0->interior_edge_matrix(*base_mesh->edge(e),
//                                         base_mesh->vertices(),
//                                         coefficient,
//                                         _gamma,
//                                         *cf_element_1,
//                                         loc_mat_plus_plus,
//                                         loc_mat_plus_minu,
//                                         loc_mat_minu_plus,
//                                         loc_mat_minu_minu);

#if defined(SHOW_MAT)
      std::cout << "\n\ninterior edge " << e << ": "
                << base_mesh->edge(e)->vertex_number(0) << " -> "
                << base_mesh->edge(e)->vertex_number(1) << " between el = "
                << *(edge_to_cell[e].begin()) << " and " << *cell_number << "\n";
      std::cout << "loc_mat_plus_plus\n";
      loc_mat_plus_plus->full_view(std::cout);
      std::cout << "loc_mat_plus_minu\n";
      loc_mat_plus_minu->full_view(std::cout);
      std::cout << "loc_mat_minu_plus\n";
      loc_mat_minu_plus->full_view(std::cout);
      std::cout << "loc_mat_minu_minu\n";
      loc_mat_minu_minu->full_view(std::cout);
#endif

      // assemble the global DG matrix for a coarse scale from the local ones

      // interaction between boundary functions of edges of the cf_element_0
      for (int di = 0; di < cf_element_0->n_dofs(); ++di)
      {
        const int dof_i = cf_element_0->dof_number(di);
        for (int dj = 0; dj < cf_element_0->n_dofs(); ++dj)
        {
          const int dof_j = cf_element_0->dof_number(dj);
          const double value = (*loc_mat_plus_plus)(di, dj);
          system_mat.add_value(dof_i,
                               dof_j,
                               value);
        }
      }


      // interaction between boundary functions of edges of the cf_element_0 and
      // cf_element_1
      for (int di = 0; di < cf_element_0->n_dofs(); ++di)
      {
        const int dof_i = cf_element_0->dof_number(di);
        for (int dj = 0; dj < cf_element_1->n_dofs(); ++dj)
        {
          const int dof_j = cf_element_1->dof_number(dj);
          const double value = (*loc_mat_plus_minu)(di, dj);
          system_mat.add_value(dof_i,
                               dof_j,
                               value);
        }
      }

      // interaction between boundary functions of edges of the cf_element_1 and
      // cf_element_0
      for (int di = 0; di < cf_element_1->n_dofs(); ++di)
      {
        const int dof_i = cf_element_1->dof_number(di);
        for (int dj = 0; dj < cf_element_0->n_dofs(); ++dj)
        {
          const int dof_j = cf_element_0->dof_number(dj);
          const double value = (*loc_mat_minu_plus)(di, dj);
          system_mat.add_value(dof_i,
                               dof_j,
                               value);
        }
      }

      // interaction between boundary functions of edges of the cf_element_1
      for (int di = 0; di < cf_element_1->n_dofs(); ++di)
      {
        const int dof_i = cf_element_1->dof_number(di);
        for (int dj = 0; dj < cf_element_1->n_dofs(); ++dj)
        {
          const int dof_j = cf_element_1->dof_number(dj);
          const double value = (*loc_mat_minu_minu)(di, dj);
          system_mat.add_value(dof_i,
                               dof_j,
                               value);
        }
      }
#if defined(SHOW_MAT)
      system_mat.final_assembly();
      std::cout << "DG MAT after inter edge\n";
      system_mat.full_view(std::cout);
      std::cout << "\n";
#endif

    }
    // otherwise there is an error (because in 2D an edge can belong to either
    // 1 or 2 elements only)
    else
    {
      require(false,
              "The " + d2s<int>(e) + "-th edge belongs to " +
              d2s<unsigned>(edge_to_cell[e].size()) +
              " number of mesh elements (should be 1 or 2 only)");
    }
  } // loop over edges of the coarse mesh

  //system_mat.final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n DG MAT after all edges \n";
  system_mat.full_view(std::cout);
  std::cout << "\n";
#endif
}



#endif // TEST_FEMPLUS_GMSFEM_BUILD_DG_MATRIX_HPP
