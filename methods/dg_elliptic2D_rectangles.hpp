#ifndef TEST_FEMPLUS_DG_ELLIPTIC2D_TRIANGLES_HPP
#define TEST_FEMPLUS_DG_ELLIPTIC2D_TRIANGLES_HPP

#include "config.hpp"
#include "dg_elliptic2D_aux.hpp"
#include "analytic_functions.hpp"
#include "femplus/sym_csr_pattern.hpp"
#include "gtest/gtest.h"

// =============================================================================
//
// test for rectangular very small mesh and constant solution
//
// =============================================================================
#if 0 // it's not going to work because non-zero Dirichlet bc is currently not implemented
TEST(DGElliptic2D, solution_rectangles_small)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  MeshPtr mesh(new RectangularMesh(0, 1, 0, 1, 2, 2));
  mesh->build();

  const double gamma = 100;

  check_dg_elliptic_solution(mesh,               // base mesh
                             gamma,              // IPDG gamma
                             ConstantFunction(1),// analytic solution
                             ConstantFunction(0),// rhs function
                             cur_error,          // get an error
                             cur_n_cells,        // get a number of cells
                             prev_error,         // set an error from prev sol
                             prev_n_cells);      // set a number of cells from
                                                 // prev solution
}
#endif

// =============================================================================
//
// test for rectangular meshes and constant solution
//
// =============================================================================
#if 0 // it's not going to work because non-zero Dirichlet bc is currently not implemented
TEST(DGElliptic2D, solution_rectangles_const_func)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 4; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 100;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles(gamma,          // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution_0(), // analytic solution
                                         rhs_function_0(),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
#endif

// =============================================================================
//
// test for rectangular meshes and linear solution (x+y)
//
// =============================================================================
#if 0 // it's not going to work because non-zero Dirichlet bc is currently not implemented
TEST(DGElliptic2D, solution_rectangles_x_plus_y)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 4; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 1000;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles(gamma,          // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution_1(), // analytic solution
                                         rhs_function_1(),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
#endif

// =============================================================================
//
// test for rectangular meshes and solution (x*y)
//
// =============================================================================
#if 0 // it's not going to work because non-zero Dirichlet bc is currently not implemented
TEST(DGElliptic2D, solution_rectangles_xy)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 4; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 10000;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles(gamma,          // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution_2(), // analytic solution
                                         rhs_function_2(),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
#endif
// =============================================================================
//
// test for rectangular meshes and solution (x*x + y*y)
//
// =============================================================================
#if 0 // it's not going to work because non-zero Dirichlet bc is currently not implemented
TEST(DGElliptic2D, solution_rectangles_xx_yy)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 4; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 100000;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles(gamma,          // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution_3(), // analytic solution
                                         rhs_function_3(),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
#endif
// =============================================================================
//
// test for rectangular meshes and solution (sin(x) + sin(y))
//
// =============================================================================
#if 0 // it's not going to work because non-zero Dirichlet bc is currently not implemented
TEST(DGElliptic2D, solution_rectangles_sinx_siny)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 4; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 100000;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles(gamma,          // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution_4(), // analytic solution
                                         rhs_function_4(),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
#endif
// =============================================================================
//
// test for rectangular meshes and solution (exp(x))
//
// =============================================================================
#if 0 // it's not going to work because non-zero Dirichlet bc is currently not implemented
TEST(DGElliptic2D, solution_rectangles_expx)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 4; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 100000;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles(gamma,          // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         an_solution_5(), // analytic solution
                                         rhs_function_5(),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}
#endif

// =============================================================================
//
// test for rectangular meshes and numerical solution with f = 1
// and zero Dirichlet bc (which are included in the weak form in DG method)
//
// =============================================================================
TEST(DGElliptic2D, solution_rectangles_rhs_1)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 4; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 100.;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles(gamma,          // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         ConstantFunction(0), // analytic solution doesn't matter
                                         ConstantFunction(1),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}

// =============================================================================
//
// test for rectangular meshes and numerical solution with f = 1, heteroheneous
// coefficient and zero Dirichlet bc (which are included in the weak form in DG
// method)
//
// =============================================================================
TEST(DGElliptic2D, solution_rectangles_rhs_1_hetero_coef)
{
  double cur_error;
  int cur_n_cells;
  double prev_error = -1;
  int prev_n_cells = -1;

  const int N_times = 5; // how many times to repeat the computation
  const int n_beg   = 2; // start number of cells in x- and y-directions

  const double gamma = 100.;

  for (int i = 0; i < N_times; ++i)
  {
    check_dg_elliptic_solution_rectangles_hetero_coef(gamma,   // IPDG gamma
                                         pow(2, i)*n_beg,// Nx
                                         pow(2, i)*n_beg,// Ny
                                         ConstantFunction(0), // analytic solution doesn't matter
                                         ConstantFunction(1),// rhs function
                                         cur_error,      // get error
                                         cur_n_cells,    // get n of mesh cells
                                         prev_error,     // set error from
                                                         // previous solution
                                         prev_n_cells);  // set n of mesh cells
                                                         // from previous
                                                         // solution
    prev_error = cur_error;
    prev_n_cells = cur_n_cells;
  }
}

#endif // TEST_FEMPLUS_DG_ELLIPTIC2D_TRIANGLES_HPP
