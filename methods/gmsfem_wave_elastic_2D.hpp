#ifndef FEMPLUS_TEST_GMSFEM_WAVE_ELASTIC_2D_HPP
#define FEMPLUS_TEST_GMSFEM_WAVE_ELASTIC_2D_HPP

#include "config.hpp"
#include "gtest/gtest.h"

#include "femplus/rectangular_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/overlap_coarse_fe_mesh.hpp"
#include "femplus/auxiliary_functions.hpp"
#include "femplus/gms_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/dense_pattern.hpp"

using namespace femplus;

//==============================================================================
//
// Fully working examples of solving wave elastic equation in 2D with GMsFEM
//
//==============================================================================
TEST(GMsFEM_wave_elastic_original_domain, rectangular_domain)
{
  // 0. set the parameters
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 10;
  const int _n_coarse_y = 10;
  const int _n_fine_x = 10;
  const int _n_fine_y = 10;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const double rho = 2500.;
  const double vp  = 1000.;
  const double vs  = 1000.;
  const int _n_boundary_bf = 25;
  const int _n_interior_bf = 25;
  Eigensolver eigensolver_boundary = LAPACK;
  Eigensolver eigensolver_interior = LAPACK;
  const double gamma = 0.5;
  const double _time_end = 0.15;
  const int nt = 500;
  const double dt = _time_end / nt;
  const double f = 10.;
  const double hx = (_x_end-_x_beg)/(_n_coarse_x*_n_fine_x);
  const double hy = (_y_end-_y_beg)/(_n_coarse_y*_n_fine_y);
  const double h = 7*hx;
  require(2*h < (_x_end-_x_beg), "Too large source support");
  const double xc = 0.5*(_x_end-_x_beg);
  const double yc = 0.5*(_y_end-_y_beg);
  const double theta = 0.5*math::PI;
  const double abc_layer_width = 10*hx;
  require(2*abc_layer_width < (_x_end-_x_beg), "Too thick ABC layer");

  const double max_wavelength = std::max(vp, vs) / f;
  const double min_wavelength = std::min(vp, vs) / f;
  const double max_h = std::max(hx, hy);
  const double min_h = std::min(hx, hy);

  std::cout << "max wavelength = " << max_wavelength << " m" << std::endl;
  std::cout << "min wavelength = " << min_wavelength << " m" << std::endl;
  std::cout << "max h          = " << max_h << " m" << std::endl;
  std::cout << "min h          = " << min_h << " m" << std::endl;
  std::cout << "min wavelenghts per domain = " << (_x_end-_x_beg)/max_wavelength << std::endl;
  std::cout << "max wavelenghts per domain = " << (_x_end-_x_beg)/min_wavelength << std::endl;

  std::cout << "after " << _time_end << " s the wave'll pass "
            << _time_end * std::max(vp, vs) << " m" << std::endl;

  // stability
  const double tmp = 1./(std::max(vp, vs)*sqrt(1./(hx*hx)+1./(hy*hy)));
  std::cout << "dt (" << dt << ") should be less than " << tmp << ", and it is"
            << (dt < tmp ? "" : " NOT") << std::endl;

  // dispersion
  std::cout << "min cells per wavelength = " << min_wavelength / max_h << std::endl;
  std::cout << "max cells per wavelength = " << max_wavelength / min_h << std::endl;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  class SourceGaussElasticRectangle: public Function
  {
  public:
    SourceGaussElasticRectangle(double h_, double xc_, double yc_, double theta_)
      : h(h_), xc(xc_), yc(yc_), theta(theta_) { }
    Vector values(const Point &p) const {
      const double x = p.x();
      const double y = p.y();
      const double Px = cos(theta);
      const double Py = sin(theta);
#if 1 // from my understanding
      const double G = exp(-((x-xc)*(x-xc)+(y-yc)*(y-yc))/(h*h));
#else // from Kai's code
      double G = 0;
      const double tmp = (x-xc)*(x-xc) + (y-yc)*(y-yc);
      const double dist_to_center = sqrt(tmp);
      if (dist_to_center <= 2.*h)
        G = exp(-tmp/10.0);
#endif
      Vector source(2);
      source(0) = G * Px;
      source(1) = G * Py;
      return source;
    }
  private:
    // source support, x-coord of center, y-coord of center, polar angle
    double h, xc, yc, theta;
  };

  class Ricker: public Function
  {
  public:
    Ricker(double f_) : f(f_) { }
    double value(const Point &p) const {
      const double t = _time;
      const double t0 = 1./f;
      const double pi = math::PI;
      const double part = pi*pi*f*f*(t-t0)*(t-t0);
      const double R = (1. - 2.*part) * exp(-part);
      return R;
    }
  private:
    double f; // source frequency
  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new CoarseFiniteElementMesh(coarse_mesh,
                                    FE_ORDER_COARSE,
                                    FE_ORDER_FINE,
                                    n_fine_dofs_per_fine_node));
  t1 = get_wall_time();
  std::cout << "build finite element mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices, damping
  //    matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  bool boundary_regularization = true;
  bool interior_regularization = true;
  bool boundary_take_first_eigen = true;
  bool interior_take_first_eigen = true;
  bool boundary_edge_mass_matrix = true;
  const bool show_time = false;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         boundary_edge_mass_matrix,
                         show_time);

//  const std::string R_dir = "/u/artemyev/projects/gmsfem_kaigao/sources_no_oversamp/R/";
//  fe_mesh->read_R(R_dir);

  // Damping matrix doesn't affect the solution until it reaches boundaries
//  fe_mesh->compute_damping_matrices(*coef_mass.get(),
//                                    *coef_stif.get(),
//                                    abc_layer_width, f);

  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, damping matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. build mass matrix
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr _mass(new PetscMatrix(mass_pattern));
  dof_handler.build_mass_matrix(*coef_mass.get(), *_mass.get());
  _mass->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- MASS MATRIX ----- \n";
  std::ofstream invmout("_mass_matrix__.dat");
  _mass->matlab_view(mass_pattern, invmout);
  invmout.close();
#endif
  t1 = get_wall_time();
  std::cout << "build mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 6. build global DG matrix
  connections.clear();
  dof_handler.dg_dofs_connections(connections);
  CSRPattern dg_pattern(connections);
  PetscMatrixPtr dg_mat(new PetscMatrix(dg_pattern));
  dof_handler.build_stiffness_matrix(*coef_stif.get(), *dg_mat.get());
  dof_handler.add_interior_edges_matrices(*coef_stif.get(), *dg_mat.get());
  dg_mat->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- DG MATRIX ----- \n";
  std::ofstream dgout("dg_matrix__.dat");
  dg_mat->matlab_view(dg_pattern, dgout);
  dgout.close();
#endif
  t1 = get_wall_time();
  std::cout << "build global DG matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 7. inv_N
  const int N = dof_handler.n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr _inv_N(new PetscMatrix(dense));
  dof_handler.build_inv_N_matrix(dt, *_inv_N.get());
  _inv_N->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- INVERSE N MATRIX ----- \n";
  std::ofstream invdgout("inv_N_matrix__.dat");
  _inv_N->full_view(dense, invdgout);
  invdgout.close();
#endif
  t1 = get_wall_time();
  std::cout << "inv_N, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 8. P matrix
  PetscMatrixPtr _P(new PetscMatrix(dense));
  dof_handler.build_P_matrix(dt, *_P.get());
  _P->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- P MATRIX ----- \n";
  std::ofstream invdgout("P_matrix__.dat");
  _P->full_view(dense, invdgout);
  invdgout.close();
#endif
  t1 = get_wall_time();
  std::cout << "P matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 9. global RHS
  SourceGaussElasticRectangle rhs_function(h, xc, yc, theta);
  Vector global_rhs;
  dof_handler.build_rhs_Kai(rhs_function, global_rhs); // with Kai's approach
//  Vector global_finescale_rhs;
//  dof_handler.build_global_finescale_rhs(rhs_function, global_finescale_rhs);
//  dof_handler.write_finescale_solution_vtu("global_finescale_rhs_1.vtu",
//                                           global_finescale_rhs, "name");
  t1 = get_wall_time();
  std::cout << "global RHS, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 10. online stage
  Vector solution(N, 0.);   // numerical solution on the current (n-th) time step
  Vector solution_1(N, 0.); // numerical solution on the (n-1)-th time step
  Vector solution_2(N, 0.); // numerical solution on the (n-2)-th time step

  Ricker ricker(f); // Ricker wavelet - RHS distribution in time

  for (int time_step = 2; time_step <= nt; ++time_step)
  {
    const double time = time_step * dt; // current time

    ricker.set_time(time - dt);
    const double r = ricker.value(Point()) * 1e+6; // point doesn't matter - only time

    const Vector tmp1 = Mv(dg_mat, solution_1); // K U_1
    const Vector tmp2 = Mv(_P,     solution_2); // P U_2
    const Vector tmp3 = Mv(_mass,  solution_1); // M U_1
    // 2M U_1 - P U_2 - dt^2 (K U_1 - F)
    const Vector tmp4 = 2.*tmp3 - tmp2 - dt*dt*(tmp1 - r*global_rhs);

    solution = Mv(_inv_N, tmp4);

    // reassign the solutions on the previuos time steps
    solution_2 = solution_1;
    solution_1 = solution;

//    if (time_step % 10 == 0)
//    {
//      std::string fname = "rect_rhs_fs_N_" + d2s<int>(_n_coarse_x) +
//                          "_n_" + d2s<int>(_n_fine_x) +
//                          "_t" + d2s<int>(time_step) + ".vtu";
//      dof_handler.write_finescale_solution_vtu(fname, global_fs_rhs, "global_fs_rhs");
//      fname = "rect_rhs_cs_2_N_" + d2s<int>(_n_coarse_x) +
//              "_n_" + d2s<int>(_n_fine_x) +
//              "_t" + d2s<int>(time_step) + ".vtu";
//      dof_handler.write_coarsescale_solution_vtu(fname, global_cs_rhs, "global_cs_rhs");
//    }

    if (time_step % 10 == 0)
    {
      std::cout << "time step " << time_step
                << " norm(solution) = " << math::L2_norm(solution)
                << " norm(gl_rhs) = " << math::L2_norm(global_rhs) << std::endl;

      const std::string fname = "elas_7_rect_sol_N_" + d2s<int>(_n_coarse_x) +
                                "_n_" + d2s<int>(_n_fine_x) +
                                "_t" + d2s<int>(time_step) + ".vtu";
      dof_handler.write_coarsescale_solution_vtu(fname,
                                                 solution,
                                                 "coarse_solution");
    }
  } // time loop
}
#if 0 // with overlapping
TEST(GMsFEM_wave_elastic_overlap, rectangular_domain)
{
  // 0. set the parameters
  const double _x_beg = 0.;
  const double _x_end = 1000.;
  const double _y_beg = 0.;
  const double _y_end = 1000.;
  const int _n_coarse_x = 2;
  const int _n_coarse_y = 2;
  const int _n_fine_x = 2;
  const int _n_fine_y = 2;
  const int FE_ORDER_COARSE = 1;
  const int FE_ORDER_FINE = 1;
  const int n_fine_dofs_per_fine_node = 2;
  const int _n_overlap_elements = 4;
  const double rho = 2500.;
  const double vp  = 1000.;
  const double vs  = 1000.;
  const int _n_boundary_bf = 5;
  const int _n_interior_bf = 5;
  Eigensolver eigensolver_boundary = LAPACK;
  Eigensolver eigensolver_interior = LAPACK;
  const double gamma = 1000; //0.5;
  const double _time_end = 0.15;
  const int nt = 500;
  const double dt = _time_end / nt;
  const double f = 10.;
  const double hx = (_x_end-_x_beg)/(_n_coarse_x*_n_fine_x);
  const double hy = (_y_end-_y_beg)/(_n_coarse_y*_n_fine_y);
  const double h = 7.*hx;
  const double xc = 0.5*(_x_end-_x_beg);
  const double yc = 0.5*(_y_end-_y_beg);
  const double theta = 0.5*math::PI;
  const double abc_layer_width = 10.*hx;

  const double max_wavelength = std::max(vp, vs) / f;
  const double min_wavelength = std::min(vp, vs) / f;
  const double max_h = std::max(hx, hy);
  const double min_h = std::min(hx, hy);

  std::cout << "max wavelength = " << max_wavelength << " m" << std::endl;
  std::cout << "min wavelength = " << min_wavelength << " m" << std::endl;
  std::cout << "max h          = " << max_h << " m" << std::endl;
  std::cout << "min h          = " << min_h << " m" << std::endl;
  std::cout << "min wavelenghts per domain = " << (_x_end-_x_beg)/max_wavelength << std::endl;
  std::cout << "max wavelenghts per domain = " << (_x_end-_x_beg)/min_wavelength << std::endl;

  std::cout << "after " << _time_end << " s the wave'll pass "
            << _time_end * std::max(vp, vs) << " m" << std::endl;

  // stability
  const double tmp = 1./(std::max(vp, vs)*sqrt(1./(hx*hx)+1./(hy*hy)));
  std::cout << "dt (" << dt << ") should be less than " << tmp << ", and it is"
            << (dt < tmp ? "" : " NOT") << std::endl;

  // dispersion
  std::cout << "min cells per wavelength = " << min_wavelength / max_h << std::endl;
  std::cout << "max cells per wavelength = " << max_wavelength / min_h << std::endl;

  class Isotropic: public Function {
  public:
    Isotropic(double rho, double vp, double vs): _rho(rho), _vp(vp), _vs(vs) { }
    virtual Vector values(const Point &p) const {
      Vector C(6);
      C(0) = _rho*_vp*_vp;
      C(1) = _rho*(_vp*_vp - 2.*_vs*_vs);
      C(2) = 0.;
      C(3) = _rho*_vp*_vp;
      C(4) = 0.;
      C(5) = _rho*_vs*_vs;
      return C;
    }
  private:
    double _rho, _vp, _vs;
  };

  class SourceGaussElasticRectangle: public Function
  {
  public:
    SourceGaussElasticRectangle(double h_, double xc_, double yc_, double theta_)
      : h(h_), xc(xc_), yc(yc_), theta(theta_) { }
    Vector values(const Point &p) const {
      const double x = p.x();
      const double y = p.y();
      const double Px = cos(theta);
      const double Py = sin(theta);
#if 0 // from my understanding
      const double G = exp(-((x-xc)*(x-xc)+(y-yc)*(y-yc))/(h*h));
#else // from Kai's code
      double G = 0;
      const double tmp = (x-xc)*(x-xc) + (y-yc)*(y-yc);
      const double dist_to_center = sqrt(tmp);
      if (dist_to_center <= 2.*h)
        G = exp(-tmp/10.0);
#endif
      Vector source(2);
      source(0) = G * Px;
      source(1) = G * Py;
      return source;
    }
  private:
    // source support, x-coord of center, y-coord of center, polar angle
    double h, xc, yc, theta;
  };

  class Ricker: public Function
  {
  public:
    Ricker(double f_) : f(f_) { }
    double value(const Point &p) const {
      const double t = _time;
      const double t0 = 1./f;
      const double pi = math::PI;
      const double part = pi*pi*f*f*(t-t0)*(t-t0);
      const double R = (1. - 2.*part) * exp(-part);
      return R;
    }
  private:
    double f; // source frequency
  };

//  class RickerElasticRectangle: public Function
//  {
//  public:
//    RickerElasticRectangle(double f_, double h_, double xc_, double yc_, double theta_)
//      : f(f_), h(h_), xc(xc_), yc(yc_), theta(theta_) { }
//    Vector values(const Point &p) const {
//      const double x = p.x();
//      const double y = p.y();
//      const double Px = cos(theta);
//      const double Py = sin(theta);
//      const double pi = math::PI;
//      const double t = _time;
//      const double t0 = 1./f;
//      const double part = pi*pi*f*f*(t-t0)*(t-t0);
//      const double R = (1. - 2.*part) * exp(-part);
//#if 1 // from my understanding
//      const double G = exp(-((x-xc)*(x-xc)+(y-yc)*(y-yc))/(h*h));
////      const double Gy = exp(-(y-yc)*(y-yc)/(h*h));
//#else // from Kai's code
//      double G = 0;
//      const double dist_to_center = sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc));
//      if (dist_to_center <= h)
//        G = exp(-((x-xc)*(x-xc) + (y-yc)*(y-yc))/10.0);
//#endif
//      Vector source(2);
//      source(0) = G * Px * R;
//      source(1) = G * Py * R;
//      return source;
//    }
//  private:
//    // source frequency, source support, x-coord of center, y-coord of center, polar angle
//    double f, h, xc, yc, theta;
//  };

  // 1. create the domain and build the mesh
  double t0 = get_wall_time();

  std::shared_ptr<RectangularMesh> coarse_base(
        new RectangularMesh(_x_beg, _x_end, _y_beg, _y_end, _n_coarse_x, _n_coarse_y));
  std::shared_ptr<CoarseMesh> coarse_mesh(
        new CoarseRectangularMesh(coarse_base, _n_fine_x, _n_fine_y));

  coarse_mesh->build_coarse_mesh();
  coarse_mesh->build_fine_mesh();
  coarse_base->numerate_edges();
  double t1 = get_wall_time();
  std::cout << "create the domain and build the mesh, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 2. build finite element mesh with overlapping and
  std::shared_ptr<CoarseFiniteElementMesh> fe_mesh(
        new OverlapCoarseFEMesh(coarse_mesh,
                                FE_ORDER_COARSE,
                                FE_ORDER_FINE,
                                n_fine_dofs_per_fine_node,
                                _n_overlap_elements));
  t1 = get_wall_time();
  std::cout << "build finite element mesh with overlapping, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 3. compute basis functions, global fine scale matrices, R matrices, damping
  //    matrices
  std::shared_ptr<Function> coef_mass(new ConstantFunction(rho));
  std::shared_ptr<Function> coef_stif(new Isotropic(rho, vp, vs));
  const bool show_time = false;
  fe_mesh->compute_basis(coef_mass,
                         coef_stif,
                         _n_boundary_bf,
                         _n_interior_bf,
                         eigensolver_boundary,
                         eigensolver_interior,
                         show_time);
  fe_mesh->compute_damping_matrices(*coef_mass.get(),
                                    *coef_stif.get(),
                                    abc_layer_width, f);
  t1 = get_wall_time();
  std::cout << "compute basis functions, global fine scale matrices, R "
               "matrices, damping matrices, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 4. distribute coarse scale dofs
  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();
  t1 = get_wall_time();
  std::cout << "distribute coarse scale dofs, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 5. build mass matrix
  std::vector<std::set<int> > connections;
  dof_handler.mass_dofs_connections(connections);
  CSRPattern mass_pattern(connections);
  PetscMatrixPtr _mass(new PetscMatrix(mass_pattern));
  dof_handler.build_mass_matrix(*coef_mass.get(), *_mass.get());
  _mass->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- MASS MATRIX ----- \n";
  std::ofstream invmout("_mass_matrix__.dat");
  _mass->matlab_view(mass_pattern, invmout);
  invmout.close();
#endif
  t1 = get_wall_time();
  std::cout << "build mass matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 6. build global DG matrix
  connections.clear();
  dof_handler.dg_dofs_connections(connections);
  CSRPattern dg_pattern(connections);
  PetscMatrixPtr dg_mat(new PetscMatrix(dg_pattern));
  dof_handler.build_stif_wo_bound_matrix(*coef_stif.get(), *dg_mat.get());
  dg_mat->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- DG MATRIX ----- \n";
  std::ofstream dgout("dg_matrix__.dat");
  dg_mat->matlab_view(dg_pattern, dgout);
  dgout.close();
#endif
  t1 = get_wall_time();
  std::cout << "build global DG matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 7. inv_N
  const int N = dof_handler.n_dofs();
  DensePattern dense(N, N);
  PetscMatrixPtr _inv_N(new PetscMatrix(dense));
  dof_handler.build_inv_N_matrix(dt, *_inv_N.get());
  _inv_N->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- INVERSE N MATRIX ----- \n";
  std::ofstream invdgout("inv_N_matrix__.dat");
  _inv_N->full_view(dense, invdgout);
  invdgout.close();
#endif
  t1 = get_wall_time();
  std::cout << "inv_N, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 8. P matrix
  PetscMatrixPtr _P(new PetscMatrix(dense));
  dof_handler.build_P_matrix(dt, *_P.get());
  _P->final_assembly();
#if defined(SHOW_MAT)
  std::cout << "\n\n   ----- P MATRIX ----- \n";
  std::ofstream invdgout("P_matrix__.dat");
  _P->full_view(dense, invdgout);
  invdgout.close();
#endif
  t1 = get_wall_time();
  std::cout << "P matrix, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 9. global RHS
  SourceGaussElasticRectangle rhs_function(h, xc, yc, theta);
  Vector global_rhs;
  dof_handler.build_rhs(rhs_function, global_rhs);
  t1 = get_wall_time();
  std::cout << "global RHS, time = " << t1-t0 << std::endl;
  t0 = t1;

  // 10. online stage
  Vector solution(N, 0.);   // numerical solution on the current (n-th) time step
  Vector solution_1(N, 0.); // numerical solution on the (n-1)-th time step
  Vector solution_2(N, 0.); // numerical solution on the (n-2)-th time step

  Ricker ricker(f); // Ricker wavelet - RHS distribution in time

  for (int time_step = 2; time_step <= nt; ++time_step)
  {
    const double time = time_step * dt; // current time

    ricker.set_time(time - dt);
    const double r = ricker.value(Point()) * 1e+6; // point doesn't matter - only time

    const Vector tmp1 = Mv(dg_mat, solution_1); // K U_1
    const Vector tmp2 = Mv(_P,     solution_2); // P U_2
    const Vector tmp3 = Mv(_mass,  solution_1); // M U_1
    // 2M U_1 - P U_2 - dt^2 (K U_1 - F)
    const Vector tmp4 = 2.*tmp3 - tmp2 - dt*dt*(tmp1 - r*global_rhs);

    solution = Mv(_inv_N, tmp4);

    // reassign the solutions on the previuos time steps
    solution_2 = solution_1;
    solution_1 = solution;

//    if (time_step % 10 == 0)
//    {
//      std::string fname = "rect_rhs_fs_N_" + d2s<int>(_n_coarse_x) +
//                          "_n_" + d2s<int>(_n_fine_x) +
//                          "_t" + d2s<int>(time_step) + ".vtu";
//      dof_handler.write_finescale_solution_vtu(fname, global_fs_rhs, "global_fs_rhs");
//      fname = "rect_rhs_cs_2_N_" + d2s<int>(_n_coarse_x) +
//              "_n_" + d2s<int>(_n_fine_x) +
//              "_t" + d2s<int>(time_step) + ".vtu";
//      dof_handler.write_coarsescale_solution_vtu(fname, global_cs_rhs, "global_cs_rhs");
//    }

    if (time_step % 10 == 0)
    {
      std::cout << "time step " << time_step
                << " norm(solution) = " << math::L2_norm(solution)
                << " norm(gl_rhs) = " << math::L2_norm(global_rhs) << std::endl;

      const std::string fname = "elas_1_rect_sol_N_" + d2s<int>(_n_coarse_x) +
                                "_n_" + d2s<int>(_n_fine_x) +
                                "_t" + d2s<int>(time_step) + ".vtu";
      dof_handler.write_coarsescale_solution_vtu(fname,
                                                 solution,
                                                 "coarse_solution");
    }

  } // time loop
}
#endif // with overlapping



#endif // FEMPLUS_TEST_GMSFEM_WAVE_ELASTIC_2D_HPP
