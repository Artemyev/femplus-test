#ifndef FEMPLUS_TEST_GMSFEM_ELLIPTIC_SALT_DOME_HPP
#define FEMPLUS_TEST_GMSFEM_ELLIPTIC_SALT_DOME_HPP

#include "config.hpp"
#include "gtest/gtest.h"
#include "elliptic2D_aux.hpp"

#include "femplus/gms_dof_handler.hpp"
#include "femplus/grid_function.hpp"
#include "femplus/finite_element_mesh.hpp"

using namespace femplus;

//==============================================================================
//
// There are several model of the salt dome. The simplest one is with the
// flat top surface.
// We have a geometrically defined model of a salt dome, split into unstructured
// fine triangles. We call this mesh F-UT, and we use standard FEM for this
// mesh. We also redefine the model as a fine rectangular grid (F-R), and we
// also use standard FEM on it, and compare the results. Then, we use F-R grid
// for GMsFEM computation with rectangular coarse grid and fine rect grid
// inside. So it's called C-RR.
//
//==============================================================================
TEST(GMsFEM_elliptic_salt_dome, flat_top_surface)
{
  class SaltDomeCoefA: public Function {
  public:
    virtual double value(const Point &point) const {
      double vp = 1e+32;
      switch (_material_id) {
        case 1: { // bottom layer
          vp = 2800; break; }
        case 2: { // salt dome
          vp = 4550; break; }
        case 3: { // layer one
          vp = 3000; break; }
        case 4: { // layer two
          vp = 2700; break; }
        case 5: { // oil layer
          vp = 2400; break; }
        case 6: { // layer three
          vp = 2300; break; }
        case 7: { // top layer
          vp = 2500; break; }
        default:
          require(false, "Unknown physical domain: " + d2s(_material_id));
      }
      return vp*vp; //(double)_material_id;
    }
  };

  const std::string F_UT_name = TESTFILES_DIR + "/salt_dome_flat_F_UT.msh";
  const std::string F_R_name  = "salt_dome_flat_F_R.msh";
  const std::string C_RR_name = "salt_dome_flat_C_RR.msh";
  std::string fname_sol;

  ConstantFunction rhs_function(1.);
  ConstantFunction bound_cond(0.);
  FunctionPtr coef_stif(new SaltDomeCoefA);
  const int fe_fine_order = 1;

  MeshPtr f_ut_mesh(new TriangularMesh(F_UT_name));

  std::shared_ptr<CGDoFHandler> f_ut_dof_handler;
  const Vector f_ut_solution =
  fem_elliptic(f_ut_mesh, fe_fine_order, *coef_stif.get(),
               rhs_function, bound_cond, f_ut_dof_handler);

  fname_sol = file_stem(F_UT_name) + "_sol.vtu";
  f_ut_dof_handler->write_solution(fname_sol,
                                   std::vector<Vector>(1, f_ut_solution),
                                   std::vector<std::string>(1, "f_ut_solution"));

  const int Nnx = 50;
  const int Nny = 50;
  MeshPtr f_r_mesh(new RectangularMesh(f_ut_mesh->min_coord(),
                                       f_ut_mesh->max_coord(),
                                       Nnx, Nny));
  dynamic_cast<RectangularMesh*>(f_r_mesh.get())->build(*f_ut_mesh.get());
  f_r_mesh->write(F_R_name);

  std::cout << "rect mesh produced" << std::endl;

//  FEMeshPtr f_r_fe_mesh(new LagrangeMesh(f_r_mesh, 1));
//  CGDoFHandler f_r_dof_handler(f_r_fe_mesh);
//  f_r_dof_handler.distribute_dofs();
//  Vector coef_distrib;
//  f_r_dof_handler.init_vector(*coef_stif.get(), coef_distrib);
//  f_r_dof_handler.write_solution_vtu_in_cells("delemeincells.vtu",
//                                              std::vector<Vector>(1, coef_distrib),
//                                              std::vector<std::string>(1, "asdad"));

  std::shared_ptr<CGDoFHandler> f_r_dof_handler;
  const Vector f_r_solution =
  fem_elliptic(f_r_mesh, fe_fine_order, *coef_stif.get(),
               rhs_function, bound_cond, f_r_dof_handler);

  fname_sol = file_stem(F_R_name) + "_sol.vtu";
  f_r_dof_handler->write_solution(fname_sol,
                                  std::vector<Vector>(1, f_r_solution),
                                  std::vector<std::string>(1, "f_r_solution"));

  // GMsFEM parameters
  const double x0 = f_ut_mesh->min_coord().x();
  const double x1 = f_ut_mesh->max_coord().x();
  const double y0 = f_ut_mesh->min_coord().y();
  const double y1 = f_ut_mesh->max_coord().y();
  const int Nx = 5;
  const int Ny = 5;
  const int nx = 10;
  const int ny = 10;
  const int coarse_order = 1;
  const int fine_order   = 1;

  // --- R matrix ---
//  const int n_boundary_bf_min       = 15;
  const int n_boundary_bf_max       = 15;
//  const int n_interior_bf_min       = 5;
  const int n_interior_bf_max       = 5;
  Eigensolver bound_eigensolver     = KRYLOV;
  Eigensolver inter_eigensolver     = KRYLOV;
  const bool bound_regularization   = true;
  const bool inter_regularization   = true;
  const bool bound_take_first_eigen = true;
  const bool inter_take_first_eigen = true;
  const bool bound_edge_mass        = true;
  const bool show_time_eig          = false;

  const double gamma  = 100;
  const bool add_boundary_edges = true;
  const bool add_interior_edges = true;
  const bool ref_numerical = true;

  RectMeshPtr basemesh(new RectangularMesh(x0, x1, y0, y1, Nx, Ny));
  CoarseMeshPtr c_rr_mesh(new CoarseRectangularMesh(basemesh, nx, ny,
                                                FineMeshType::Rectangular));

  c_rr_mesh->build_coarse_mesh();
  c_rr_mesh->build_fine_mesh();
  c_rr_mesh->set_material_ids(f_r_mesh.get());
  basemesh->numerate_edges();

  c_rr_mesh->write_msh(C_RR_name);

  Vector c_rr_solution;
  fname_sol = file_stem(C_RR_name);
  double rel_error =
  gmsfem_elliptic(c_rr_mesh, coef_stif, coarse_order, fine_order,
                  n_boundary_bf_max, n_interior_bf_max, bound_eigensolver,
                  inter_eigensolver, bound_regularization,
                  inter_regularization, bound_take_first_eigen,
                  inter_take_first_eigen, bound_edge_mass,
                  show_time_eig, gamma, add_boundary_edges,
                  add_interior_edges, rhs_function, fname_sol,
                  c_rr_solution, f_r_mesh, bound_cond, ref_numerical);

  std::cout << "rel_error = " << rel_error << std::endl;
}

#endif // FEMPLUS_TEST_GMSFEM_ELLIPTIC_SALT_DOME_HPP
