#ifndef TEST_FEMPLUS_ELLIPTIC2D_AUX_HPP
#define TEST_FEMPLUS_ELLIPTIC2D_AUX_HPP

#include "config.hpp"

#include "femplus/function.hpp"
#include "femplus/constant_function.hpp"
#include "femplus/rectangular_mesh.hpp"
#include "femplus/lagrange_finite_element_mesh.hpp"
#include "femplus/dof_handler.hpp"
#include "femplus/cg_dof_handler.hpp"
#include "femplus/csr_pattern.hpp"
#include "femplus/vector.hpp"
#include "femplus/petsc_matrix.hpp"
#include "femplus/math_functions.hpp"
#include "femplus/solver.hpp"
#include "femplus/triangular_mesh.hpp"
#include "femplus/rectangle.hpp"
#include "femplus/mesh_element.hpp"
#include "femplus/coarse_finite_element_mesh.hpp"
#include "femplus/coarse_rectangular_mesh.hpp"
#include "femplus/gms_dof_handler.hpp"

#if defined(USE_BOOST)
  #include "boost/format.hpp"
#endif

//#define SHOW_MATRIX

using namespace femplus;

// =============================================================================
//
// solution of an elliptic 2D problem with Dirichlet boundary condition
//
// =============================================================================
Vector fem_elliptic(MeshPtr mesh,
                    int order,
                    const Function &coef_stif,
                    const Function &rhs_function,
                    const Function &bound_value,
                    std::shared_ptr<CGDoFHandler> &dof_handler)
{
  FEMeshPtr fe_mesh(new LagrangeMesh(mesh, order));

  dof_handler = std::shared_ptr<CGDoFHandler>(new CGDoFHandler(fe_mesh));
  dof_handler->distribute_dofs();
  std::vector<std::set<int> > connections;
  dof_handler->dofs_connections(connections);
  PatternPtr pattern(new CSRPattern(connections));

  PetscMatrix system_mat(*pattern.get()); // global stiffness matrix
  dof_handler->build_stiffness_matrix(coef_stif, system_mat);
  system_mat.final_assembly();

  PetscMatrixPtr L2_mat(new PetscMatrix(*pattern.get()));
  dof_handler->build_L2_matrix(*L2_mat.get());
  L2_mat->final_assembly();

  Vector system_rhs; // right hand side vector
  dof_handler->build_vector(L2_mat, rhs_function, system_rhs);

  // NULL here means that we consider all boundaries for applying Dirichlet bc
  dof_handler->dirichlet(system_mat, NULL, bound_value, &system_rhs);

  Vector solution; // numerical solution

  Solver solver(system_mat);
  solver.solve(system_rhs, solution);

  return solution;
}
//==============================================================================
void check_elliptic_solution(MeshPtr mesh,
                             const Function &an_solution,
                             const Function &rhs_function,
                             const Function &coef_stif,
                             double &current_rel_error,
                             int &current_n_elements,
                             double prev_rel_error = -1,
                             int prev_n_elements = -1,
                             bool show_results = false,
                             const std::string &fname_base = "")
{
  // numerical solution
  const int order = 1;
  std::shared_ptr<CGDoFHandler> dof_handler;
  Vector solution = fem_elliptic(mesh, order,
                                 coef_stif,
                                 rhs_function,
                                 an_solution, // boundary condition
                                 dof_handler);

  Vector exact_solution; // analytical solution
  dof_handler->init_vector(an_solution, exact_solution);
  current_rel_error = math::rel_error(exact_solution, solution);

  const double errors_ratio = prev_rel_error / current_rel_error;
  const double n_cells_ratio = 1. * current_n_elements / prev_n_elements;

  if (show_results)
  {
#if defined(USE_BOOST)
    using boost::format;
    std::cout << format("ncells = %5d rel_error = %2.8e "
                        "error_reduction = %2.8e mesh_reduction = %5g\n")
                 % mesh->n_elements()
                 % current_rel_error
                 % (fabs(prev_rel_error + 1) < 1e-8 ? 0 : errors_ratio)
                 % (prev_n_elements == -1 ? 0 : n_cells_ratio);
#else
//    std::cout.setf(std::ios::scientific);
//    std::cout.precision(16);
//    std::cout << current_rel_error << ",\n";
    std::cout << " ncells = " << mesh->n_elements()
              << " rel_error = " << current_rel_error
              << " error_reduction = " << (fabs(prev_rel_error + 1) < 1e-8 ? 0 : errors_ratio)
              << " mesh_reduction = " << (prev_n_elements == -1 ? 0 : n_cells_ratio)
              << "\n";
#endif
  }
  if (fname_base != "") {
    const std::string fname = "sol_" + fname_base + ".vtu";
    std::vector<Vector> sols;
    sols.push_back(exact_solution);
    sols.push_back(solution);
    sols.push_back(exact_solution - solution);
    std::vector<std::string> nams;
    nams.push_back("reference_solution");
    nams.push_back("numerical_solution");
    nams.push_back("difference_between_them");
    dof_handler->write_solution(fname, sols, nams);
  }
}
// -----------------------------------------------------------------------------
void check_elliptic_solution_rectangles(int N_FINE_X,
                                        int N_FINE_Y,
                                        const Function &an_solution,
                                        const Function &rhs_function,
                                        const Function &coef_stif,
                                        double &current_rel_error,
                                        int &current_n_elements,
                                        double prev_rel_error = -1,
                                        int prev_n_elements = -1,
                                        bool show_results = false,
                                        const std::string &fname = "",
                                        double X_BEG = 0.,
                                        double X_END = 1.,
                                        double Y_BEG = 0.,
                                        double Y_END = 1.)
{
  MeshPtr mesh(new RectangularMesh(X_BEG, X_END,
                                   Y_BEG, Y_END,
                                   N_FINE_X, N_FINE_Y));
  mesh->build();
  current_n_elements = mesh->n_elements();

  check_elliptic_solution(mesh,
                          an_solution,
                          rhs_function,
                          coef_stif,
                          current_rel_error,
                          current_n_elements,
                          prev_rel_error,
                          prev_n_elements,
                          show_results,
                          fname);
}
// -----------------------------------------------------------------------------
void check_elliptic_solution_triangles(int N_FINE_X,
                                       int N_FINE_Y,
                                       const Function &an_solution,
                                       const Function &rhs_function,
                                       const Function &coef_stif,
                                       double &current_rel_error,
                                       int &current_n_elements,
                                       double prev_rel_error = -1,
                                       int prev_n_elements = -1,
                                       bool show_results = false,
                                       const std::string &fname = "",
                                       double X_BEG = 0.,
                                       double X_END = 1.,
                                       double Y_BEG = 0.,
                                       double Y_END = 1.)
{
  std::vector<Point> points;
  points.push_back(Point(X_BEG, Y_BEG));
  points.push_back(Point(X_END, Y_BEG));
  points.push_back(Point(X_BEG, Y_END));
  points.push_back(Point(X_END, Y_END));
  MeshElementPtr rectangle(new Rectangle(0, 1, 2, 3));
  MeshPtr mesh(new TriangularMesh(points, rectangle, N_FINE_X));
  mesh->build();
  current_n_elements = mesh->n_elements();

  check_elliptic_solution(mesh,
                          an_solution,
                          rhs_function,
                          coef_stif,
                          current_rel_error,
                          current_n_elements,
                          prev_rel_error,
                          prev_n_elements,
                          show_results,
                          fname);
}

//==============================================================================
//
// GMsFEM
//
//==============================================================================
double gmsfem_elliptic(CoarseMeshPtr &cmesh,
                       FunctionPtr coef_stif,
                       int fe_order_coarse,
                       int fe_order_fine,
                       int n_boundary_bf,
                       int n_interior_bf,
                       Eigensolver bound_eigensolver,
                       Eigensolver inter_eigensolver,
                       bool boundary_regularization,
                       bool interior_regularization,
                       bool boundary_take_first_eigen,
                       bool interior_take_first_eigen,
                       bool edge_mass_matrix,
                       bool show_time_eigensolver,
                       double gamma,
                       bool add_boundary_edges,
                       bool add_interior_edges,
                       const Function &rhs_function,
                       const std::string &fname_diff,
                       Vector &solution,
                       MeshPtr ref_mesh,
                       const Function &ref_solution,
                       bool ref_numerical = false)
{
  CoarseFEMeshPtr fe_mesh(new CoarseFiniteElementMesh(cmesh,
                                                      fe_order_coarse,
                                                      fe_order_fine));

  FunctionPtr coef_one(new ConstantFunction(1.));
  fe_mesh->compute_basis(coef_one, // there is no mass coefficient
                         coef_stif,
                         n_boundary_bf,
                         n_interior_bf,
                         bound_eigensolver,
                         inter_eigensolver,
                         boundary_regularization,
                         interior_regularization,
                         boundary_take_first_eigen,
                         interior_take_first_eigen,
                         edge_mass_matrix,
                         show_time_eigensolver);

  std::string fname;
//  if (fname_diff != "") {
//    fname = "acoustic_bound_bf_0_" + fname_diff + ".vtu";
//    fe_mesh->element(0)->write_boundary_bf(fname);
//    fname = "acoustic_inter_bf_0_" + fname_diff + ".vtu";
//    fe_mesh->element(0)->write_interior_bf(fname);
//  }

//  if (fname_diff != "") {
//    fname = "acoustic_interior_bf_" + fname_diff + ".vtu";
//    fe_mesh->write_interior_basis(fname);
//    fname = "acoustic_boundary_bf_" + fname_diff + ".vtu";
//    fe_mesh->write_boundary_basis(fname);
//  }

#if defined(SHOW_MAT)
//  for (int el = 0; el < fe_mesh->n_elements(); ++el)
//  {
//    std::cout << "\n\nR matrix for " << el << " elm\n";
//    fe_mesh->element(el)->R()->full_view(std::cout);
//  }
#endif

  GMsDoFHandler dof_handler(fe_mesh, gamma);
  dof_handler.distribute_dofs();

  std::vector<std::set<int> > connections;
  dof_handler.dg_dofs_connections(connections);
  PatternPtr dg_pattern(new CSRPattern(connections));

  PetscMatrix system_mat(*dg_pattern.get()); // global DG matrix
  dof_handler.build_stiffness_matrix(*coef_stif.get(), system_mat);
  if (add_boundary_edges)
    dof_handler.add_boundary_edges_matrices(*coef_stif.get(), system_mat);
  if (add_interior_edges)
    dof_handler.add_interior_edges_matrices(*coef_stif.get(), system_mat);
  system_mat.final_assembly();
#if defined(SHOW_MATRIX)
  std::cout << "stiffness\n";
  system_mat.matlab_view();
#endif

  Vector system_rhs; // right hand side vector
  dof_handler.build_vector(rhs_function, system_rhs);

  if (fname_diff != "") {
    fname = "rhs_" + fname_diff + ".vtu";
    dof_handler.write_coarsescale_solution_vtu(fname, system_rhs, "rhs");
  }

  Solver solver(system_mat);
  Vector coarse_solution;
  solver.solve(system_rhs, coarse_solution);

  if (fname_diff != "") {
    fname = "sol_" + fname_diff;
    if (!add_boundary_edges) fname += "_no_bou";
    if (!add_interior_edges) fname += "_no_int";
    fname += ".vtu";
    dof_handler.write_coarsescale_solution_vtu(fname, coarse_solution, "sol");
  }

  // numerical solution
  solution = dof_handler.to_fine_scale(coarse_solution);

  double rel_error = 0; // relative error between this numerical solution and
                        // analytical one

  // get a reference solution
  Vector ref_sol; // reference solution
  if (ref_mesh == NULL) // get an analytical reference solution on the same mesh as for GMsFEM
  {
    dof_handler.init_vector(ref_solution, ref_sol);
    if (fname_diff != "") {
      fname = "diff_" + fname_diff + ".vtu";
      dof_handler.write_finescale_solution_vtu(fname, ref_sol - solution, "diff");
    }
    rel_error = math::rel_error(ref_sol, solution); // relative error
  }
  else // another reference mesh
  {
    Vector num_sol_at_ref_dofs; // GMsFEM solution at reference dofs
    std::shared_ptr<CGDoFHandler> ref_dh;

    if (ref_numerical == false) { // reference solution is analytical, but on an another mesh
      ref_mesh->build();
      FEMeshPtr fe_ref_mesh(new LagrangeMesh(ref_mesh, fe_order_fine));
      ref_dh = std::shared_ptr<CGDoFHandler>(new CGDoFHandler(fe_ref_mesh));
      ref_dh->distribute_dofs();
      ref_dh->init_vector(ref_solution, ref_sol);
    }
    else // reference solution is numerical - computed on fine mesh using FEM
    {
      ref_sol = fem_elliptic(ref_mesh, fe_order_fine, *coef_stif.get(),
                             rhs_function, ref_solution, ref_dh);
    }

    // recompute GMsFEM solution at reference dofs points
    rel_error = dof_handler.compute_error(ref_dh->dofs(),
                                          ref_sol,
                                          coarse_solution,
                                          num_sol_at_ref_dofs);

    if (fname_diff != "") {
      fname = "compare_with_an_" + fname_diff + ".vtu";
      std::vector<Vector> output_sol(3);
      output_sol[0] = ref_sol;
      output_sol[1] = num_sol_at_ref_dofs;
      output_sol[2] = ref_sol - num_sol_at_ref_dofs;
      std::vector<std::string> names(3);
      names[0] = "reference_solution";
      names[1] = "gmsfem_solution_at_reference_solution_dofs";
      names[2] = "difference_between_them";
      ref_dh->write_solution_vtu(fname, output_sol, names);
    }
  }

  return rel_error;
}

#endif // TEST_FEMPLUS_ELLIPTIC2D_AUX_HPP
